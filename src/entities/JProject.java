package entities;

import java.util.List;
import java.util.concurrent.Semaphore;

import gui.runApplication;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import sim.JClass;

public class JProject {
	
	private final String pathTo;
	private final String projectName;
	private List<JClass> projectClasses;
	private final BooleanProperty classesInitialized;
	private final Semaphore jsonSema=new Semaphore(1, true);
	
	public JProject(String pathTo, String projectName){
		this.pathTo=pathTo;
		this.projectName=projectName;
		this.projectClasses=null;
		this.classesInitialized=new SimpleBooleanProperty(false);
	}
	public void startPreprocessing() {
		makePreprocessingThread().start();
	}
	private void startPreprocessingAndWait() {
		Thread preThread = makePreprocessingThread();
		preThread.start();
		try {
			preThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private Thread makePreprocessingThread() {
		return new Thread(){
			public void run(){
				System.out.println("makePreprocessingThread:"+pathTo+"\\"+projectName);
				if(!classesInitialized.get()){	
					try {
						jsonSema.acquire();
						if(!classesInitialized.get())
							setProjectClasses(runApplication.preProcess(pathTo+"\\"+projectName, projectName));
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						jsonSema.release();
					}			
				}	
			}
		};
	}
	private void setProjectClasses(List<JClass> projectClasses) {
		this.projectClasses = projectClasses;
		setClassesInitialized(projectClasses!=null);
	}
	public String getPathTo() {
		return pathTo;
	}
	public String getProjectName() {
		return projectName;
	}
	public String getProjectDir() {
		return pathTo+"\\"+projectName;
	}
	public List<JClass> getProjectClasses() {
		startPreprocessingAndWait();
		return projectClasses;
	}	
	public BooleanProperty classesInitializedProperty() {
		return classesInitialized;
	}
	public void setClassesInitialized(boolean classesInitialized) {
		Platform.runLater(()->this.classesInitialized.set(classesInitialized));
	}
}
