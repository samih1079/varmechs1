package entities;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class VisualizedPair {
	private final double enw;
	private final double pnw;
	private final double th;
	private final String project1;
	private final String project2;
	private final String visFile;
	private final DoubleProperty  progress;
	public VisualizedPair(double enw, double pnw, double th, String project1, String project2, String visFile){
		this.enw=enw;
		this.pnw=pnw;
		this.th=th;
		this.project1=project1;
		this.project2=project2;
		this.visFile=visFile;
		progress = new SimpleDoubleProperty(){
			public void set(double value){
				Platform.runLater(()->super.set(value));
			}
		};
	}
	public VisualizedPair(double enw, double pnw, double th, String project1, String project2, String visFile, boolean done) {
		this.enw=enw;
		this.pnw=pnw;
		this.th=th;
		this.project1=project1;
		this.project2=project2;
		this.visFile=visFile;
		if(done)
			progress = new SimpleDoubleProperty(100);
		else
			progress = new SimpleDoubleProperty();
	}
	public double getEnw() {
		return enw;
	}
	public double getPnw() {
		return pnw;
	}
	public double getTh() {
		return th;
	}
	public String getProject1() {
		return project1;
	}
	public String getProject2() {
		return project2;
	}
	public String getVisFile() {
		return visFile;
	}
	public double getProgress() {
		return progress.get();
	}
	public void setPrecentage(double precentage) {
		this.progress.set(precentage);
	}
	public DoubleProperty progressProperty() {
        return progress;
    }
}
