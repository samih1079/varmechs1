package entities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Semaphore;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class DBconn {
	private static final String database = "sova";
	private static boolean canConnect=true;
	private static final int MAX_CONN=90;
	private static int connCounter=0;
	public DBconn(){
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			//Class.forName("com.mysql.jdbc.Driver");

			Connection conn = getConn();
			canConnect=conn!=null;//Establish if we can connect to the database
			if(canConnect)
				closeConn(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static Connection getConn(){
		if(!canConnect || connCounter>MAX_CONN)
			return null;	
		try {
			connCounter++;
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, "root", "");
		} catch (SQLException e) {
			connCounter--;
			e.printStackTrace();
		} 
		return null;
	}
	
	public static void closeConn(Connection conn){
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			connCounter--;
		}
	}
	
	public static ResultSet executeQuery(Connection conn, String msg) throws SQLException{
		if(conn==null)
			return null;
		Statement statment = conn.createStatement();
		return statment.executeQuery(msg);
	}
	public static boolean execute(Connection conn, String msg) throws SQLException{
		if(conn==null)
			return false;
		Statement statment = conn.createStatement();
		return statment.execute(msg);
	}
}