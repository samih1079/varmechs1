/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;


import java.util.ArrayList;

import java.sql.Connection;

/**
 *
 * @author 
 */
public class TempMcsSimilarityHandler {

    
	public static TempMCSSimilarityEntity getSimByterm(String term1,String term2, Connection conn){
		TempMCSSimilarityEntity details = new TempMCSSimilarityEntity(term1, term2, conn);   
           if (details.getTempSimByterms(true)) {
          return details;
         }
         return null;
            
 }
               


    public static boolean insertRequirement(String term1, String term2, Float simVal, Connection conn) {

    	TempMCSSimilarityEntity recDetails = new TempMCSSimilarityEntity(term1, term2, simVal, conn);
        if (recDetails.addMCSTermsSim()) {
            return true;
        }
        return false;
    }
}
