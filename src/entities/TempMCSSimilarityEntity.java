/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.sql.Connection;

/**
 *
 * @author Owner
 */
public class TempMCSSimilarityEntity {
    private String term1;
    private String term2;
    private Float MCSSimilarity;
    private Connection conn;

    public TempMCSSimilarityEntity(String term1, String term2, Connection conn) {
        this.term1 = term1;
        this.term2 = term2;
        this.conn=conn;
    }
    
    

    public TempMCSSimilarityEntity(String term1, String term2, Float MCSSimilarity, Connection conn) {
        this.term1 = term1;
        this.term2 = term2;
        this.MCSSimilarity = MCSSimilarity;
        this.conn=conn;
    }

    public String getTerm1() {
        return term1;
    }

    public void setTerm1(String term1) {
        this.term1 = term1;
    }

    public String getTerm2() {
        return term2;
    }

    public void setTerm2(String term2) {
        this.term2 = term2;
    }

    public Float getMCSSimilarity() {
        return MCSSimilarity;
    }

    public void setMCSSimilarity(Float MCSSimilarity) {
        this.MCSSimilarity = MCSSimilarity;
    }

    @Override
    public String toString() {
        return "TempMCSSimilarityEntity{" + "term1=" + term1 + ", term2=" + term2 + ", MCSSimilarity=" + MCSSimilarity + '}';
    }
 
        
 
public  boolean  getTempSimByterms(boolean upload) {
          boolean found=false;
          ResultSet rs;
          String msg = String.format("select * from TblTempSimilarity where term1='%s' and term2='%s' "
                   , getTerm1(), getTerm2());
        try {
            rs= DBconn.executeQuery(conn, msg);
            if (rs!=null && rs.next()) {
               found=true;
               if(upload)  {
                     setTerm1(rs.getString("term1"));
                     setTerm2(rs.getString("term2"));
            	     setMCSSimilarity(rs.getFloat("MCSSimilarity"));
                   
               }
            }
        } catch (SQLException e) {
        	e.printStackTrace();      
        }
        return found;
    }

     
    
    public boolean addMCSTermsSim() {
        String msg = String.format("INSERT INTO TblTempSimilarity (term1,term2,MCSSimilarity)"
                + " VALUES ('%s','%s',%s)", getTerm1(),getTerm2(),getMCSSimilarity());      
        try {
			return DBconn.execute(conn, msg);
		} catch (SQLException e) {
			e.printStackTrace(); 
			return false;
		}
    }




}
