package entities;

import java.util.LinkedList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

public class MyTreeNode {
	private String name;
	private List<MyTreeNode> children;
	private int childrenCount;
	public MyTreeNode(String name){
		this.name=name;
		children = new LinkedList<>();
		childrenCount=0;
	}
	public String getName() {
		return name;
	}
	public void addToFront(MyTreeNode node) {
		children.add(0, node);
		childrenCount++;
	}
	public void add(MyTreeNode node) {
		children.add(node);
		childrenCount++;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<MyTreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<MyTreeNode> children) {
		childrenCount=children.size();
		this.children = children;
	}
	public boolean isLeaf() {
		return childrenCount==0;
	}
	public int getChildrenCount() {
		return childrenCount;
	}
	public MyTreeNode firstChild() {
		if(childrenCount==0)
			return null;
		return children.get(0);	
	}	
	public String toString(){
		return name;
	}
	public DefaultMutableTreeNode toTreeNode() {
		DefaultMutableTreeNode root=new DefaultMutableTreeNode(name);
		for(MyTreeNode child:children)
			root.add(child.toTreeNode());
		return root;
	}	
}
