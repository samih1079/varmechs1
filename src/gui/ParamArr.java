package gui;

import java.util.Arrays;

public class ParamArr implements Comparable<ParamArr>{
	private final Integer[] params;
	public ParamArr(Integer... objects){
		params=objects;
	}
	public boolean isEmpty() {
		return params.length==0;
	}
	@Override
	public int hashCode() {
		return Arrays.deepHashCode(params);
	}
	@Override
	public int compareTo(ParamArr other) {
		Integer[] otherParams = other.params;
		int pLen=params.length;
		if(otherParams.length==pLen){	
			for(int i=0;i<pLen;i++){
				int cmp=otherParams[i]-params[i];
				if(cmp!=0)
					return cmp;
			}
			return 0;
		}
		return -(otherParams.length-pLen)*100;
	}
	public boolean equals(Object obj) {
		if(obj instanceof ParamArr){
			ParamArr other=(ParamArr) obj;
			return Arrays.deepEquals(this.params, other.params);
		}
		return false;
	}
	public String toString(){
		int pLen=params.length;
		String s="";
		for(int i=0;i<pLen;i++){
			s+=params[i]+"%";
			if(i!=params.length-1)
				s+=" | ";
		}
		return s;
	}
}
