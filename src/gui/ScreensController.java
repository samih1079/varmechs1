package gui;

import java.util.HashMap;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

	/**
	 * This class holds all the screen to be displayed in HashMap.
	 * @author AsafM
	 */
	public class ScreensController extends StackPane{

		private HashMap<String, Node> screens = new HashMap<>();

		/**
		 * Constructor.
		 */
		public ScreensController()
		{
			super();
		}

		/**
		 * The method adds a screen to the collection.
		 * @param name
		 * @param screen
		 */
		public void addScreen(String name, Node screen)
		{
			screens.put(name, screen);
		}

		/**
		 * The method returns the Node with that name.
		 * @param name
		 * @return value
		 */
		public Node getScreen(String name)
		{
			return screens.get(name);
		}

		/**
		 * The method loads the fxml file, add the screen to the screens collection and
		 * injects the screenPane to the controller.
		 * @param name
		 * @param resource
		 * @return true if loaded, false if not
		 */
		public boolean loadScreen(String name, String resource)
		{
			try
			{
				FXMLLoader myloader = new FXMLLoader (getClass().getResource(resource));
				Parent loadScreen = (Parent) myloader.load();
				ControlledScreen myScreenController = ((ControlledScreen) myloader.getController());
				myScreenController.setScreenParent(this);
				addScreen(name, loadScreen);
				return true;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}

		/**
		 * The method displays a screen by it's name.
		 * Checks if the screen had already been loaded.
		 * If there is more than one screen then the new screen is added second and the current is removed.
		 * If there is'nt any screen displayed then the new screen is added to the root.
		 * @param name
		 * @return true if setted, false if not
		 */
		public boolean setScreen (final String name)
		{
			if (screens.get(name) != null) // Screen loaded
			{
				final DoubleProperty opacity = opacityProperty();

				if (!getChildren().isEmpty()) // If there is more than one screen
				{
					Timeline fade = new Timeline (
							new KeyFrame (Duration.ZERO, new KeyValue (opacity, 1.0)),
							new KeyFrame (new Duration (400), new EventHandler<ActionEvent>()
					{
						@Override
						public void handle (ActionEvent t)
						{
							getChildren().remove(0); // Removes the displayed screen
							getChildren().add(0, screens.get(name)); // Adds the new screen
							Timeline fadeIn = new Timeline (
									new KeyFrame (Duration.ZERO, new KeyValue (opacity, 0.0)),
									new KeyFrame (new Duration(1500), new KeyValue(opacity, 1.0)));
							fadeIn.play();
						}
					}, new KeyValue(opacity, 0.0)));
					fade.play();
				}
				else
				{
					setOpacity(0.0);
					getChildren().add(screens.get(name));
					Timeline fadeIn = new Timeline (
							new KeyFrame (Duration.ZERO, new KeyValue (opacity, 0.0)),
							new KeyFrame (new Duration(1500), new KeyValue(opacity, 1.0))); // First upload speed
					fadeIn.play();
				}
				return true;
			}
			else
			{
				System.out.println("screen has'nt been loaded!\n");
				return false;
			}
		}

		/**
		 * Method will remove the screen from collection of screens.
		 * @param name
		 * @return true if removed, false if did'nt
		 */
		public boolean unloadScreen (String name)
		{
			if (screens.remove(name) == null)
			{
				System.out.println("The screen did not exist");
				return false;
			}
			return true;
		}

	}
