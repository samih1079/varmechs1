package gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.collections15.Transformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import org.jd.gui.App;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import edu.uci.ics.jung.algorithms.filters.EdgePredicateFilter;
import edu.uci.ics.jung.algorithms.filters.VertexPredicateFilter;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.SpringLayout;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbsoluteCrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import entities.MyTreeNode;
import javafx.util.Pair;
import sim.JClass;
import sim.MiniJClass;
import sim.ProjectSim;
import sim.SDComp;
import storageUtils.ClassesRcmnd;
import storageUtils.CvisObjToJson;
import storageUtils.ESUtils;
import storageUtils.MethodsCat;
import sim.ClassSim;

public class ClusterGraph {
	private final static int winWidth = 1280;
	private final static int winHeight = 720;
	private final static Dimension winDim = new Dimension(winWidth, winHeight);
	private final static int grphWidth = winWidth - 100;
	private final static int grphHeight = winHeight - 200;
	private final static int minLength = 100; // Length for similarity 1.0
	private final static int maxLength = 500; // Length for similarity 0.0
	private final static FlowLayout flowLeft = new FlowLayout(FlowLayout.LEFT);

	private Graph<SimVertex, SimEdge> clsConGraph = new UndirectedSparseMultigraph<SimVertex, SimEdge>();
	private Graph<SimVertex, SimEdge> clsConGraph2 = clsConGraph;

	private Graph<SimVertex, SimEdge> proConGraph = new UndirectedSparseMultigraph<SimVertex, SimEdge>();
	private Graph<SimVertex, SimEdge> proConGraph2 = proConGraph;

	private Map<String, SimVertex> nameToClsVertex = new HashMap<String, SimVertex>();
	private Map<String, SimVertex> nameToProVertex = new HashMap<String, SimVertex>();

	private JButton zoomInB = new JButton("Zoom In");
	private boolean isZoomed = false;

	private String clusterName;

	private double usePre = 80;
	private double subPre = 80;
	private double overPre = 80;

	private SpringLayout<SimVertex, SimEdge> clsLayout;
	private SpringLayout<SimVertex, SimEdge> proLayout;

	private String[] program;
	private String[] path;

	private Map<ParamArr, Integer> paramIndexer;
	private JColorChooser[] edgeCC;
	private JColorChooser[] verCC;

	private JTree proTree[];

	private int programsAmount = 0;
	private Map<String, Integer> programIndexer = new HashMap<>();

	private int proGraphMul;
	private int clsGraphMul;

	private TreeSet<ParamArr> paramsSet = new TreeSet<>();
	private String[] progspairs;

	public static void main(String[] args) throws InvalidFormatException, IOException {
		System.out.println("ClusterGraph main: i am here");
		if (args.length < 1) {
			System.out.println("ClusterGraph main: i am here");
			// System.exit(1);
		}
		PrintStream out = new PrintStream(new FileOutputStream("CGV_OutputLog.txt"));
		System.setOut(out);
		System.setErr(out);
		new ClusterGraph(args);
	}

	public ClusterGraph(String[] args) throws InvalidFormatException, IOException {
		PrintStream out = new PrintStream(new FileOutputStream("CGV_OutputLog.txt"));
		progspairs=args;
		System.out.println("ClusterGraph ClusterGraph(): i am here");
		if (args.length < 1)
		{
			System.exit(1);
			System.out.println("ClusterGraph: no data to show");
			System.setOut(out);
			System.setErr(out);
			System.out.println("ClusterGraph: no data to show");

		}
		
		else {
			System.setOut(out);
			System.setErr(out);
			switch (ESUtils.EsORFiles) {
			case ESUtils.JUST_FILES:
				initializeGraph(args);
				addGraphVis();
				break;
			case ESUtils.JUST_ES:
				Thread th=new Thread() {
					public void run() {try {
						initializeProgsLevelGraphByES(args);
					} catch (JsonSyntaxException | IOException e) {
						e.printStackTrace();
					}}
				};
				th.start();
				try {
					th.join();
					addGraphVis();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			default:
				break;
			}
		
		}
	}

	private void initializeGraph(String[] args) throws JsonSyntaxException, IOException {
		int maxPrograms = args.length * 2;
		// start arrays at the maximum amount of programs
		program = new String[maxPrograms];
		path = new String[maxPrograms];
		proTree = new JTree[maxPrograms];
		for (int i = 0; i < args.length; i++) {
			System.out.println("initializeGraph:" + args[i]);
			File visFile = new File(args[i]);
			readFromFile(visFile);
		}
		int prAmount = 0;
		paramIndexer = new LinkedHashMap<>();
		while (!paramsSet.isEmpty())
			paramIndexer.put(paramsSet.pollFirst(), prAmount++);
		// resize the arrays to the right size
		program = Arrays.copyOfRange(program, 0, programsAmount);
		path = Arrays.copyOfRange(path, 0, programsAmount);
		proTree = Arrays.copyOfRange(proTree, 0, programsAmount);
		for (SimVertex ver1 : proConGraph.getVertices()) {
			for (SimVertex ver2 : proConGraph.getVertices()) {
				if (ver1 != ver2) {
					if (!proConGraph.isNeighbor(ver1, ver2))
						proConGraph.addEdge(new SimEdge(ver1.getProjectName(), ver2.getProjectName(), ver1.getName(),
								ver2.getName(), 0, 0, 0, 0, 0, SimType.NULL, new ParamArr()), ver1, ver2);
				}
			}
		}
	}
	private void initializeProgsLevelGraphByES(String[] args) throws JsonSyntaxException, IOException {
		int maxPrograms = args.length * 2;
		// start arrays at the maximum amount of programs
		program = new String[maxPrograms];
		//path = new String[maxPrograms];
		proTree = new JTree[maxPrograms];
		for (int i = 0; i < args.length; i++) {
			System.out.println("initializeGraph:" + args[i]);
		//	File visFile = new File(args[i]);
			readFromFileByESProgsLevel(args[i]);
			//readFromFileByESProgsLevel(args[i]);
		}
		int prAmount = 0;
		paramIndexer = new LinkedHashMap<>();
		while (!paramsSet.isEmpty())
			paramIndexer.put(paramsSet.pollFirst(), prAmount++);
		// resize the arrays to the right size
		program = Arrays.copyOfRange(program, 0, programsAmount);
		//path = Arrays.copyOfRange(path, 0, programsAmount);
		proTree = Arrays.copyOfRange(proTree, 0, programsAmount);
		for (SimVertex ver1 : proConGraph.getVertices()) {
			for (SimVertex ver2 : proConGraph.getVertices()) {
				if (ver1 != ver2) {
					if (!proConGraph.isNeighbor(ver1, ver2))
						proConGraph.addEdge(new SimEdge(ver1.getProjectName(), ver2.getProjectName(), ver1.getName(),
								ver2.getName(), 0, 0, 0, 0, 0, SimType.NULL, new ParamArr()), ver1, ver2);
				}
			}
		}
	}
	private void initializeClassesGraphByES(String[] args) throws JsonSyntaxException, IOException {
		int maxPrograms = args.length * 2;
		// start arrays at the maximum amount of programs
//		program = new String[maxPrograms];
//		//path = new String[maxPrograms];
//		proTree = new JTree[maxPrograms];
		for (int i = 0; i < args.length; i++) {
			System.out.println("initializeGraph:" + args[i]);
		//	File visFile = new File(args[i]);
			readFromFileByESClassesLevel(args[i]);
			//readFromFileByESProgsLevel(args[i]);
		}
//		int prAmount = 0;
//		paramIndexer = new LinkedHashMap<>();
//		while (!paramsSet.isEmpty())
//			paramIndexer.put(paramsSet.pollFirst(), prAmount++);
//		// resize the arrays to the right size
//		program = Arrays.copyOfRange(program, 0, programsAmount);
//		//path = Arrays.copyOfRange(path, 0, programsAmount);
//		proTree = Arrays.copyOfRange(proTree, 0, programsAmount);
//		for (SimVertex ver1 : proConGraph.getVertices()) {
//			for (SimVertex ver2 : proConGraph.getVertices()) {
//				if (ver1 != ver2) {
//					if (!proConGraph.isNeighbor(ver1, ver2))
//						proConGraph.addEdge(new SimEdge(ver1.getProjectName(), ver2.getProjectName(), ver1.getName(),
//								ver2.getName(), 0, 0, 0, 0, 0, SimType.NULL, new ParamArr()), ver1, ver2);
//				}
//			}
//		}
	}
	private void addGraphVis() {
		clusterName = "";
		for (int i = 0; i < programsAmount; i++)
			clusterName += program[i] + " ";
		// buildGraph();
		clsGraphMul = Math.max(nameToClsVertex.size() / 20, 1);
		clsLayout = new SpringLayout<SimVertex, SimEdge>(clsConGraph, new WeightedLengthTransformer());
		clsLayout.setSize(new Dimension(grphWidth * clsGraphMul, grphHeight * clsGraphMul));
		clsLayout.setForceMultiplier(0.99);
		clsLayout.setRepulsionRange(150 * clsGraphMul);
		proGraphMul = Math.max(proConGraph.getVertexCount() / 20, 1);
		proLayout = new SpringLayout<SimVertex, SimEdge>(proConGraph, new WeightedLengthTransformer());
		proLayout.setSize(new Dimension(grphWidth * proGraphMul, grphHeight * proGraphMul));
		proLayout.setForceMultiplier(0.99);
		proLayout.setRepulsionRange(150 * proGraphMul);
		for (int i = 0; i < programsAmount; i++) {
			proTree[i].setSelectionRow(0);
			proTree[i].getSelectionModel().addTreeSelectionListener(e -> reFilter());
		}
		reFilter();
		JPanel treePanel = new JPanel();
		treePanel.setLayout(flowLeft);
		Dimension treeDim = new Dimension(grphWidth / 4, grphHeight - 30);
		for (int i = 0; i < programsAmount; i++) {
			JScrollPane treeScroll = new JScrollPane(proTree[i]);
			treeScroll.setPreferredSize(treeDim);
			treePanel.add(treeScroll);
		}
		JFrame treeFrame = new JFrame("Filter By Files");
		treeFrame.setLayout(flowLeft);
		JScrollPane treeScrollPanel = new JScrollPane(treePanel);
		treeScrollPanel.setPreferredSize(new Dimension(grphWidth, grphHeight));
		treeFrame.add(treeScrollPanel);
		treeFrame.pack();
		JPanel vertColorPanel = new JPanel();
		setVertColorPanel(vertColorPanel);
		JPanel edColorPanel = new JPanel();
		setEdgesColorPanel(edColorPanel);
		SimilarityVV vv = new SimilarityVV(proLayout);
		vv.setPreferredSize(new Dimension(grphWidth, grphHeight)); // Sets the viewing area size
		vv.scaleToLayout(new AbsoluteCrossoverScalingControl());
		JToggleButton hideVer = new JToggleButton("Hide classes");
		JButton advFilter = new JButton("Filter Files");
		advFilter.addActionListener(e -> treeFrame.setVisible(true));
		vv.getRenderContext().setVertexIncludePredicate(
				v -> !isZoomed || !(hideVer.isSelected()) || (clsConGraph2.degree(v.element) > 0));
		vv.getPickedEdgeState().addItemListener(e -> {
			SimEdge edge = (SimEdge) e.getItem();
			if (vv.getPickedEdgeState().isPicked(edge))
				if (isZoomed)
					showEdgeInfo(edge);
				else
					showClassesCon(vv, edge);
		});
		ChangeListener useListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				JSlider source = (JSlider) event.getSource();
				usePre = source.getValue();
				reFilter();
			}
		};
		ChangeListener subListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				JSlider source = (JSlider) event.getSource();
				subPre = source.getValue();
				reFilter();
			}
		};
		ChangeListener overListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				JSlider source = (JSlider) event.getSource();
				overPre = source.getValue();
				reFilter();
			}
		};
		JSlider useSlider = new JSlider();
		addSliderPreProp(useSlider);
		useSlider.addChangeListener(useListener);
		JSlider subSlider = new JSlider();
		addSliderPreProp(subSlider);
		subSlider.addChangeListener(subListener);
		JSlider overSlider = new JSlider();
		addSliderPreProp(overSlider);
		overSlider.addChangeListener(overListener);
		// structSlider.setPreferredSize(new Dimension(winWidth/5,50));
		JToggleButton pB = new JToggleButton("Pause");
		pB.addActionListener(a -> clsLayout.lock(pB.isSelected()));
		zoomInB.addActionListener(a -> {
			System.out.println("zoom");
			if (isZoomed)
				showProjectCon(vv);
			else
				showClassesCon(vv);
		});
		JPanel topPanel = new JPanel();
		topPanel.setLayout(flowLeft);
		// topPanel.setMaximumSize(new Dimension(winWidth,50));
		topPanel.add(new JLabel("<html>Parametric filter<br>(PARAM)</html>"));
		topPanel.add(useSlider);
		topPanel.add(new JLabel("<html>Sub-Typing filter<br>(ST)</html>"));
		topPanel.add(subSlider);
		topPanel.add(new JLabel("<html>Overloading filter<br>(OVER)</html>"));
		topPanel.add(overSlider);
		topPanel.add(zoomInB);
		topPanel.add(pB);
		topPanel.add(hideVer);
		topPanel.add(advFilter);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(topPanel);
		mainPanel.add(new GraphZoomScrollPane(vv), BorderLayout.CENTER);
		mainPanel.add(edColorPanel);
		mainPanel.add(vertColorPanel);
		// TODO add similarity panel****
		JPanel pnlSimClustring = new JPanel();
		setSemilarityClsutringPanel(pnlSimClustring);
		JScrollPane scrSimpnl = new JScrollPane(pnlSimClustring);
		// mainPanel.add(scrSimpnl);
		JPanel mainPanel2 = new JPanel();
		mainPanel2.setLayout(new BoxLayout(mainPanel2, BoxLayout.X_AXIS));
		mainPanel2.add(mainPanel);
		mainPanel2.add(scrSimpnl);

		JScrollPane scrollPane = new JScrollPane(mainPanel2);// mainPanel
		// scrollPane.setPreferredSize(winDim);
		scrollPane.setPreferredSize(new Dimension(getScreenWorkingWidth() * 3 / 4, getScreenWorkingHeight() * 3 / 4));
		// ***
		JFrame frame = new JFrame(clusterName);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.add(scrollPane);
		frame.pack();
		frame.setVisible(true);
//	    layout.lock(true);
		System.out.println(nameToClsVertex.size());
	}
	private void addGraphVisPro() {
			clusterName = "";
			for (int i = 0; i < programsAmount; i++)
				clusterName += program[i] + " ";
			// buildGraph();
	//		clsGraphMul = Math.max(nameToClsVertex.size() / 20, 1);
	//		clsLayout = new SpringLayout<SimVertex, SimEdge>(clsConGraph, new WeightedLengthTransformer());
	//		clsLayout.setSize(new Dimension(grphWidth * clsGraphMul, grphHeight * clsGraphMul));
	//		clsLayout.setForceMultiplier(0.99);
	//		.setRepulsionRange(150 * clsGraphMul);
			proGraphMul = Math.max(proConGraph.getVertexCount() / 20, 1);
			proLayout = new SpringLayout<SimVertex, SimEdge>(proConGraph, new WeightedLengthTransformer());
			proLayout.setSize(new Dimension(grphWidth * proGraphMul, grphHeight * proGraphMul));
			proLayout.setForceMultiplier(0.99);
			proLayout.setRepulsionRange(150 * proGraphMul);
			//fileFilter()
			JPanel vertColorPanel = new JPanel();
			setVertColorPanel(vertColorPanel);
			JPanel edColorPanel = new JPanel();
			setEdgesColorPanel(edColorPanel);
			SimilarityVV vv = new SimilarityVV(proLayout);
			vv.setPreferredSize(new Dimension(grphWidth, grphHeight)); // Sets the viewing area size
			vv.scaleToLayout(new AbsoluteCrossoverScalingControl());
			JToggleButton hideVer = new JToggleButton("Hide classes");
			JButton advFilter = new JButton("Filter Files");
			advFilter.addActionListener(e -> fileFilter(true));
			vv.getRenderContext().setVertexIncludePredicate(
					v -> !isZoomed || !(hideVer.isSelected()) || (clsConGraph2.degree(v.element) > 0));
			vv.getPickedEdgeState().addItemListener(e -> {
				SimEdge edge = (SimEdge) e.getItem();
				if (vv.getPickedEdgeState().isPicked(edge))
					if (isZoomed)
						showEdgeInfo(edge);
					else
						showClassesCon(vv, edge);
			});
			ChangeListener useListener = new ChangeListener() {
				public void stateChanged(ChangeEvent event) {
					JSlider source = (JSlider) event.getSource();
					usePre = source.getValue();
					reFilter();
				}
			};
			ChangeListener subListener = new ChangeListener() {
				public void stateChanged(ChangeEvent event) {
					JSlider source = (JSlider) event.getSource();
					subPre = source.getValue();
					reFilter();
				}
			};
			ChangeListener overListener = new ChangeListener() {
				public void stateChanged(ChangeEvent event) {
					JSlider source = (JSlider) event.getSource();
					overPre = source.getValue();
					reFilter();
				}
			};
			JSlider useSlider = new JSlider();
			addSliderPreProp(useSlider);
			useSlider.addChangeListener(useListener);
			JSlider subSlider = new JSlider();
			addSliderPreProp(subSlider);
			subSlider.addChangeListener(subListener);
			JSlider overSlider = new JSlider();
			addSliderPreProp(overSlider);
			overSlider.addChangeListener(overListener);
			// structSlider.setPreferredSize(new Dimension(winWidth/5,50));
//			JToggleButton pB = new JToggleButton("Pause");
//			pB.addActionListener(a -> clsLayout.lock(pB.isSelected()));
//			zoomInB.addActionListener(a -> {
//				System.out.println("zoom");
//				if (isZoomed)
//					showProjectCon(vv);
//				else
//					showClassesCon(vv);
//			});
			JPanel topPanel = new JPanel();
			topPanel.setLayout(flowLeft);
			// topPanel.setMaximumSize(new Dimension(winWidth,50));
			topPanel.add(new JLabel("<html>Parametric filter<br>(PARAM)</html>"));
			topPanel.add(useSlider);
			topPanel.add(new JLabel("<html>Sub-Typing filter<br>(ST)</html>"));
			topPanel.add(subSlider);
			topPanel.add(new JLabel("<html>Overloading filter<br>(OVER)</html>"));
			topPanel.add(overSlider);
			topPanel.add(zoomInB);
			//topPanel.add(pB);
			topPanel.add(hideVer);
			topPanel.add(advFilter);
			JPanel mainPanel = new JPanel();
			mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
			mainPanel.add(topPanel);
			mainPanel.add(new GraphZoomScrollPane(vv), BorderLayout.CENTER);
			mainPanel.add(edColorPanel);
			mainPanel.add(vertColorPanel);
			// TODO samih add similarity panel****
			JPanel pnlSimClustring = new JPanel();
			setSemilarityClsutringPanel(pnlSimClustring);
			JScrollPane scrSimpnl = new JScrollPane(pnlSimClustring);
			// mainPanel.add(scrSimpnl);
			JPanel mainPanel2 = new JPanel();
			mainPanel2.setLayout(new BoxLayout(mainPanel2, BoxLayout.X_AXIS));
			mainPanel2.add(mainPanel);
			mainPanel2.add(scrSimpnl);
	
			JScrollPane scrollPane = new JScrollPane(mainPanel2);// mainPanel
			// scrollPane.setPreferredSize(winDim);
			scrollPane.setPreferredSize(new Dimension(getScreenWorkingWidth() * 5 / 6, getScreenWorkingHeight() * 5 / 6));
			// ***
			JFrame frame = new JFrame(clusterName);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.add(scrollPane);
			frame.pack();
			frame.setVisible(true);
	//	    layout.lock(true);
			System.out.println(nameToClsVertex.size());
		}

	public void fileFilter(boolean visible)
	{
		for (int i = 0; i < programsAmount; i++) {
			proTree[i].setSelectionRow(0);
			proTree[i].getSelectionModel().addTreeSelectionListener(e -> reFilter());
		}
		reFilter();
		JPanel treePanel = new JPanel();
		treePanel.setLayout(flowLeft);
		Dimension treeDim = new Dimension(grphWidth / 4, grphHeight - 30);
		for (int i = 0; i < programsAmount; i++) {
			JScrollPane treeScroll = new JScrollPane(proTree[i]);
			treeScroll.setPreferredSize(treeDim);
			treePanel.add(treeScroll);
		}
		JFrame treeFrame = new JFrame("Filter By Files");
		treeFrame.setLayout(flowLeft);
		JScrollPane treeScrollPanel = new JScrollPane(treePanel);
		treeScrollPanel.setPreferredSize(new Dimension(grphWidth, grphHeight));
		treeFrame.add(treeScrollPanel);
		treeFrame.pack();
		treeFrame.setVisible(visible);
	}
	private void readFromFileByESProgsLevel(String visFile) throws JsonSyntaxException, IOException {
		 ESUtils es=ESUtils.getInstance();
		Gson gson = new Gson();
		String[] proj1_2=visFile.split("_");
		String pro1 =proj1_2[0];
		String pro2 =proj1_2[1];
		//BufferedReader reader = new BufferedReader(new FileReader(visFile));
//		CvisObjToJson cvisObjToJson=ESUtils.getInstance()
//				.read2ProgCvis(ESUtils.BaseINDEX, ESUtils.TYPe_CVIS, visFile);
		Integer[] params = es.getParams(pro1,pro2,ESUtils.PARAMS_Index);
		ParamArr paramArr = new ParamArr(params);
		if (paramsSet.contains(paramArr))
			paramArr = paramsSet.floor(paramArr);
		else
			paramsSet.add(paramArr);		
		Integer pro1Index = programIndexer.get(pro1);
		Integer pro2Index = programIndexer.get(pro2);
		if (pro1Index == null) {
			pro1Index = programsAmount++;
			program[pro1Index] = pro1;
			//path[pro1Index] = visFile.getParent();
			programIndexer.put(pro1, pro1Index);
			//MiniJClass[] pro1Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<MiniJClass> pro1Arr=es.getMiniClasses(pro1, null);
			List<SimVertex> vetx1 = new LinkedList<>();
			for (MiniJClass cls : pro1Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro1, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro1Index, ver);
				vetx1.add(ver);
			}
			proTree[pro1Index] = setTree(vetx1, pro1.replace('.', ','));
			nameToProVertex.put(pro1, new ProjectVertex(pro1, pro1Arr));
		} 
		if (pro2Index == null) {
			pro2Index = programsAmount++;
			program[pro2Index] = pro2;
			//path[pro2Index] = visFile.getParent();
			programIndexer.put(pro2, pro2Index);
			//MiniJClass[] pro2Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<MiniJClass> pro2Arr=es.getMiniClasses(pro2, null);
			List<SimVertex> vetx2 = new LinkedList<>();
			for (MiniJClass cls : pro2Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro2, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro2Index, ver);
				vetx2.add(ver);
			}
			proTree[pro2Index] = setTree(vetx2, pro2.replace('.', ','));
			nameToProVertex.put(pro2, new ProjectVertex(pro2, pro2Arr));
		}
	
		//SDComp[] compRes = gson.fromJson(reader.readLine(), SDComp[].class);
//		 List<SDComp> compRes=cvisObjToJson.reccs;
//		for (SDComp res : compRes) {
//			String cls1 = res.getClass1();
//			String cls2 = res.getClass2();
//			int usePre = (int) (100 * res.getUsePart());
//			int refPre = (int) (100 * res.getRefPart());
//			int extPre = (int) (100 * res.getExtPart());
//			int refExtPre = (int) (100 * res.getRefExtPart());
//			int overPre = (int) (100 * res.getOverloadingPart());
//			SimEdge useEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, usePre, 0, 0, 0, 0, SimType.PARAM, paramArr);
//			SimEdge subEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, refPre, extPre, refExtPre, 0, SimType.ST,
//					paramArr);
//			SimEdge overEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, 0, 0, 0, overPre, SimType.OVER, paramArr);
//			res.getMethodSim().forEach(ms -> {
//				// System.out.println(ms.getMethod1()+" "+ms.getMethod2());
//				if (ms.getType().equals(ClassSim.USE))
//					useEdge.addToCluster(ms);
//				else if (ms.getType().equals(ClassSim.OVERLOADING))
//					overEdge.addToCluster(ms);
//				else
//					subEdge.addToCluster(ms);
//			});
//			SimVertex cls1Ver = nameToClsVertex.get(cls1 + "##" + pro1Index);
//			SimVertex cls2Ver = nameToClsVertex.get(cls2 + "##" + pro2Index);
//			if (cls1Ver != null && cls2Ver != null) {
//				clsConGraph.addEdge(useEdge, cls1Ver, cls2Ver);
//				clsConGraph.addEdge(subEdge, cls1Ver, cls2Ver);
//				clsConGraph.addEdge(overEdge, cls1Ver, cls2Ver);
//			} else
//				System.out.println(cls1Ver + " " + cls1 + " || " + cls2Ver + " " + cls2);
//		}
		SimVertex pro1Ver = nameToProVertex.get(pro1);
		SimVertex pro2Ver = nameToProVertex.get(pro2);
	
		if (pro1Ver != null && pro2Ver != null) {
			ProjectSim projectSim = null;
			try {
				projectSim = es.getProjectSim(pro1,pro2,null);
				int totMethods = projectSim.getTot();
				SimEdge useEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 100 * projectSim.getUse() / totMethods, 0, 0,
						0, 0, SimType.PARAM, paramArr);
				SimEdge subEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 100 * projectSim.getRef() / totMethods,
						100 * projectSim.getExt() / totMethods, 100 * projectSim.getRefExt() / totMethods, 0,
						SimType.ST, paramArr);
				SimEdge overEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 0, 0, 0,
						100 * projectSim.getOverloading() / totMethods, SimType.OVER, paramArr);
				proConGraph.addEdge(useEdge, pro1Ver, pro2Ver);
				proConGraph.addEdge(subEdge, pro1Ver, pro2Ver);
				proConGraph.addEdge(overEdge, pro1Ver, pro2Ver);
			} catch (Exception e) {
				proConGraph.addEdge(new SimEdge(pro1, pro2, pro1, pro2, 0, 0, 0, 0, 0, SimType.UNDEFINED, paramArr),
						pro1Ver, pro2Ver);
			}
		} else
			System.out.println(pro1Ver + " " + pro1 + " || " + pro2Ver + " " + pro2);
		//reader.close();
	}
	private void readFromFileByESClassesLevel(String visFile) throws JsonSyntaxException, IOException {
		 ESUtils es=ESUtils.getInstance();
		Gson gson = new Gson();
		String[] proj1_2=visFile.split("_");
		String pro1 =proj1_2[0];
		String pro2 =proj1_2[1];
		//BufferedReader reader = new BufferedReader(new FileReader(visFile));
//		CvisObjToJson cvisObjToJson=ESUtils.getInstance()
//				.read2ProgCvis(ESUtils.BaseINDEX, ESUtils.TYPe_CVIS, visFile);
		Integer[] params = es.getParams(pro1,pro2,ESUtils.PARAMS_Index);
		ParamArr paramArr = new ParamArr(params);
		if (paramsSet.contains(paramArr))
			paramArr = paramsSet.floor(paramArr);
		else
			paramsSet.add(paramArr);		
		Integer pro1Index = programIndexer.get(pro1);
		Integer pro2Index = programIndexer.get(pro2);
		if (pro1Index == null) {
			pro1Index = programsAmount++;
			program[pro1Index] = pro1;
			//path[pro1Index] = visFile.getParent();
			programIndexer.put(pro1, pro1Index);
			//MiniJClass[] pro1Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<MiniJClass> pro1Arr=es.getMiniClasses(pro1, null);
			List<SimVertex> vetx1 = new LinkedList<>();
			for (MiniJClass cls : pro1Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro1, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro1Index, ver);
				vetx1.add(ver);
			}
			proTree[pro1Index] = setTree(vetx1, pro1.replace('.', ','));
			nameToProVertex.put(pro1, new ProjectVertex(pro1, pro1Arr));
		} 
		if (pro2Index == null) {
			pro2Index = programsAmount++;
			program[pro2Index] = pro2;
			//path[pro2Index] = visFile.getParent();
			programIndexer.put(pro2, pro2Index);
			//MiniJClass[] pro2Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<MiniJClass> pro2Arr=es.getMiniClasses(pro2, null);
			List<SimVertex> vetx2 = new LinkedList<>();
			for (MiniJClass cls : pro2Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro2, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro2Index, ver);
				vetx2.add(ver);
			}
			proTree[pro2Index] = setTree(vetx2, pro2.replace('.', ','));
			nameToProVertex.put(pro2, new ProjectVertex(pro2, pro2Arr));
		}
	
		//SDComp[] compRes = gson.fromJson(reader.readLine(), SDComp[].class);
		 List<ClassesRcmnd> compRes=es.getCLassRcmnds(pro1, pro2, ESUtils.CLASSES_RCMNDS_Index);
		for (ClassesRcmnd res : compRes) {
			String cls1 = res.getClass1();
			String cls2 = res.getClass2();
			int usePre = (int) (100 * res.getSdCompBase().getUsePart());
			int refPre = (int) (100 * res.getSdCompBase().getRefPart());
			int extPre = (int) (100 * res.getSdCompBase().getExtPart());
			int refExtPre = (int) (100 * res.getSdCompBase().getRefExtPart());
			int overPre = (int) (100 * res.getSdCompBase().getOverloadingPart());
			SimEdge useEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, usePre, 0, 0, 0, 0, SimType.PARAM, paramArr);
			SimEdge subEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, refPre, extPre, refExtPre, 0, SimType.ST,
					paramArr);
			SimEdge overEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, 0, 0, 0, overPre, SimType.OVER, paramArr);
			List<MethodsCat> mslst=es.getMethodsCat(pro1, pro2, cls1, cls2, es.METHODS_CAT_Index);
					mslst.forEach(ms -> {
				// System.out.println(ms.getMethod1()+" "+ms.getMethod2());
				if (ms.getType().equals(ClassSim.USE))
					useEdge.addToCluster(ms);
				else if (ms.getType().equals(ClassSim.OVERLOADING))
					overEdge.addToCluster(ms);
				else
					subEdge.addToCluster(ms);
			});
			SimVertex cls1Ver = nameToClsVertex.get(cls1 + "##" + pro1Index);
			SimVertex cls2Ver = nameToClsVertex.get(cls2 + "##" + pro2Index);
			if (cls1Ver != null && cls2Ver != null) {
				clsConGraph.addEdge(useEdge, cls1Ver, cls2Ver);
				clsConGraph.addEdge(subEdge, cls1Ver, cls2Ver);
				clsConGraph.addEdge(overEdge, cls1Ver, cls2Ver);
			} else
				System.out.println(cls1Ver + " " + cls1 + " || " + cls2Ver + " " + cls2);
		}
//		SimVertex pro1Ver = nameToProVertex.get(pro1);
//		SimVertex pro2Ver = nameToProVertex.get(pro2);
//	
//		if (pro1Ver != null && pro2Ver != null) {
//			ProjectSim projectSim = null;
//			try {
//				projectSim = es.getProjectSim(pro1,pro2,null);
//				int totMethods = projectSim.getTot();
//				SimEdge useEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 100 * projectSim.getUse() / totMethods, 0, 0,
//						0, 0, SimType.PARAM, paramArr);
//				SimEdge subEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 100 * projectSim.getRef() / totMethods,
//						100 * projectSim.getExt() / totMethods, 100 * projectSim.getRefExt() / totMethods, 0,
//						SimType.ST, paramArr);
//				SimEdge overEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 0, 0, 0,
//						100 * projectSim.getOverloading() / totMethods, SimType.OVER, paramArr);
//				proConGraph.addEdge(useEdge, pro1Ver, pro2Ver);
//				proConGraph.addEdge(subEdge, pro1Ver, pro2Ver);
//				proConGraph.addEdge(overEdge, pro1Ver, pro2Ver);
//			} catch (Exception e) {
//				proConGraph.addEdge(new SimEdge(pro1, pro2, pro1, pro2, 0, 0, 0, 0, 0, SimType.UNDEFINED, paramArr),
//						pro1Ver, pro2Ver);
//			}
//		} else
//			System.out.println(pro1Ver + " " + pro1 + " || " + pro2Ver + " " + pro2);
		//reader.close();
	}

	private void readFromFile(File visFile) throws JsonSyntaxException, IOException {
		Gson gson = new Gson();
		BufferedReader reader = new BufferedReader(new FileReader(visFile));
		Integer[] params = gson.fromJson(reader.readLine(), Integer[].class);
		ParamArr paramArr = new ParamArr(params);
		if (paramsSet.contains(paramArr))
			paramArr = paramsSet.floor(paramArr);
		else
			paramsSet.add(paramArr);
		String pro1 = reader.readLine();
		String pro2 = reader.readLine();
		Integer pro1Index = programIndexer.get(pro1);
		Integer pro2Index = programIndexer.get(pro2);
		if (pro1Index == null) {
			pro1Index = programsAmount++;
			program[pro1Index] = pro1;
			path[pro1Index] = visFile.getParent();
			programIndexer.put(pro1, pro1Index);
			MiniJClass[] pro1Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<SimVertex> vetx1 = new LinkedList<>();
			for (MiniJClass cls : pro1Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro1, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro1Index, ver);
				vetx1.add(ver);
			}
			proTree[pro1Index] = setTree(vetx1, pro1.replace('.', ','));
			nameToProVertex.put(pro1, new ProjectVertex(pro1, Arrays.asList(pro1Arr)));
		} else
			reader.readLine();
		if (pro2Index == null) {
			pro2Index = programsAmount++;
			program[pro2Index] = pro2;
			path[pro2Index] = visFile.getParent();
			programIndexer.put(pro2, pro2Index);
			MiniJClass[] pro2Arr = gson.fromJson(reader.readLine(), MiniJClass[].class);
			List<SimVertex> vetx2 = new LinkedList<>();
			for (MiniJClass cls : pro2Arr) {
				String name = cls.getClassName();
				SimVertex ver = new SimVertex(name, pro2, cls.getMethods());
				nameToClsVertex.put(name + "##" + pro2Index, ver);
				vetx2.add(ver);
			}
			proTree[pro2Index] = setTree(vetx2, pro2.replace('.', ','));
			nameToProVertex.put(pro2, new ProjectVertex(pro2, Arrays.asList(pro2Arr)));
		} else
			reader.readLine();
	
		SDComp[] compRes = gson.fromJson(reader.readLine(), SDComp[].class);
	
		for (SDComp res : compRes) {
			String cls1 = res.getClass1();
			String cls2 = res.getClass2();
			int usePre = (int) (100 * res.getUsePart());
			int refPre = (int) (100 * res.getRefPart());
			int extPre = (int) (100 * res.getExtPart());
			int refExtPre = (int) (100 * res.getRefExtPart());
			int overPre = (int) (100 * res.getOverloadingPart());
			SimEdge useEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, usePre, 0, 0, 0, 0, SimType.PARAM, paramArr);
			SimEdge subEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, refPre, extPre, refExtPre, 0, SimType.ST,
					paramArr);
			SimEdge overEdge = new SimEdgeExt(pro1, pro2, cls1, cls2, 0, 0, 0, 0, overPre, SimType.OVER, paramArr);
			res.getMethodSim().forEach(ms -> {
				// System.out.println(ms.getMethod1()+" "+ms.getMethod2());
				if (ms.getType().equals(ClassSim.USE))
					useEdge.addToCluster(ms);
				else if (ms.getType().equals(ClassSim.OVERLOADING))
					overEdge.addToCluster(ms);
				else
					subEdge.addToCluster(ms);
			});
			SimVertex cls1Ver = nameToClsVertex.get(cls1 + "##" + pro1Index);
			SimVertex cls2Ver = nameToClsVertex.get(cls2 + "##" + pro2Index);
			if (cls1Ver != null && cls2Ver != null) {
				clsConGraph.addEdge(useEdge, cls1Ver, cls2Ver);
				clsConGraph.addEdge(subEdge, cls1Ver, cls2Ver);
				clsConGraph.addEdge(overEdge, cls1Ver, cls2Ver);
			} else
				System.out.println(cls1Ver + " " + cls1 + " || " + cls2Ver + " " + cls2);
		}
		SimVertex pro1Ver = nameToProVertex.get(pro1);
		SimVertex pro2Ver = nameToProVertex.get(pro2);
	
		if (pro1Ver != null && pro2Ver != null) {
			ProjectSim projectSim = null;
			try {
				projectSim = gson.fromJson(reader.readLine(), ProjectSim.class);
				int totMethods = projectSim.getTot();
				SimEdge useEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 100 * projectSim.getUse() / totMethods, 0, 0,
						0, 0, SimType.PARAM, paramArr);
				SimEdge subEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 100 * projectSim.getRef() / totMethods,
						100 * projectSim.getExt() / totMethods, 100 * projectSim.getRefExt() / totMethods, 0,
						SimType.ST, paramArr);
				SimEdge overEdge = new SimEdgeExt(pro1, pro2, pro1, pro2, 0, 0, 0, 0,
						100 * projectSim.getOverloading() / totMethods, SimType.OVER, paramArr);
				proConGraph.addEdge(useEdge, pro1Ver, pro2Ver);
				proConGraph.addEdge(subEdge, pro1Ver, pro2Ver);
				proConGraph.addEdge(overEdge, pro1Ver, pro2Ver);
			} catch (Exception e) {
				proConGraph.addEdge(new SimEdge(pro1, pro2, pro1, pro2, 0, 0, 0, 0, 0, SimType.UNDEFINED, paramArr),
						pro1Ver, pro2Ver);
			}
		} else
			System.out.println(pro1Ver + " " + pro1 + " || " + pro2Ver + " " + pro2);
		reader.close();
	}

	private void setSemilarityClsutringPanel(JPanel pnlSimClustring) {
		pnlSimClustring.setBackground(Color.BLUE);
		Object[] columnNames = { "Program", "Class", "MColor", "PS", "SS", "OS", "Choose" };
		Object[][] data = {
				{ "testp1", "trstCls1", new Integer(2), new Double(0.50), new Double(0.20), new Double(0.30), false },
				{ "testp2", "trstCls2", new Integer(3), new Double(0.30), new Double(0.40), new Double(0.40), false }

		};
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		JTable table = new JTable(model) {

			// private static final long serialVersionUID = 1L;

			/*
			 * @Override public Class getColumnClass(int column) { return getValueAt(0,
			 * column).getClass(); }
			 */
			@Override
			public Class getColumnClass(int column) {
				switch (column) {
				case 0:
					return String.class;
				case 1:
					return String.class;
				case 2:
					return Integer.class;
				case 3:
					return Double.class;
				case 4:
					return Double.class;
				case 5:
					return Double.class;
				default:
					return Boolean.class;
				}
			}
		};
		model.addRow(new Object[] { "testp1", "trstCls1", new Integer(2), new Double(0.50), new Double(0.20),
				new Double(0.30), false });
		table.setFont(new Font("Serif", Font.PLAIN, 14));
		table.getTableHeader().setFont(new Font("SansSerif", Font.ITALIC, 18));
		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		JScrollPane scrollPane = new JScrollPane(table);
		pnlSimClustring.add(scrollPane);
	}

	private void showProjectCon(VisualizationViewer<SimVertex, SimEdge> vv) {
		vv.setGraphLayout(proLayout);
		new CrossoverScalingControl().scale(vv, ((float) clsGraphMul) / proGraphMul, new Point2D.Float(0, 0));
		zoomInB.setText("Zoom In");
		isZoomed = false;
	}

	private void showClassesCon(VisualizationViewer<SimVertex, SimEdge> vv, SimEdge... edges) {
		if (edges.length == 0)
			for (JTree tree : proTree)
				tree.setSelectionRow(0);
		else {
			for (JTree tree : proTree)
				tree.setSelectionRow(1);
			for (SimEdge edge : edges) {
				proTree[programIndexer.get(edge.getPro1())].setSelectionRow(0);
				proTree[programIndexer.get(edge.getPro2())].setSelectionRow(0);
			}
		}
		vv.setGraphLayout(clsLayout);
		new CrossoverScalingControl().scale(vv, ((float) proGraphMul) / clsGraphMul, new Point2D.Float(0, 0));
		zoomInB.setText("Zoom Out");
		isZoomed = true;
	}

	private void setVertColorPanel(JPanel vertColorPanel) {
		verCC = new JColorChooser[programsAmount];
		vertColorPanel.setLayout(flowLeft);
		vertColorPanel.add(new JLabel("Change color of classes from: "));
		JButton[] vCB = new JButton[programsAmount];
		ActionListener colorChooserListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton) e.getSource();
				String command = button.getActionCommand();
				JFrame colorFrame = new JFrame("Pick a color for the " + button.getText());
				int commandIndex = Integer.parseInt(command);
				colorFrame.add(verCC[commandIndex]);
				colorFrame.pack();
				colorFrame.setVisible(true);
			}
		};
		AncestorListener colorChangeListener = new AncestorListener() {
			@Override
			public void ancestorAdded(AncestorEvent arg0) {
			}

			@Override
			public void ancestorMoved(AncestorEvent arg0) {
			}

			@Override
			public void ancestorRemoved(AncestorEvent arg0) {
				for (int i = 0; i < programsAmount; i++) {
					JColorChooser vColor = verCC[i];
					JButton vCBI = vCB[i];
					vCBI.setBackground(vColor.getColor());
					vCBI.setForeground(Color.getColor(null, 0xFFFFFF - vColor.getColor().getRGB()));
				}
			}
		};
		for (int i = 0; i < programsAmount; i++) {
			JButton vCBI = new JButton(program[i]);
			JColorChooser ccI = new JColorChooser(colorize(i));
			ccI.addAncestorListener(colorChangeListener);
			vCBI.setActionCommand(i + "");
			Color color = ccI.getColor();
			verCC[i] = ccI;
			vCBI.setBackground(color);
			vCBI.setForeground(Color.getColor(null, (0xFFFFFF - color.getRGB())));
			vCBI.addActionListener(colorChooserListener);
			vertColorPanel.add(vCBI);
			vCB[i] = vCBI;
		}
	}

	private Color colorize(int i) {
		int numOfColors = E_Colors.values().length;
		E_Colors color = E_Colors.values()[i];
		return i < numOfColors ? color.getColor() : color.blend();
	}

	private void setEdgesColorPanel(JPanel colorPanel) {
		int paramsAmount = paramIndexer.size();
		edgeCC = new JColorChooser[paramsAmount];
		colorPanel.setLayout(flowLeft);
		colorPanel.add(new JLabel("Change color of edges with the parameters: "));
		JButton[] vCB = new JButton[paramsAmount];
		ActionListener colorChooserListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton) e.getSource();
				String command = button.getActionCommand();
				JFrame colorFrame = new JFrame("Pick a color for the " + button.getText());
				int commandIndex = Integer.parseInt(command);
				colorFrame.add(edgeCC[commandIndex]);
				colorFrame.pack();
				colorFrame.setVisible(true);
			}
		};
		AncestorListener colorChangeListener = new AncestorListener() {
			@Override
			public void ancestorAdded(AncestorEvent arg0) {
			}

			@Override
			public void ancestorMoved(AncestorEvent arg0) {
			}

			@Override
			public void ancestorRemoved(AncestorEvent arg0) {
				for (int i = 0; i < paramsAmount; i++) {
					JColorChooser vColor = edgeCC[i];
					JButton vCBI = vCB[i];
					vCBI.setBackground(vColor.getColor());
					vCBI.setForeground(Color.getColor(null, 0xFFFFFF - vColor.getColor().getRGB()));
				}
			}
		};
		for (Entry<ParamArr, Integer> a : paramIndexer.entrySet()) {
			JButton vCBI = new JButton(a.getKey().toString());
			int theHash = a.getKey().hashCode();
			Color ccolor;
			if (theHash < 100) {
				ccolor = Color.YELLOW;
			} else
				ccolor = new Color((int) (Math.pow(theHash, 1.5)));
			JColorChooser ccI = new JColorChooser(ccolor);
			ccI.addAncestorListener(colorChangeListener);
			vCBI.setActionCommand(a.getValue() + "");
			Color color = ccI.getColor();
			edgeCC[a.getValue()] = ccI;
			vCBI.setBackground(color);
			vCBI.setForeground(Color.getColor(null, (0xFFFFFF - color.getRGB())));
			vCBI.addActionListener(colorChooserListener);
			colorPanel.add(vCBI);
			vCB[a.getValue()] = vCBI;
		}
	}

	/*
	 * private void setColorPanel(JPanel colorPanel) { edgeCC=new
	 * JColorChooser(Color.ORANGE); verCC=new JColorChooser[programsAmount];
	 * colorPanel.setLayout(flowLeft); colorPanel.add(new
	 * JLabel("Change color of: ")); JButton eCB = new JButton("Edges"); JButton[]
	 * vCB=new JButton[programsAmount]; ActionListener colorChooserListener = new
	 * ActionListener() {
	 * 
	 * @Override public void actionPerformed(ActionEvent e) { JButton button =
	 * (JButton)e.getSource(); String command=button.getActionCommand(); JFrame
	 * colorFrame = new JFrame("Pick a color for the "+button.getText());
	 * if(command.equals("edges")) colorFrame.add(edgeCC); else { int
	 * commandIndex=Integer.parseInt(command); colorFrame.add(verCC[commandIndex]);
	 * } colorFrame.pack(); colorFrame.setVisible(true); } }; AncestorListener
	 * colorChangeListener = new AncestorListener() {
	 * 
	 * @Override public void ancestorAdded(AncestorEvent arg0) { }
	 * 
	 * @Override public void ancestorMoved(AncestorEvent arg0) { }
	 * 
	 * @Override public void ancestorRemoved(AncestorEvent arg0) {
	 * eCB.setBackground(edgeCC.getColor());
	 * eCB.setForeground(Color.getColor(null,0xFFFFFF-edgeCC.getColor().getRGB()));
	 * for(int i=0;i<programsAmount;i++){ JColorChooser vColor = verCC[i]; JButton
	 * vCBI = vCB[i]; vCBI.setBackground(vColor.getColor());
	 * vCBI.setForeground(Color.getColor(null,0xFFFFFF-vColor.getColor().getRGB()));
	 * } } }; eCB.setActionCommand("edges"); eCB.setBackground(edgeCC.getColor());
	 * eCB.setForeground(Color.getColor(null,0xFFFFFF-edgeCC.getColor().getRGB()));
	 * eCB.addActionListener(colorChooserListener);
	 * edgeCC.addAncestorListener(colorChangeListener); colorPanel.add(eCB); for(int
	 * i=0;i<programsAmount;i++){ JButton vCBI = new
	 * JButton("Classes from "+program[i]); JColorChooser ccI = new
	 * JColorChooser(new Color((int) (program[i].hashCode()*(10))));
	 * ccI.addAncestorListener(colorChangeListener); vCBI.setActionCommand(i+"");
	 * Color color = ccI.getColor(); verCC[i]=ccI; vCBI.setBackground(color);
	 * vCBI.setForeground(Color.getColor(null,(0xFFFFFF-color.getRGB())));
	 * vCBI.addActionListener(colorChooserListener); colorPanel.add(vCBI); vCB[i] =
	 * vCBI; } }
	 */
	private JTree setTree(List<SimVertex> vetx, String name) {
		Map<String, Pair<List<SimVertex>, MyTreeNode>> vertxNames = new HashMap<String, Pair<List<SimVertex>, MyTreeNode>>();
		MyTreeNode root = new MyTreeNode(name);
		for (SimVertex ver : vetx) {
			String verName = ver.getName();
			String[] splited = verName.split("\\.");
			if (splited.length > 0)
				verName = splited[0];
			// System.out.println(verName+" "+Arrays.toString(splited));
			Pair<List<SimVertex>, MyTreeNode> verPair = vertxNames.get(verName);
			List<SimVertex> verList;
			if (verPair == null) {
				verList = new LinkedList<>();
				MyTreeNode child = new MyTreeNode(verName);
				root.add(child);
				vertxNames.put(verName, new Pair<>(verList, child));
			} else
				verList = verPair.getKey();
			verList.add(ver);
			// System.out.println(verList);
		}
		for (Entry<String, Pair<List<SimVertex>, MyTreeNode>> entry : vertxNames.entrySet()) {
			setNextLevel(entry.getValue(), 1);
		}
		compressRoot(root);
		root.addToFront(new MyTreeNode(".Hide Project"));
		return new JTree(root.toTreeNode());
	}

	private void setNextLevel(Pair<List<SimVertex>, MyTreeNode> pair, int i) {
		List<SimVertex> vetx = pair.getKey();
		if (vetx.isEmpty())
			return;
		Map<String, Pair<List<SimVertex>, MyTreeNode>> vertxNames = new HashMap<String, Pair<List<SimVertex>, MyTreeNode>>();
		MyTreeNode root = pair.getValue();
		for (SimVertex ver : vetx) {
			String[] splited = ver.getName().split("\\.");
			if (splited.length > i) {
				String verName = splited[i];
				Pair<List<SimVertex>, MyTreeNode> verPair = vertxNames.get(verName);
				List<SimVertex> verList;
				if (verPair == null) {
					verList = new LinkedList<>();
					MyTreeNode child = new MyTreeNode(verName);
					root.add(child);
					vertxNames.put(verName, new Pair<>(verList, child));
				} else
					verList = verPair.getKey();
				verList.add(ver);
			}
		}
		for (Entry<String, Pair<List<SimVertex>, MyTreeNode>> entry : vertxNames.entrySet()) {
			setNextLevel(entry.getValue(), i + 1);
		}
	}

	private void compressRoot(MyTreeNode root) {
		if (root.isLeaf())
			return;
		while (root.getChildrenCount() == 1) {
			MyTreeNode fChild = root.firstChild();
			root.setName(root + "." + fChild);
			root.setChildren(fChild.getChildren());
		}
		for (MyTreeNode child : root.getChildren()) {
			compressRoot(child);
		}
	}

	private String shortenClassName(String className, String projectName) {
		TreePath[] selectedPaths = null;
		for (int i = 0; i < programsAmount; i++) {
			if (projectName.equals(program[i]))
				selectedPaths = proTree[i].getSelectionPaths();
		}
		String[] splitedName = className.split("\\.");
		int splitLen = splitedName.length;
		String ancName = "";
		for (int i = getAncLen(selectedPaths, splitedName) - 1; i < splitLen - 1; i++)
			ancName += splitedName[i] + ".";
		ancName += splitedName[splitLen - 1];
		return ancName;
	}

	private int getAncLen(TreePath[] selectedPaths, String[] splitedName) {
		int sameCounter = splitedName.length;
		for (TreePath tPath : selectedPaths) {
			Object[] pathArr = tPath.getPath();
			String pathAsString = "";
			for (Object p : pathArr)
				pathAsString += p.toString() + ".";
			String[] sPath = pathAsString.split("\\.");
			int i = 1;
			int pathLen = Math.min(sameCounter, sPath.length);
			for (; i < pathLen && sPath[i].equals(splitedName[i - 1]); i++)
				;
			sameCounter = Math.min(sameCounter, i);
		}
		return sameCounter;
	}

	private void addSliderPreProp(JSlider slider) {
		slider.setPaintTicks(true);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(80);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(5);
	}

	private void showEdgeInfo(SimEdge edge) {
		System.out.println(edge);
		Forest<SimVertex, DiffStr> clustersGraph = edge.getClusters();
		System.out.println(clustersGraph.getEdgeCount());
		int sizeMul = clustersGraph.getEdgeCount() / 30 + 1;
		Dimension graphDim = new Dimension(winWidth * sizeMul, winHeight * sizeMul);
		SpringLayout<SimVertex, DiffStr> layout = new SpringLayout<SimVertex, DiffStr>(clustersGraph,
				e -> (int) (maxLength * 0.5));
		layout.setSize(graphDim);
		layout.setForceMultiplier(0.99);
		layout.setRepulsionRange(350);
		MyVV<DiffStr> vv = new MyVV<DiffStr>(layout);
		vv.setPreferredSize(winDim); // Sets the viewing area size
		vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<SimVertex>());
		vv.getRenderContext().setVertexFontTransformer(a -> new Font(null, Font.PLAIN, 14));
		vv.scaleToLayout(new AbsoluteCrossoverScalingControl());
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JButton viewSource = new JButton("View Sources");
		viewSource.addActionListener(e -> viewSources(edge));
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(viewSource);
		mainPanel.add(topPanel);
		mainPanel.add(new GraphZoomScrollPane(vv), BorderLayout.CENTER);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		layout.lock(true);
		JFrame frame = new JFrame(edge.getName());
		frame.setPreferredSize(winDim);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setVisible(true);
	}

	private void viewSources(SimEdge edge) {
		String pro1 = edge.getPro1();
		File project1base = new File(path[programIndexer.get(pro1)] + "\\" + pro1);
		String cls1name = edge.getCls1().replace('.', '\\') + ".class";
		String cls1Path = project1base.getAbsolutePath() + "\\" + cls1name;
		System.out.println(cls1Path);
		if (!new File(cls1Path).exists())
			for (File folder : project1base.listFiles()) {
				if (folder.isDirectory()) {
					cls1Path = folder.getAbsolutePath() + "\\" + cls1name;
					System.out.println(cls1Path);
					if (new File(cls1Path).exists())
						break;
				}
			}
		String pro2 = edge.getPro2();
		File project2base = new File(path[programIndexer.get(pro2)] + "\\" + pro2);
		String cls2name = edge.getCls2().replace('.', '\\') + ".class";
		String cls2Path = project2base.getAbsolutePath() + "\\" + cls2name;
		System.out.println(cls2Path);
		if (!new File(cls2Path).exists())
			for (File folder : project2base.listFiles()) {
				if (folder.isDirectory()) {
					cls2Path = folder.getAbsolutePath() + "\\" + cls2name;
					System.out.println(cls2Path);
					if (new File(cls2Path).exists())
						break;
				}
			}
		System.out.println(cls1Path + " " + cls2Path);
		try {
			startJVM(App.class, cls1Path, false, false);
			startJVM(App.class, cls2Path, false, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startJVM(Class<? extends Object> clazz, String file, boolean redirectStream, boolean waifFor)
			throws Exception {
		String separator = System.getProperty("file.separator");
		String classpath = System.getProperty("java.class.path");
		String path = System.getProperty("java.home") + separator + "bin" + separator + "java";
		ProcessBuilder processBuilder = new ProcessBuilder(path, "-cp", classpath, clazz.getCanonicalName(), file);
		processBuilder.redirectErrorStream(redirectStream);
		Process process = processBuilder.start();
		if (waifFor)
			process.waitFor();
	}

	private void reFilter() {
		VertexPredicateFilter<SimVertex, SimEdge> verPredicateFilter = new VertexPredicateFilter<SimVertex, SimEdge>(
				v -> {
					String verName = v.getName();
					TreePath[] selectedPaths = null;
					for (int i = 0; i < programsAmount; i++) {
						if (v.getProjectName().equals(program[i]))
							selectedPaths = proTree[i].getSelectionPaths();
					}
					// System.out.println(verName);
					return isInPath(verName, selectedPaths);
				});
		EdgePredicateFilter<SimVertex, SimEdge> edgePredicateFilter = new EdgePredicateFilter<SimVertex, SimEdge>(e -> {
			SimType edgeType = e.getType();
			if (edgeType.equals(SimType.PARAM))
				return e.getSim() > usePre;
			if (edgeType.equals(SimType.ST))
				return e.getSim() > subPre;
			if (edgeType.equals(SimType.OVER))
				return e.getSim() > overPre;
			if (usePre + usePre + usePre == 0)
				return edgeType.equals(SimType.UNDEFINED) || edgeType.equals(SimType.NULL);
			return false;
		});
		clsConGraph2 = verPredicateFilter.transform(clsConGraph);
		clsConGraph2 = edgePredicateFilter.transform(clsConGraph2);
		clsLayout.setGraph(clsConGraph2);
		proConGraph2 = edgePredicateFilter.transform(proConGraph);
		proLayout.setGraph(proConGraph2);
	}

	private boolean isInPath(String verName, TreePath[] selectedPaths) {
		String[] splitedName = verName.split("\\.");
		// System.out.println(Arrays.toString(selectedPaths));
		int splitLength = splitedName.length;
		for (TreePath path : selectedPaths) {
			// System.out.println(path.toString()+" "+Arrays.toString(splitedName));
			Object[] pathArr = path.getPath();
			String pathAsString = "";
			for (Object p : pathArr)
				pathAsString += p.toString() + ".";
			String[] sPath = pathAsString.split("\\.");
			int pathLength = sPath.length;
			// System.out.println(sPath[pathLength-1]);
			if (pathLength == 1)
				return true;
			if (splitLength >= pathLength - 1) {
				boolean isPrefix = true;
				for (int i = 1; i < pathLength; i++) {
					// System.out.println(sPath[i]+" "+splitedName[i-1]);
					if (!sPath[i].equals(splitedName[i - 1])) {
						isPrefix = false;
						break;
					}
				}
				if (isPrefix)
					return true;
			}
		}
		return false;
	}

	class WeightedLengthTransformer implements Transformer<SimEdge, Integer> {
		@Override
		public Integer transform(SimEdge edge) {
			double similarity = Math.min(1, ((double) edge.getSim()) / 100);
			int length = (int) (minLength + (1.0 - similarity) * (maxLength - minLength));
			return length;
		}
	}

	class VertexPaint implements Transformer<SimVertex, Paint> {
		public Paint transform(SimVertex v) {
			for (int i = 0; i < programsAmount; i++) {
				if (v.getProjectName().equals(program[i]))
					return verCC[i].getColor();
			}
			return Color.WHITE;
		}
	};

	class EdgePaint implements Transformer<SimEdge, Paint> {
		public Paint transform(SimEdge e) {
			// System.out.println(edgeCC[paramIndexer.get(e.getPr())].getColor().toString());
			Color color = edgeCC[paramIndexer.get(e.getPr())].getColor();
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), 200);
			if (e.getUseSim() > 0)
				color = color.darker();
			else if (e.getOverSim() > 0)
				color = color.brighter().brighter();
			return color;
		}
	};

	class VertexSizeShaper implements Transformer<SimVertex, Shape> {
		@Override
		public Shape transform(SimVertex v) {
			int size = v.getSize() + 10;
			return new Ellipse2D.Double(-size / 2, -size / 2, size, size);
		}
	};

	static class EdgeSimStroke implements Transformer<SimEdge, Stroke> {
		public Stroke transform(SimEdge edge) {
			double similarity = Math.min(100, edge.getSim());
			similarity /= 100;
			int width = (int) (2 + similarity * 5);
			return new BasicStroke(width);
		}
	};

	class MyVV<E> extends VisualizationViewer<SimVertex, E> {
		private static final long serialVersionUID = -578022647294057763L;

		public MyVV(Layout<SimVertex, E> layout) {
			super(layout);
			setGraphMouse(new MultiMouse<String, DiffStr>());
			getRenderContext().setVertexFillPaintTransformer(new VertexPaint());
			getRenderContext().setLabelOffset(20);
			getRenderContext().setEdgeFontTransformer(a -> new Font(null, Font.BOLD, 12));
			getRenderContext().setEdgeArrowPredicate(a -> false);
			getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<>());
		}
	}

	class SimilarityVV extends MyVV<SimEdge> {
		private static final long serialVersionUID = 3651756666128082531L;

		public SimilarityVV(Layout<SimVertex, SimEdge> layout) {
			super(layout);
			getRenderContext().setEdgeDrawPaintTransformer(new EdgePaint());
			getRenderContext().setVertexShapeTransformer(new VertexSizeShaper());
			getRenderContext().setEdgeStrokeTransformer(new EdgeSimStroke());
			getRenderContext().setVertexLabelTransformer(v -> shortenClassName(v.getName(), v.getProjectName()));
			setEdgeToolTipTransformer(e -> e.getEdgeInfo());
			setVertexToolTipTransformer(v -> v.htmlDesc());
		}
	}

	public static int getScreenWorkingWidth() {
		return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
	}

	public static int getScreenWorkingHeight() {
		return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
	}

}
