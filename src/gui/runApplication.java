package gui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import javafx.beans.property.DoubleProperty;
import javafx.scene.control.ProgressBar;

import java.util.Set;

import sim.AClass;
import sim.CommutativePair;
import sim.Deep;
import sim.JClass;
import sim.JMethod;
import sim.JSONRead;
import sim.JSONStruct;
import sim.MiniJClass;
import sim.MiniJMethod;
import sim.Parameter;
import sim.ReaderHelper;
import sim.ScriptMaker;
import sim.Shallow;
import sim.Similarity;
import sim.XMIRead;
import sim.XMIStruct;
import sim.XmiAdditionalInfo;
import storageUtils.CvisObjToJson;

import java.io.*;

public class runApplication
{

	public static List<JClass> preProcess(String folder, String progName) throws Exception
	{
		ScriptMaker.preProccessProject(".*", folder);
		List<JClass> jsonClasses = JSONRead.readFromFile(folder+"//json", progName);	
		XmiAdditionalInfo xmiProgram = XMIRead.readFromFile(folder, progName);	
		for(Entry<String, String> entry:xmiProgram.getClassHirarchy()){
			for(JClass cls1:jsonClasses){
				for(JClass cls2:jsonClasses){
					if(cls1.getClassName().equals(entry.getKey())&&cls2.getClassName().equals(entry.getValue()))
						cls1.addAllMethods(cls2.getMethods());
				}
			}
		}
		Map<String, List<Shallow>> methodsByXmi = xmiProgram.getMethods();	
		//System.out.println("***");
		//System.out.println(methodsByXmi);
		//System.out.println();
		for(JClass cls:jsonClasses){
			//System.out.println(cls.getClassName());
			List<Shallow> methodList = methodsByXmi.get(cls.getClassName());
			if(methodList!=null){
				//System.out.println("not null!! "+methodList);
				for(JMethod jmethod:cls.getMethods()){
					String jmethodName = jmethod.getName();
					//System.out.println(jmethod);
					for( Shallow sameNameM:methodList.parallelStream().filter(m->m.getName().equals(jmethodName)).toArray(Shallow[]::new)){
						//System.out.println(sameNameM);
						if(new LinkedList<>(jmethod.getParams()).equals(sameNameM.getPara())){
							jmethod.setVisab(sameNameM.getVisibility());
							//System.out.println(jmethod+"\n"+sameNameM+"\n***********");
							break;
						}
					}
					jmethod.primToObject();				
				}	
			}
		}
		//ReaderHelper.primitiveToObject(
		//System.out.println();
		new Thread(){
			public void run(){
				JSONRead.toExcel(jsonClasses, folder, progName);	
			}
		}.start();	
		return jsonClasses;
	}
	//the jsonClasses here are already with the correct class hierarchy as they were made by the above preProcess function
	public static void runApp(double nw, double opNW, double opParW, String simType,
			String xmiFolder, String prog1name, String prog2name, List<JClass> json1Classes, List<JClass> json2Classes,
			DoubleProperty progress, boolean isDebug, Map<CommutativePair<String, String>, Double> typesMap, double eSim) throws Exception
	{
		if(json1Classes==null || json2Classes==null)
			return;
		String path=new File(xmiFolder).getParent();
		/*System.setOut(new PrintStream(new OutputStream() { //dummy syso
		    @Override public void write(int b) throws IOException {}
		}));*/
		//PrintStream originalStream = System.out;
		new File(path+"\\output\\cvisFiles").mkdirs();
		new File(path+"\\output\\logFiles").mkdirs();
		new File(path+"\\output\\exelFiles").mkdirs();
		PrintStream visInfo = new PrintStream(new FileOutputStream(path+
				 "\\output\\cvisFiles\\ProjectComp_" + prog1name + "#" + prog2name + "#nw"+String.format("%.2f", nw)
				 +"_pnw"+String.format("%.2f", opNW)
				 +"_th"+String.format("%.2f", opParW)+"_"+".cvis")); //the file format to show visualisation
		Integer[] params={(int) (nw*100), (int) (opNW*100), (int) (opParW*100)};
		visInfo.println(new Gson().toJson(params));
		visInfo.println(prog1name);
		visInfo.println(prog2name);
		visInfo.println(new Gson().toJson(toMini(json1Classes)));
		visInfo.println(new Gson().toJson(toMini(json2Classes)));
    	visInfo.close();
		Similarity sim = new Similarity(simType, prog1name, prog2name, progress, isDebug, typesMap, eSim);
		
//		//TODO samih
//		CvisObjToJson cvisObjToJson= new CvisObjToJson();
//		cvisObjToJson.params=params;
//		cvisObjToJson.proj1Name=prog1name;
//		cvisObjToJson.proj2Name=prog2name;
//		cvisObjToJson.json1Classes=toMini(json1Classes);
//		cvisObjToJson.json2Classes=toMini(json2Classes);
		
		sim.calcsim(json1Classes, json2Classes, nw, opNW, opParW, path); // nameWeight, opNameWeight, opParWeight, s1Weight
		//System.setOut(originalStream);
		System.out.println("\n\nProgram Finished. runApp1");
	}
	//the jsonClasses here are already with the correct class hierarchy as they were made by the above preProcess function
	public static void runAppByES(double nw, double opNW, double opParW, String simType,
			String xmiFolder, String prog1name, String prog2name, List<JClass> json1Classes, List<JClass> json2Classes,
			DoubleProperty progress, boolean isDebug, Map<CommutativePair<String, String>, Double> typesMap, double eSim, ProgressBar progressBar) throws Exception
	{
		if(json1Classes==null || json2Classes==null)
			return;
		String path=new File(xmiFolder).getParent();
		/*System.setOut(new PrintStream(new OutputStream() { //dummy syso
		    @Override public void write(int b) throws IOException {}
		}));*/
		//PrintStream originalStream = System.out;
//		new File(path+"\\output\\cvisFiles").mkdirs();
		new File(path+"\\output\\logFiles").mkdirs();
//		new File(path+"\\output\\exelFiles").mkdirs();
//		PrintStream visInfo = new PrintStream(new FileOutputStream(path+
//				 "\\output\\cvisFiles\\ProjectComp_" + prog1name + "#" + prog2name + "#nw"+String.format("%.2f", nw)
//				 +"_pnw"+String.format("%.2f", opNW)
//				 +"_th"+String.format("%.2f", opParW)+"_"+".cvis")); //the file format to show visualisation
		Integer[] params={(int) (nw*100), (int) (opNW*100), (int) (opParW*100)};
//		visInfo.println(new Gson().toJson(params));
//		visInfo.println(prog1name);
//		visInfo.println(prog2name);
//		visInfo.println(new Gson().toJson(toMini(json1Classes)));
//		visInfo.println(new Gson().toJson(toMini(json2Classes)));
//    	visInfo.close();
		Similarity sim = new Similarity(simType, prog1name, prog2name, progress, isDebug, typesMap, eSim);
		
//		//TODO samih
//		CvisObjToJson cvisObjToJson= new CvisObjToJson();
//		cvisObjToJson.params=params;
//		cvisObjToJson.proj1Name=prog1name;
//		cvisObjToJson.proj2Name=prog2name;
//		cvisObjToJson.json1Classes=toMini(json1Classes);
//		cvisObjToJson.json2Classes=toMini(json2Classes);
		
		sim.calcsimBYES(params,json1Classes, json2Classes, nw, opNW, opParW, path); // nameWeight, opNameWeight, opParWeight, s1Weight
		//System.setOut(originalStream);
		
		System.out.println("\n\nProgram Finished. runApp1");
	}
		/**
		 * Runs the logic.
		 * invoced from Main-> main with args
		 * @param nw
		 * @param opNW
		 * @param opParW
		 * @param simType
		 * @param progress 
		 * @throws Exception 
		 */
		public static void runApp(double nw, double opNW, double opParW, String simType,
				String folder1, String folder2, String xmiFolder,String xmi2Folder, String prog1name, String prog2name,
				DoubleProperty progress, Map<CommutativePair<String, String>, Double> typesMap) throws Exception
		{
			String path=new File(xmiFolder).getParent();
			//PrintStream originalStream = System.out;
			JSONRead json = new JSONRead(folder1, folder2, prog1name, prog2name);
			List<JClass>[] twoApps = json.ReadFromFile("#nw"+nw+"_pnw"+opNW+"_th"+opParW+"_");
			List<JClass> json1Classes=twoApps[0];
			List<JClass> json2Classes=twoApps[1];
			
			if(json1Classes.size()==0||json2Classes.size()==0){
				System.out.println("\n\nProgram Finished. (one or more project is empty)");
				return;
			}
			
			XMIRead xmi = new XMIRead(xmiFolder, xmi2Folder, prog1name, prog2name);
			XmiAdditionalInfo xmiProgram1;
			XmiAdditionalInfo xmiProgram2;
			XmiAdditionalInfo[] programs = xmi.ReadFromFile();
			xmiProgram1 = programs[0];
			xmiProgram2 = programs[1];
			
			for(Entry<String, String> entry:xmiProgram1.getClassHirarchy()){
				for(JClass cls1:json1Classes){
					for(JClass cls2:json1Classes){
						if(cls1.getClassName().equals(entry.getKey())&&cls2.getClassName().equals(entry.getValue()))
							cls1.addAllMethods(cls2.getMethods());
					}
				}
			}
			for(Entry<String, String> entry:xmiProgram2.getClassHirarchy()){
				for(JClass cls1:json2Classes){
					for(JClass cls2:json2Classes){
						if(cls1.getClassName().equals(entry.getKey())&&cls2.getClassName().equals(entry.getValue()))
							cls1.addAllMethods(cls2.getMethods());
					}
				}
			}
			//joinClasses(xmiProgram1, xmiProgram2, json1Classes, json2Classes);

			/* for (AClass a: prog1Classes) {
				// System.out.println("Class: " + a.getClassName());
				if (a.getClassName().equals("Game"))
						for (Operation o:a.getOperations()) {
							// System.out.println("Operation: " + o.getE());
							if (o.getE().equals ("paintComponent"))
								System.out.println(o.getE()+ ";"+o.getS1()+ ";"+o.getS2()+";"+a.getClassName());
						}
			} */
			// Similarity + Clustering
			/*System.setOut(new PrintStream(new OutputStream() { //dummy syso
			    @Override public void write(int b) throws IOException {}
			}));*/
			PrintStream visInfo = new PrintStream(new FileOutputStream(path+
					 "\\ProjectComp_" + prog1name + "#" + prog2name + "#nw"+nw+"_pnw"+opNW+"_th"+opParW+"_"+".cvis")); //the file format to show visualisation
			visInfo.println(prog1name);
			visInfo.println(prog2name);
			visInfo.println(new Gson().toJson(toMini(json1Classes)));
			visInfo.println(new Gson().toJson(toMini(json2Classes)));
        	visInfo.close();
			Similarity sim = new Similarity(simType, prog1name, prog2name, progress, false, typesMap, 0.8);
			//TODO samih
//			CvisObjToJson cvisObjToJson= new CvisObjToJson();
//			//cvisObjToJson.params=params;
//			cvisObjToJson.proj1Name=prog1name;
//			cvisObjToJson.proj2Name=prog2name;
//			cvisObjToJson.json1Classes=toMini(json1Classes);
//			cvisObjToJson.json2Classes=toMini(json2Classes);
			sim.calcsim(json1Classes, json2Classes, nw, opNW, opParW, path); // nameWeight, opNameWeight, opParWeight, s1Weight
			//System.setOut(originalStream);
			System.out.println("\n\nProgram Finished.  runApp2");
		}

		public static List<MiniJClass> toMini(List<JClass> classes) {
			List<MiniJClass> l=new LinkedList<>();
			for(JClass jclass:classes){
				MiniJClass minC=new MiniJClass(jclass.getClassName());
				for(JMethod method:jclass.getMethods()){
					MiniJMethod minM=new MiniJMethod(method.getName());
					minC.addMethods(minM);
				}
				l.add(minC);
			}
			return l;
		}
		public static void initXlFile(File compResFile) throws IOException {
			XSSFWorkbook wb = new XSSFWorkbook();
			FileOutputStream out = new FileOutputStream(compResFile);
			wb.write(out);
			out.close();
			wb.close();
		}
}

