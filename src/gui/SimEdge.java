package gui;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.Forest;
import sim.MethodSim;

public class SimEdge {
	private String pro1;
	private String pro2;
	private String cls1;
	private String cls2;
	private SimType type;
	private int useSim;
	private int refSim;
	private int extSim;
	private int refExtSim;
	private int overSim;
	private int sim;
	private String edgeName;
	private String edgeInfo;
	private ParamArr pr;
	/**
	 * gets classes names and all the similarities value
	 * @param cls1
	 * @param cls2
	 * @param useSim
	 * @param extSim
	 * @param refSim
	 * @param refExtSim
	 */
	private Forest<SimVertex, DiffStr> clusters= new DelegateForest<SimVertex, DiffStr>();
	public SimEdge(String pro1, String pro2, String cls1, String cls2, int useSim, int refSim, int extSim, int refExtSim, int overSim,
			SimType type, ParamArr pr){
		this.pr=pr;
		edgeName=cls1+" & "+cls2;
		edgeInfo="<html>"+edgeName;
		if(useSim>0)
			edgeInfo+="<br> Use:"+useSim+"%";
		if(refSim>0)
			edgeInfo+="<br> Ref.:"+refSim+"%";
		if(extSim>0)
			edgeInfo+="<br> Ext.:"+extSim+"%";
		if(refExtSim>0)
			edgeInfo+="<br> Ref. Ext.:"+refExtSim+"%";
		if(overSim>0)
			edgeInfo+="<br> Overloading:"+overSim+"%";
		if(!pr.isEmpty())
			edgeInfo+="<br> Parameteters: "+pr.toString();
		edgeInfo+="</html>";
		this.pro1=pro1;
		this.pro2=pro2;
		this.cls1=cls1;
		this.cls2=cls2;
		this.sim=useSim+refSim+extSim+refExtSim+overSim;
		this.useSim=useSim;
		this.refSim=refSim;
		this.extSim=extSim;
		this.refExtSim=refExtSim;
		this.overSim=overSim;
		this.type=type;	
	}
	public int getSim() {
		return sim;
	}
	public void setSim(int sim) {
		this.sim = sim;
	}
	public String getCls1() {
		return cls1;
	}
	public String getCls2() {
		return cls2;
	}
	public String getPro1() {
		return pro1;
	}
	public String getPro2() {
		return pro2;
	}
	public String getName() {
		return edgeName;
	}
	public SimType getType() {
		return type;
	}
	public int getUseSim() {
		return useSim;
	}
	public int getExtSim() {
		return extSim;
	}
	public int getRefSim() {
		return refSim;
	}
	public int getRefExtSim() {
		return refExtSim;
	}
	public int getOverSim() {
		return overSim;
	}
	public void addToCluster(MethodSim ms) {
		clusters.addEdge(new DiffStr(ms.getType().name()),
				new SimVertex(ms.getMethod1(), pro1),
				new SimVertex(ms.getMethod2(), pro2));
	}
	
	public Forest<SimVertex, DiffStr> getClusters() {
		return clusters;
	}
	public String getEdgeInfo() {
		return edgeInfo;
	}
	public String toString(){
		return type.name();
	}
	public ParamArr getPr() {
		return pr;
	}
	public void setPr(ParamArr pr) {
		this.pr = pr;
	}
}
