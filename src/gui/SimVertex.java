package gui;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import sim.JMethod;
import sim.MiniJClass;
import sim.MiniJMethod;

public class SimVertex implements Comparable<SimVertex>{
	protected String name;
	protected String projectName;
	protected List<String> operations=new LinkedList<String>();
	public SimVertex(String name, String projectName){
		this.name=name;
		this.projectName=projectName;	
	}
	public SimVertex(String name, String projectName, Collection<MiniJMethod> set){
		this.name=name;
		this.projectName=projectName;	
		set.forEach(m->operations.add(m.getName()));
		operations.sort(null);
	}
	public SimVertex(String name, Collection<MiniJClass> set){
		this.name=name;
		this.projectName=name;	
		set.forEach(m->operations.add(m.getClassName()));
		operations.sort(null);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSize() {
		return operations.size();
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String htmlDesc(){
		String s="<html>"+name+"<br>Operations (#"+operations.size()+"):";
		String prevOp="";
		int sameCntr=1;
		for(String op:operations){
			if(!prevOp.equals(op)){
				s+="<br>";
				s+=op+" (#"+sameCntr+")";
				prevOp=op;
				sameCntr=1;
			}
			else
				sameCntr++;		
		}
		s+="</html>";
		return s;
	}
	public String toString(){
		return name;
	}
	public int compareTo(SimVertex vOther){
		int nameComp=this.getName().compareTo(vOther.getName());
		if(nameComp!=0)
			return nameComp;
		int proComp=this.getProjectName().compareTo(vOther.getProjectName());
		if(proComp!=0)
			return proComp;
		int opSizeComp=((Integer)this.operations.size()).compareTo(vOther.operations.size());
		if(opSizeComp!=0)
			return opSizeComp;
		if(!this.operations.containsAll(vOther.operations))
			return 1;
		return 0;	
	}
	public int hashCode(){
		return getName().hashCode()*getProjectName().hashCode()*operations.hashCode();	
	}
	public boolean equals(Object other){
		SimVertex vOther=(SimVertex) other;
		return this.getName().equals(vOther.getName())&&this.getProjectName().equals(vOther.getProjectName())
				&&this.operations.size()==vOther.operations.size()&&this.operations.containsAll(vOther.operations);	
	}
}
