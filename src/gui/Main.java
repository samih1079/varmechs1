package gui;

import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.HashMap;

import entities.DBconn;
import javafx.application.Application;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

/**
 * The main class of the system.
 * @author AsafM
 * Including bypass of GUI
 */

public class Main extends Application
{

	static String name;
	static String opName;
	static String parameters;
	static String parNameWeight;
	static String s1;
	static String eWeight;
	static String clusterThreshold;
	static String distanceThreshold;


	/**
	 * Method loads all the GUI screens to a main container.
	 */
	@Override
    public void start(Stage primaryStage)
	 {
		//DBconn.initConn();
		try
		{
			ScreensController mainContainer = new ScreensController(); // Screen controller holds all screens

			// Loading all the screens to the main container
			mainContainer.loadScreen(ScreensFramework.screen1ID, ScreensFramework.screen1File);

			// Starts with the main screen (no.1: main form)
			mainContainer.setScreen(ScreensFramework.screen1ID);

			final ScrollPane root = new ScrollPane();
			root.setContent(mainContainer);
			final Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setTitle("VarMeR");
			stage.setScene(scene);
			stage.setResizable(true);
			stage.show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    }

	/**
	 * The main method of the system.
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		/*PrintStream out = new PrintStream(new FileOutputStream("VarMeR_OutputLog.txt"));
    	System.setOut(out);
    	System.setErr(new PrintStream(new OutputStream() {
    		  public void write(int b) {
    		    // NO-OP
    		  }
    		}));*/
		new DBconn();
		PrintStream originalStream = System.out;
		try {
			if (args.length == 5)
			{
				deSerialize();

				String jsonFolder1 = args[0] + "\\Json\\";
				String xmiFolder1 = args[0];
				String prog1name = args[1];

				String jsonFolder2 = args[2] + "\\Json\\";
				String xmiFolder2 = args[2];
				String prog2name = args[3];
				String simType = "MCS"; // Default
				if (args[4].equals("MCS") || args[4].equals("LSA") || args[4].equals("UMBS"))
					simType = args[4];
				runApplication.runApp(Double.parseDouble(name), Double.parseDouble(opName), Double.parseDouble(parameters),
					simType, jsonFolder1, jsonFolder2, xmiFolder1, xmiFolder2, prog1name, prog2name, new SimpleDoubleProperty(),
					new HashMap<>());
			}
			else
				launch(args); // launch the UI JavaFX
		} catch (Exception e) {
			e.printStackTrace();
			originalStream.println("ERROR [End process]");
			System.exit(1);
		} finally {
			originalStream.println("[End process]");
			System.exit(0);
		}
	}

	public static void deSerialize()
	{
		try
	      {
	         FileInputStream fis = new FileInputStream("ser/name.ser");
	         ObjectInputStream ois = new ObjectInputStream(fis);
	         name = (String) ois.readObject();
	         ois.close();
	         fis.close();

	         FileInputStream fis2 = new FileInputStream("ser/opName.ser");
	         ObjectInputStream ois2 = new ObjectInputStream(fis2);
	         opName = (String) ois2.readObject();
	         ois2.close();
	         fis2.close();

	         FileInputStream fis3 = new FileInputStream("ser/parameters.ser");
	         ObjectInputStream ois3 = new ObjectInputStream(fis3);
	         parameters = (String) ois3.readObject();
	         ois3.close();
	         fis3.close();

	         FileInputStream fis4 = new FileInputStream("ser/s1.ser");
	         ObjectInputStream ois4 = new ObjectInputStream(fis4);
	         s1 = (String) ois4.readObject();
	         ois4.close();
	         fis4.close();

	         FileInputStream fis10 = new FileInputStream("ser/eWeight.ser");
	         ObjectInputStream ois10 = new ObjectInputStream(fis10);
	         eWeight = (String) ois10.readObject();
	         ois10.close();
	         fis10.close();


	         FileInputStream fis12 = new FileInputStream("ser/clusterThreshold.ser");
	         ObjectInputStream ois12 = new ObjectInputStream(fis12);
	         clusterThreshold = (String) ois12.readObject();
	         ois12.close();
	         fis12.close();

	         FileInputStream fis13 = new FileInputStream("ser/distanceThreshold.ser");
	         ObjectInputStream ois13 = new ObjectInputStream(fis13);
	         distanceThreshold = (String) ois13.readObject();
	         ois13.close();
	         fis13.close();

	         FileInputStream fis14 = new FileInputStream("ser/parNameWeight.ser");
	         ObjectInputStream ois14 = new ObjectInputStream(fis14);
	         parNameWeight = (String) ois14.readObject();
	         ois14.close();
	         fis14.close();
	      }
		catch(IOException ioe)
	      {
	         ioe.printStackTrace();
	      }
		catch(ClassNotFoundException c)
	      {
	         System.out.println("Class not found");
	         c.printStackTrace();
	      }
	}
}

