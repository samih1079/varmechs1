package gui;

import com.sun.javafx.scene.control.skin.VirtualFlow;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class MyTextFieldTableCell<S> extends TextFieldTableCell<S, String> {
    /** */
    private String curTxt = "";

    /** {@inheritDoc} */
    @Override public void startEdit() {
        super.startEdit();

        curTxt = "";

        Node g = getGraphic();

        if (g != null) {
            final TextField tf = (TextField)g;

            tf.textProperty().addListener(new ChangeListener<String>() {            
            	public void changed(ObservableValue<? extends String> val, String oldVal, String newVal) {
                    
                }
            });

            tf.setOnKeyReleased(new EventHandler<KeyEvent>() {
                @Override public void handle(KeyEvent evt) {
                    if (KeyCode.ENTER == evt.getCode())
                        cancelEdit();
                    else if (KeyCode.ESCAPE == evt.getCode()) {
                        cancelEdit();
                    }
                }
            });

            // Special hack for editable TextFieldTableCell.
            // Cancel edit when focus lost from text field, but do not cancel if focus lost to VirtualFlow.
            tf.focusedProperty().addListener(new ChangeListener<Boolean>() {
                @Override public void changed(ObservableValue<? extends Boolean> val, Boolean oldVal, Boolean newVal) {
                    Node fo = getScene().getFocusOwner();

                    if (!newVal) {
                        if (fo instanceof VirtualFlow) {
                            if (fo.getParent().getParent() != getTableView())
                                cancelEdit();
                        }
                        else
                            cancelEdit();
                    }
                }
            });

            Platform.runLater(new Runnable() {
                @Override public void run() {
                    tf.requestFocus();
                }
            });
        }
    }

    /** {@inheritDoc} */
    @Override public void cancelEdit() {
    	 commitEdit(curTxt);
    }
}