package gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import entities.JProject;
import entities.VisualizedPair;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import scala.annotation.meta.param;
import javafx.scene.control.ProgressBar;
import semilar.data.Sentence;
import semilar.sentencemetrics.CorleyMihalceaComparer;
import semilar.tools.preprocessing.SentencePreprocessor;
import sim.CommutativePair;
import storageUtils.ESUtils;

/** This class is the controller of the main form (GUI start form) */
public class MainFormController implements Initializable, ControlledScreen {
	@FXML
	private TableView<TypesPair> tb_types;
	private ObservableList<TypesPair> typesList = FXCollections
			.synchronizedObservableList(FXCollections.observableArrayList());
	@FXML
	private StackPane stackedPane;
	@FXML
	private TableView<JProject> tb_projects;
	private ObservableList<JProject> projectsList = FXCollections.observableArrayList();
	@FXML
	private TableColumn<JProject, Boolean> tb_projects_proc;

	@FXML
	private TableView<VisualizedPair> tb_pairs;
	private ObservableList<VisualizedPair> pairsList = FXCollections
			.synchronizedObservableList(FXCollections.observableArrayList());
	@FXML
	private TableColumn<VisualizedPair, Double> tb_pairs_pre;

	@FXML
	private TableView<VisualizedPair> tb_fin_pairs;
	private ObservableList<VisualizedPair> finPairsList = FXCollections
			.synchronizedObservableList(FXCollections.observableArrayList());

	@SuppressWarnings("unused")
	private ScreensController mycontroller;

	@FXML
	private TextField eleW;
	@FXML
	private TextField paramW;
	@FXML
	private TextField simThres;
	@FXML
	private TextField eleW1;
	@FXML
	private TextField paramW1;
	@FXML
	private TextField simThres1;
	@FXML
	private TextField eSimThres;
	@FXML
	private TextField missionName;
	@FXML
	private Label lblMissionName;
	@FXML
	private RadioButton mcs;
	@FXML
	private RadioButton lsa;
	@FXML
	private RadioButton umb;

	@FXML
	private ToggleButton debugButton;
	@FXML
	public ProgressBar progressBar;
	@FXML
	public Button btnRunall;
	@FXML
	public Button btnRun;
	@FXML
	public Button btnShowSelslected;
	@FXML
	public Button btnShowAll;
	private String lastProjectLoc = null;

	final static private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");

	private final Semaphore autoPreprocSem = new Semaphore(2, true);

	private final static int THRED_POOL_SIZE = 3;

	private final static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(THRED_POOL_SIZE);

	private final static ExecutorService THREAD_POOL_ADDPrjs = Executors.newFixedThreadPool(THRED_POOL_SIZE);

	public static int allPairs = 0;
	public static int currentPair = 0;

	/** This method allows the injection of the Parent screenPane */
	public void setScreenParent(ScreensController screenParent) {
		mycontroller = screenParent;
	}

	/**
	 * Initialize method for component pre-loading to screen
	 * 
	 * @param arg0
	 * @param arg1
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		stackedPane.setVisible(false);

		final ToggleGroup group = new ToggleGroup();
		mcs.setToggleGroup(group);
		mcs.setSelected(true);
		lsa.setToggleGroup(group);
		umb.setToggleGroup(group);
		deSerialize();
		InvalidationListener serListen = new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				serialize();
			}
		};

		btnShowAll.setDisable(true);
		btnShowSelslected.setDisable(true);

		eleW.textProperty().addListener(serListen);
		eleW1.textProperty().addListener(serListen);
		paramW.textProperty().addListener(serListen);
		paramW1.textProperty().addListener(serListen);
		simThres.textProperty().addListener(serListen);
		simThres1.textProperty().addListener(serListen);
		eSimThres.textProperty().addListener(serListen);

		tb_projects.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE); // Multiple project selection
		tb_fin_pairs.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		TableColumn<TypesPair, String> type1 = ((TableColumn) tb_types.getColumns().get(0));
		TableColumn<TypesPair, String> type2 = ((TableColumn) tb_types.getColumns().get(1));
		TableColumn<TypesPair, String> sim = ((TableColumn) tb_types.getColumns().get(2));

		type1.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().type1));
		type2.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().type2));
		sim.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().sim));

		type1.setCellFactory(TextFieldTableCell.<TypesPair>forTableColumn());
		type2.setCellFactory(TextFieldTableCell.<TypesPair>forTableColumn());
		sim.setCellFactory(TextFieldTableCell.<TypesPair>forTableColumn());

		for (int i = 0; i < 100; i++)
			typesList.add(new TypesPair());
		type1.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<TypesPair, String>>() {
			@Override
			public void handle(CellEditEvent e) {
				((TypesPair) e.getRowValue()).type1 = (String) e.getNewValue();
			}
		});
		type2.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<TypesPair, String>>() {
			@Override
			public void handle(CellEditEvent e) {
				((TypesPair) e.getRowValue()).type2 = (String) e.getNewValue();
			}
		});
		sim.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<TypesPair, String>>() {
			@Override
			public void handle(CellEditEvent e) {
				((TypesPair) e.getRowValue()).sim = (String) e.getNewValue();
			}
		});
		// tb_types.seton.setOnScroll(a->System.out.println("fihcoig"));
		Platform.runLater(() -> {
			ScrollBar verticalBar = (ScrollBar) tb_types.lookup(".scroll-bar:vertical");
			verticalBar.valueProperty().addListener((obs, oldValue, newValue) -> {
				if (newValue.doubleValue() >= verticalBar.getMax()) {
					verticalBar.setValue((oldValue.doubleValue() + newValue.doubleValue()) / 2);
					typesList.add(new TypesPair());
				}
			});
		});

		tb_projects.getColumns().get(0).setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getPathTo()));
		tb_projects.getColumns().get(1)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getProjectName()));
		tb_projects_proc.setCellValueFactory(new PropertyValueFactory<JProject, Boolean>("classesInitialized"));
		tb_projects_proc.setCellFactory(CheckBoxTableCell.forTableColumn(tb_projects_proc));

		tb_pairs.getColumns().get(0)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getEnw())));
		tb_pairs.getColumns().get(1)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getPnw())));
		tb_pairs.getColumns().get(2)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getTh())));
		tb_pairs.getColumns().get(3).setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getProject1()));
		tb_pairs.getColumns().get(4).setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getProject2()));
		tb_pairs_pre.setCellValueFactory(new PropertyValueFactory<VisualizedPair, Double>("progress"));
		tb_pairs_pre.setCellFactory(ProgressBarTableCell.<VisualizedPair>forTableColumn());

		tb_fin_pairs.getColumns().get(0)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getEnw())));
		tb_fin_pairs.getColumns().get(1)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getPnw())));
		tb_fin_pairs.getColumns().get(2)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(String.format("%.2f", p.getValue().getTh())));
		tb_fin_pairs.getColumns().get(3)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getProject1()));
		tb_fin_pairs.getColumns().get(4)
				.setCellValueFactory(p -> new ReadOnlyObjectWrapper(p.getValue().getProject2()));

		((TableColumn) tb_projects.getColumns().get(0)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_projects.getColumns().get(1)).setCellFactory(TooltippedTableCell.forTableColumn());

		((TableColumn) tb_pairs.getColumns().get(0)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_pairs.getColumns().get(1)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_pairs.getColumns().get(2)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_pairs.getColumns().get(3)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_pairs.getColumns().get(4)).setCellFactory(TooltippedTableCell.forTableColumn());

		((TableColumn) tb_fin_pairs.getColumns().get(0)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_fin_pairs.getColumns().get(1)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_fin_pairs.getColumns().get(2)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_fin_pairs.getColumns().get(3)).setCellFactory(TooltippedTableCell.forTableColumn());
		((TableColumn) tb_fin_pairs.getColumns().get(4)).setCellFactory(TooltippedTableCell.forTableColumn());

		tb_types.setItems(typesList);
		tb_projects.setItems(projectsList);
		tb_pairs.setItems(pairsList);
		tb_fin_pairs.setItems(finPairsList);

		tb_projects.setDisable(true);
		tb_pairs.setDisable(true);
		tb_fin_pairs.setDisable(true);

		ListChangeListener lcPrL = new ListChangeListener() {
			@Override
			public void onChanged(Change arg0) {
				if (projectsList.isEmpty())
					tb_projects.setDisable(true);
				else
					tb_projects.setDisable(false);
			}
		};
		ListChangeListener lcPL = new ListChangeListener() {
			@Override
			public void onChanged(Change arg0) {
				if (pairsList.isEmpty())
					tb_pairs.setDisable(true);
				else
					tb_pairs.setDisable(false);
			}
		};
		ListChangeListener lcFP = new ListChangeListener() {
			@Override
			public void onChanged(Change arg0) {
				if (finPairsList.isEmpty())
					tb_fin_pairs.setDisable(true);
				else
					tb_fin_pairs.setDisable(false);
			}
		};

		projectsList.addListener(lcPrL);
		pairsList.addListener(lcPL);
		finPairsList.addListener(lcFP);

		new Thread() {
			public void run() {
				// Initialise MCS
				SentencePreprocessor preprocessor = new SentencePreprocessor(
						SentencePreprocessor.TokenizerType.STANFORD, SentencePreprocessor.TaggerType.STANFORD,
						SentencePreprocessor.StemmerType.PORTER, SentencePreprocessor.ParserType.STANFORD);
				CorleyMihalceaComparer cmComparer = new CorleyMihalceaComparer(0.3f, false, "NONE", "par");

				Sentence sentence1 = preprocessor.preprocessSentence("r");
				Sentence sentence2 = preprocessor.preprocessSentence("length");
				try {
					cmComparer.computeSimilarity(sentence1, sentence2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	// ***************************************SERIALIZE***************************************************

	/**
	 * Method loads the latest data from the system by de-serializing it.
	 */
	@SuppressWarnings("unchecked")
	private void deSerialize() {
		try {
			FileInputStream fis = new FileInputStream("ser/name.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			this.eleW.setText((String) ois.readObject());
			ois.close();
			fis.close();

			FileInputStream fis2 = new FileInputStream("ser/opName.ser");
			ObjectInputStream ois2 = new ObjectInputStream(fis2);
			this.paramW.setText((String) ois2.readObject());
			ois2.close();
			fis2.close();

			FileInputStream fis3 = new FileInputStream("ser/parameters.ser");
			ObjectInputStream ois3 = new ObjectInputStream(fis3);
			this.simThres.setText((String) ois3.readObject());
			ois3.close();
			fis3.close();

			FileInputStream fis4 = new FileInputStream("ser/name1.ser");
			ObjectInputStream ois4 = new ObjectInputStream(fis4);
			this.eleW1.setText((String) ois4.readObject());
			ois4.close();
			fis4.close();

			FileInputStream fis5 = new FileInputStream("ser/opName1.ser");
			ObjectInputStream ois5 = new ObjectInputStream(fis5);
			this.paramW1.setText((String) ois5.readObject());
			ois5.close();
			fis5.close();

			FileInputStream fis6 = new FileInputStream("ser/parameters1.ser");
			ObjectInputStream ois6 = new ObjectInputStream(fis6);
			this.simThres1.setText((String) ois6.readObject());
			ois6.close();
			fis6.close();

			FileInputStream fis7 = new FileInputStream("ser/typesTable.ser");
			ObjectInputStream ois7 = new ObjectInputStream(fis7);
			((List<TypesPair>) ois7.readObject()).forEach(a -> typesList.add(a));
			ois7.close();
			fis7.close();

			FileInputStream fis8 = new FileInputStream("ser/eSim.ser");
			ObjectInputStream ois8 = new ObjectInputStream(fis8);
			this.eSimThres.setText((String) ois8.readObject());
			ois8.close();
			fis8.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ClassNotFoundException c) {
			System.err.println("Class not found");
			c.printStackTrace();
		}
	}

	/**
	 * Method saves the latest data of the system by serializing it.
	 */
	protected void serialize() {
		try {
			FileOutputStream fos = new FileOutputStream("ser/name.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(eleW.getText());
			oos.close();
			fos.close();

			FileOutputStream fos2 = new FileOutputStream("ser/opName.ser");
			ObjectOutputStream oos2 = new ObjectOutputStream(fos2);
			oos2.writeObject(paramW.getText());
			oos2.close();
			fos2.close();

			FileOutputStream fos3 = new FileOutputStream("ser/parameters.ser");
			ObjectOutputStream oos3 = new ObjectOutputStream(fos3);
			oos3.writeObject(simThres.getText());
			oos3.close();
			fos3.close();

			FileOutputStream fos4 = new FileOutputStream("ser/name1.ser");
			ObjectOutputStream oos4 = new ObjectOutputStream(fos4);
			oos4.writeObject(eleW1.getText());
			oos4.close();
			fos4.close();

			FileOutputStream fos5 = new FileOutputStream("ser/opName1.ser");
			ObjectOutputStream oos5 = new ObjectOutputStream(fos5);
			oos5.writeObject(paramW1.getText());
			oos5.close();
			fos5.close();

			FileOutputStream fos6 = new FileOutputStream("ser/parameters1.ser");
			ObjectOutputStream oos6 = new ObjectOutputStream(fos6);
			oos6.writeObject(simThres1.getText());
			oos6.close();
			fos6.close();

			FileOutputStream fos7 = new FileOutputStream("ser/typesTable.ser");
			ObjectOutputStream oos7 = new ObjectOutputStream(fos7);
			oos7.writeObject(new ArrayList<>(typesList));
			oos7.close();
			fos7.close();

			FileOutputStream fos8 = new FileOutputStream("ser/eSim.ser");
			ObjectOutputStream oos8 = new ObjectOutputStream(fos8);
			oos8.writeObject(eSimThres.getText());
			oos8.close();
			fos8.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	// ***************************************** Launch Method
	// *********************************************
	@FXML
	public void addProject(ActionEvent event) {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Choose a project");
		String currentDir = lastProjectLoc;
		if (currentDir == null)
			currentDir = System.getProperty("user.dir");
		chooser.setInitialDirectory(new File(currentDir));
		File file = chooser.showDialog(null);
		if (file != null && file.isDirectory()) {
			addProjectFolder(file, true);

		}

	}

	private void addProjectFolder(File file, boolean isManual) {
		String fileParent = file.getParentFile().getAbsolutePath();
		lastProjectLoc = fileParent;
		JProject jProject = new JProject(fileParent, file.getName());
		projectsList.add(jProject);
		if (isManual)
			THREAD_POOL_ADDPrjs.execute(() -> {
				try {
					autoPreprocSem.acquire();
					jProject.startPreprocessing();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					autoPreprocSem.release();
				}
			});
		// new Thread() {
		// public void run() {
		// try {
		// autoPreprocSem.acquire();
		// jProject.startPreprocessing();
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// autoPreprocSem.release();
		// }
		// }
		// }.start();
	}

	@FXML
	public void mapTypes(ActionEvent event) {
		stackedPane.setVisible(true);
	}

	@FXML
	public void hideStacked(MouseEvent event) {
		stackedPane.setVisible(false);
		serialize();
	}

	@FXML
	public void run(ActionEvent event) {
		if (ESUtils.EsORFiles == ESUtils.JUST_ES) {
			if (checkIndex(missionName.getText()) > 1) {
				lblMissionName.setTextFill(Color.BLACK);
				ESUtils.BaseINDEX = missionName.getText().toLowerCase();
				missionName.setText(ESUtils.BaseINDEX);
				// if(tb_projects.getSelectionModel().getSelectedItems().size()>1)
				// {

				JProject[] proArr = tb_projects.getSelectionModel().getSelectedItems().toArray(new JProject[0]);

				switch (ESUtils.EsORFiles) {
				case ESUtils.JUST_FILES:
					runFor(proArr);
					break;
				case ESUtils.JUST_ES:
					runForByES(proArr);
					break;
				default:
					break;
				}

				// }
				// else
				// {
				// Alert alert = new Alert(AlertType.CONFIRMATION);
				// alert.setTitle("Add Projects");
				// alert.setHeaderText("You must add at least tow projects");
				// ButtonType noButton = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
				// alert.getButtonTypes().setAll(noButton);
				//
				// Optional<ButtonType> result = alert.showAndWait();
				// }
			}
		} else {

			JProject[] proArr = tb_projects.getSelectionModel().getSelectedItems().toArray(new JProject[0]);

			runFor(proArr);

		}
	}

	@FXML
	public void runAll(ActionEvent event) {
		if (ESUtils.EsORFiles == ESUtils.JUST_ES) {
			if (checkIndex(missionName.getText()) > 1) {
				// if(tb_projects.getSelectionModel().getSelectedItems().size()>1)
				// {
				lblMissionName.setTextFill(Color.BLACK);
				ESUtils.BaseINDEX = missionName.getText();
				JProject[] proArr = projectsList.toArray(new JProject[0]);
				switch (ESUtils.EsORFiles) {
				case ESUtils.JUST_FILES:
					runFor(proArr);
					break;
				case ESUtils.JUST_ES:
					runForByES(proArr);
					break;
				default:
					break;
				}
				// }
				// else
				// {
				// Alert alert = new Alert(AlertType.CONFIRMATION);
				// alert.setTitle("Add Projects");
				// alert.setHeaderText("You must add at least tow projects");
				// ButtonType noButton = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
				// alert.getButtonTypes().setAll(noButton);
				//
				// Optional<ButtonType> result = alert.showAndWait();
				// }
			}
		} else {
			JProject[] proArr = projectsList.toArray(new JProject[0]);
			runFor(proArr);
		}

	}

	/**
	 * 
	 * @param index
	 * @return 0 - empty field, 1-index found use another name, 2-index found update
	 *         it, 3 - index found overwrite it 4- index not found, empty index will
	 *         be created
	 */
	private int checkIndex(String index) {
		if (missionName.getText().length() == 0) {
			lblMissionName.setTextFill(Color.RED);
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Mission Name is missing:");
			alert.setHeaderText("You have to fill mission name feild");
			ButtonType noButton = new ButtonType("OK", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(noButton);

			Optional<ButtonType> result = alert.showAndWait();
			return 0;
		} else
			try {
				index = index.toLowerCase();
				missionName.setText(index);
				if (ESUtils.getInstance().createIfNotFound(index, null) == true) {
					lblMissionName.setTextFill(Color.RED);
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Mission Name is Found: " + index);
					alert.setHeaderText("You have to fill another mission name or update current index ");
					ButtonType updateButton = new ButtonType("update", ButtonData.CANCEL_CLOSE);
					ButtonType overwriteButton = new ButtonType("overwrite", ButtonData.CANCEL_CLOSE);
					ButtonType noButton = new ButtonType("No, i want to use another name", ButtonData.CANCEL_CLOSE);

					alert.getButtonTypes().setAll(noButton, updateButton, overwriteButton);

					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == noButton) {
						return 1;
					} else if (result.get() == updateButton) {
						lblMissionName.setTextFill(Color.BLACK);
						ESUtils.PARAMS_Index = index + ESUtils.PARAMS_TYPE;
						ESUtils.MINIJPROG_Index = index + ESUtils.MINIJPROG_TYPE;
						ESUtils.PROJECT_SIM_Index = index + ESUtils.PROJECT_SIM_TYPE;
						ESUtils.CLASSES_RCMNDS_Index = index + ESUtils.CLASSES_RCMNDS_TYPE;
						ESUtils.METHODS_CAT_Index = index + ESUtils.METHODS_CAT_TYPE;
						return 2;
					} else if (result.get() == overwriteButton) {
						lblMissionName.setTextFill(Color.BLACK);
						ESUtils.getInstance().overwriteAllIndex(index);

						return 3;

					}
				} else {
					lblMissionName.setTextFill(Color.BLACK);
					return 4;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return -1;
	}

	private void runFor(JProject[] proArr) {

		if (proArr.length < 2) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Add Projects");
			alert.setHeaderText("You must  add at least tow projects");
			ButtonType noButton = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(noButton);

			Optional<ButtonType> result = alert.showAndWait();
			return;
		}
		// clear outcomes when calculating confirmation
		if (!finPairsList.isEmpty()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Do you want to clear the outcomes?");
			alert.setHeaderText("Do you want to clear the outcomes?");
			ButtonType yesButton = new ButtonType("Yes");
			ButtonType noButton = new ButtonType("No", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(yesButton, noButton);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == yesButton) {
				finPairsList.clear();
			}
		}
		double nw = Double.valueOf(eleW.getText());
		double opNW = Double.valueOf(paramW.getText());
		double parW = Double.valueOf(simThres.getText());
		double nw1 = Double.valueOf(eleW1.getText());
		double opNW1 = Double.valueOf(paramW1.getText());
		double parW1 = Double.valueOf(simThres1.getText());
		double eSim = Double.valueOf(eSimThres.getText());

		boolean autoNotOverwrite = false;
		boolean autoOverwrite = false;
		for (int i = 0; i < proArr.length; i++) {
			for (int j = i + 1; j < proArr.length; j++) {
				try {
					JProject proI = proArr[i];
					JProject proJ = proArr[j];
					String progIname = proI.getProjectName();
					String progJname = proJ.getProjectName();
					String xmiIFolder = proI.getPathTo() + "\\" + progIname;
					String xmiJFolder = proJ.getPathTo() + "\\" + progJname;
					;
					String simType;
					if (mcs.isSelected())
						simType = "MCS";
					else if (lsa.isSelected())
						simType = "LSA";
					else
						simType = "UMBS";
					String path1 = new File(xmiIFolder).getParent();
					String path2 = new File(xmiJFolder).getParent();

					for (double nwI = nw; nwI <= nw1; nwI += 0.1) {
						for (double opNWI = opNW; opNWI <= opNW1; opNWI += 0.1) {
							for (double parWI = parW; parWI <= parW1; parWI += 0.1) {
								String visFileSuffix = "#nw" + String.format("%.2f", nwI) + "_pnw"
										+ String.format("%.2f", opNWI) + "_th" + String.format("%.2f", parWI) + "_"
										+ ".cvis";
								// check all possible visualisation file names/paths
								File visFile1 = new File(path1 + "\\output\\cvisFiles\\ProjectComp_" + progIname + "#"
										+ progJname + visFileSuffix);
								File visFile2 = new File(path2 + "\\output\\cvisFiles\\ProjectComp_" + progIname + "#"
										+ progJname + visFileSuffix);
								File visFile3 = new File(path1 + "\\output\\cvisFiles\\ProjectComp_" + progJname + "#"
										+ progIname + visFileSuffix);
								File visFile4 = new File(path2 + "\\output\\cvisFiles\\ProjectComp_" + progJname + "#"
										+ progIname + visFileSuffix);
								List<File> existingVis = new LinkedList<>();
								if (visFile1.exists())
									existingVis.add(visFile1);
								if (visFile2.exists())
									existingVis.add(visFile2);
								if (visFile3.exists())
									existingVis.add(visFile3);
								if (visFile4.exists())
									existingVis.add(visFile4);
								boolean calc = autoOverwrite || existingVis.isEmpty();
								File visFile = null;
								if (!calc)
									visFile = Collections.max(existingVis,
											(a, b) -> ((Long) a.lastModified()).compareTo(b.lastModified()));
								if (!calc && !autoNotOverwrite) {
									Alert alert = new Alert(AlertType.CONFIRMATION);
									alert.setTitle("Replace or use file");
									alert.setHeaderText("A matching visualization file from"
											+ dateFormat.format(new Date(visFile.lastModified()))
											+ "for those porject was found.");
									alert.setContentText("Do you want to use it?\n"
											+ "(Choosing no means to do all the calculations again which can take time)");

									ButtonType yesButton = new ButtonType("Yes");
									ButtonType rYesButton = new ButtonType("Yes and remember my choice");
									ButtonType noButton = new ButtonType("No", ButtonData.CANCEL_CLOSE);
									ButtonType rNoButton = new ButtonType("No and remember my choice");

									alert.getButtonTypes().setAll(yesButton, rYesButton, noButton, rNoButton);

									Optional<ButtonType> result = alert.showAndWait();
									if (result.get() == rYesButton) {
										autoNotOverwrite = true;
									} else if (result.get() == rNoButton) {
										autoOverwrite = true;
										calc = true;
									} else if (result.get() != yesButton) {
										// if close or no
										calc = true;
									}
								}
								final double nwT = nwI;
								final double opNWT = opNWI;
								final double parWT = parWI;
								if (calc) {
									THREAD_POOL.execute(() -> {
										try {
											VisualizedPair visP = new VisualizedPair(nwT, opNWT, parWT, progIname,
													progJname, visFile1.getAbsolutePath());

											ChangeListener<? super Number> finListener = new ChangeListener<Number>() {

												@Override
												public void changed(ObservableValue<? extends Number> obsVal,
														Number oldVal, Number newVal) {
													if (newVal.intValue() > 0) {
														pairsList.remove(visP);
														finPairsList.add(visP);
													}
												}

											};
											visP.progressProperty().addListener(finListener);
											pairsList.add(visP);
											runApplication.runApp(nwT, opNWT, parWT, simType, xmiIFolder, progIname,
													progJname, proI.getProjectClasses(), proJ.getProjectClasses(),
													visP.progressProperty(), debugButton.isSelected(),
													toTypesMap(typesList), eSim);

										} catch (Exception e) {
											e.printStackTrace();
										}
									});
									/*
									 * new Thread() { public void run() { try { VisualizedPair visP = new
									 * VisualizedPair(nwT, opNWT, parWT, progIname, progJname,
									 * visFile1.getAbsolutePath());
									 * 
									 * ChangeListener<? super Number> finListener = new ChangeListener<Number>() {
									 * 
									 * @Override public void changed(ObservableValue<? extends Number> obsVal,
									 * Number oldVal, Number newVal) { if (newVal.intValue() > 0) {
									 * pairsList.remove(visP); finPairsList.add(visP); } }
									 * 
									 * }; visP.progressProperty().addListener(finListener); pairsList.add(visP);
									 * runApplication.runApp(nwT, opNWT, parWT, simType, xmiIFolder, progIname,
									 * progJname, proI.getProjectClasses(), proJ.getProjectClasses(),
									 * visP.progressProperty(), debugButton.isSelected(), toTypesMap(typesList),
									 * eSim); } catch (Exception e) { e.printStackTrace(); } } }.start();
									 */
								} else {
									// check the newest file
									finPairsList.add(new VisualizedPair(nwT, opNWT, parWT, progIname, progJname,
											visFile.getAbsolutePath()));
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}

	private void runForByES(JProject[] proArr) {

		if (proArr.length < 2) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Add Projects");
			alert.setHeaderText("You must  add at least tow projects");
			ButtonType noButton = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(noButton);

			Optional<ButtonType> result = alert.showAndWait();
			return;
		}
		// clear outcomes when calculating confirmation
		if (!finPairsList.isEmpty()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Do you want to clear the outcomes?");
			alert.setHeaderText("Do you want to clear the outcomes?");
			ButtonType yesButton = new ButtonType("Yes");
			ButtonType noButton = new ButtonType("No", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(yesButton, noButton);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == yesButton) {
				finPairsList.clear();
			}
		}
		double nw = Double.valueOf(eleW.getText());
		double opNW = Double.valueOf(paramW.getText());
		double parW = Double.valueOf(simThres.getText());
		double nw1 = Double.valueOf(eleW1.getText());
		double opNW1 = Double.valueOf(paramW1.getText());
		double parW1 = Double.valueOf(simThres1.getText());
		double eSim = Double.valueOf(eSimThres.getText());

		allPairs = (int) (proArr.length * (proArr.length - 1) / 2.0);
		progressBar.setProgress(0);
		btnShowAll.setDisable(true);
		btnShowSelslected.setDisable(true);

		boolean autoNotOverwrite = false;
		boolean autoOverwrite = false;
		for (int i = 0; i < proArr.length; i++) {
			for (int j = i + 1; j < proArr.length; j++) {
				try {
					// MainFormController.currentPair++;
					// System.out.println("runForByES:"+currentPair);
					// progressBar.setProgress((double)MainFormController.currentPair/
					// MainFormController.allPairs);
					JProject proI = proArr[i];
					JProject proJ = proArr[j];
					String progIname = proI.getProjectName();
					String progJname = proJ.getProjectName();
					String xmiIFolder = proI.getPathTo() + "\\" + progIname;
					String xmiJFolder = proJ.getPathTo() + "\\" + progJname;
					;
					String simType;
					if (mcs.isSelected())
						simType = "MCS";
					else if (lsa.isSelected())
						simType = "LSA";
					else
						simType = "UMBS";
					String path1 = new File(xmiIFolder).getParent();
					String path2 = new File(xmiJFolder).getParent();

					for (double nwI = nw; nwI <= nw1; nwI += 0.1) {
						for (double opNWI = opNW; opNWI <= opNW1; opNWI += 0.1) {
							for (double parWI = parW; parWI <= parW1; parWI += 0.1) {
								// String visFileSuffix = "#nw"+String.format("%.2f", nwI)
								// +"_pnw"+String.format("%.2f", opNWI)
								// +"_th"+String.format("%.2f", parWI)+"_"+ ".cvis";
								// //check all possible visualisation file names/paths
								// File visFile1 = new File(path1 + "\\output\\cvisFiles\\ProjectComp_" +
								// progIname + "#" + progJname + visFileSuffix);
								// File visFile2 = new File(path2 + "\\output\\cvisFiles\\ProjectComp_" +
								// progIname + "#" + progJname + visFileSuffix);
								// File visFile3 = new File(path1 + "\\output\\cvisFiles\\ProjectComp_" +
								// progJname + "#" + progIname + visFileSuffix);
								// File visFile4 = new File(path2 + "\\output\\cvisFiles\\ProjectComp_" +
								// progJname + "#" + progIname + visFileSuffix);
								// List<File> existingVis=new LinkedList<>();
								List<String> existingVis = new LinkedList<>();
								String doc_id1 = progIname + "_" + progJname;
								String doc_id2 = progJname + "_" + progIname;
								ESUtils searchUtils = ESUtils.getInstance();
								if (searchUtils.isFoundPrg1Prg2(progIname, progJname, null))
									existingVis.add(doc_id1);
								if (searchUtils.isFoundPrg1Prg2(progJname, progIname, null))
									existingVis.add(doc_id2);
								boolean calc = autoOverwrite || existingVis.isEmpty();
								String visFile = null;
								if (existingVis.isEmpty() == false)
									visFile = existingVis.get(0);// TODO fix to last modified
								// if(!calc)//TODO fix last modified for ES docs;
								// visFile=Collections.max(existingVis,
								// (a,b)->((Long)a.lastModified()).compareTo(b.lastModified()));
								if (!calc && !autoNotOverwrite) {
									Alert alert = new Alert(AlertType.CONFIRMATION);
									alert.setTitle("Replace or use file");
									alert.setHeaderText("A matching visualization file from"/*
																							 * +dateFormat.format(new
																							 * Date(visFile.lastModified
																							 * ()))
																							 */
											+ progIname + "," + progJname + "for those porject was found.");// TODO fix
																											// last
																											// modified
																											// for ES
																											// docs in
									// alert header;
									alert.setContentText("Do you want to use it?\n"
											+ "(Choosing no means to do all the calculations again which can take time)");

									ButtonType yesButton = new ButtonType("Yes");
									ButtonType rYesButton = new ButtonType("Yes and remember my choice");
									ButtonType noButton = new ButtonType("No", ButtonData.CANCEL_CLOSE);
									ButtonType rNoButton = new ButtonType("No and remember my choice");

									alert.getButtonTypes().setAll(yesButton, rYesButton, noButton, rNoButton);

									Optional<ButtonType> result = alert.showAndWait();
									if (result.get() == rYesButton) {
										autoNotOverwrite = true;
									} else if (result.get() == rNoButton) {
										autoOverwrite = true;
										calc = true;
									} else if (result.get() != yesButton) {
										// if close or no
										calc = true;
									}
								}
								final double nwT = nwI;
								final double opNWT = opNWI;
								final double parWT = parWI;
								if (calc) {
									THREAD_POOL.execute(() -> {

										try {
											VisualizedPair visP = new VisualizedPair(nwT, opNWT, parWT, progIname,
													progJname, doc_id1);

											ChangeListener<? super Number> finListener = new ChangeListener<Number>() {

												@Override
												public void changed(ObservableValue<? extends Number> obsVal,
														Number oldVal, Number newVal) {
													if (newVal.intValue() > 0) {
														pairsList.remove(visP);
														finPairsList.add(visP);
														MainFormController.currentPair++;
														// System.out.println("runForByES:"+currentPair);
														progressBar.setProgress((double) MainFormController.currentPair
																/ MainFormController.allPairs);
														//if (MainFormController.currentPair == MainFormController.allPairs) {
														if(pairsList.size()==0) {	
														    btnShowAll.setDisable(false);
															btnShowSelslected.setDisable(false);
														}
													}
												}

											};
											visP.progressProperty().addListener(finListener);
											pairsList.add(visP);
											runApplication.runAppByES(nwT, opNWT, parWT, simType, xmiIFolder, progIname,
													progJname, 
													proI.getProjectClasses(),
													proJ.getProjectClasses(),
													visP.progressProperty(),
													debugButton.isSelected(),
													toTypesMap(typesList), eSim, progressBar);

										} catch (Exception e) {
											e.printStackTrace();
										}
									});
									
								} else {
									// check the newest file
									finPairsList
											.add(new VisualizedPair(nwT, opNWT, parWT, progIname, progJname, visFile));
									MainFormController.currentPair++;
									// System.out.println("runForByES:"+currentPair);
									progressBar.setProgress(
											(double) MainFormController.currentPair / MainFormController.allPairs);
									if (MainFormController.currentPair == MainFormController.allPairs) {
										btnShowAll.setDisable(false);
										btnShowSelslected.setDisable(false);
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}

	private Map<CommutativePair<String, String>, Double> toTypesMap(ObservableList<TypesPair> typesList) {
		Map<CommutativePair<String, String>, Double> m = new HashMap<>();
		typesList.forEach(a -> {
			if (a.type1.length() != 0 && a.type2.length() != 0 && !Double.isNaN(Double.valueOf(a.sim)))
				m.put(new CommutativePair<>(a.type1, a.type2), Double.parseDouble(a.sim));
		});
		return m;
	}

	@FXML
	public void clearTables(ActionEvent event) {
		projectsList.clear();
		pairsList.clear();
		finPairsList.clear();
	}

	@FXML
	public void importProjects(ActionEvent event) {
		try {
			FileChooser fileChooser = new FileChooser();
			// Set extension filter
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("txt files (*.txt)", "*.txt");
			fileChooser.getExtensionFilters().add(extFilter);
			// Show save file dialog
			fileChooser.setTitle("Choose file");
			fileChooser.setTitle("Choose projects' folders' list");
			String currentDir = lastProjectLoc;
			if (currentDir == null)
				currentDir = System.getProperty("user.dir");
			fileChooser.setInitialDirectory(new File(currentDir));
			File file = fileChooser.showOpenDialog(null);
			if (file == null)
				return;
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while (reader.ready()) {
				try {
					File projectFile = new File(reader.readLine());
					if (projectFile.isDirectory())
						addProjectFolder(projectFile, false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void exportProjects(ActionEvent event) {
		try {
			FileChooser fileChooser = new FileChooser();
			// Set extension filter
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("txt files (*.txt)", "*.txt");
			fileChooser.getExtensionFilters().add(extFilter);
			// Show save file dialog
			fileChooser.setTitle("Choose file");
			fileChooser.setTitle("Choose projects' folders' list");
			String currentDir = lastProjectLoc;
			if (currentDir == null)
				currentDir = System.getProperty("user.dir");
			fileChooser.setInitialDirectory(new File(currentDir));
			File file = fileChooser.showSaveDialog(null);
			if (file == null)
				return;
			PrintStream printer = new PrintStream(file);
			for (JProject project : projectsList) {
				printer.println(project.getProjectDir());
			}
			printer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void visualize(ActionEvent event) {
		Collection<VisualizedPair> selectedVis = tb_fin_pairs.getSelectionModel().getSelectedItems();
		visualizeFor(selectedVis);
	}

	@FXML
	public void visualizeAll(ActionEvent event) {
		visualizeFor(finPairsList);
	}

	private void visualizeFor(Collection<VisualizedPair> selectedVis) {
		List<String> visFiles = new LinkedList<>();
		for (VisualizedPair visPair : selectedVis)
			visFiles.add(visPair.getVisFile());
		new Thread() {
			public void run() {
				try {
					startGraphVisualizer(ClusterGraph.class, true, visFiles);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/*
	 * private void startGraphVisualizer(Class<? extends Object> clazz, boolean
	 * redirectStream, String... args) throws Exception {
	 * System.out.println(clazz.getCanonicalName()); String separator =
	 * System.getProperty("file.separator"); String classpath =
	 * System.getProperty("java.class.path"); String path =
	 * System.getProperty("java.home") + separator + "bin" + separator + "java";
	 * List<String> commands = new LinkedList<>(Arrays.asList(path, "-cp",
	 * classpath, clazz.getCanonicalName())); commands.addAll(Arrays.asList(args));
	 * ProcessBuilder processBuilder = new ProcessBuilder(commands);
	 * processBuilder.redirectErrorStream(redirectStream); Process process =
	 * processBuilder.start(); process.waitFor(); System.out.println("Fin"); }
	 */
	private void startGraphVisualizer(Class<? extends Object> clazz, boolean redirectStream, List<String> args)
			throws InvalidFormatException, IOException {
		System.out.println("startGraphVisualizer" + clazz.getCanonicalName());
		String separator = System.getProperty("file.separator");
		String classpath = System.getProperty("java.class.path");
		String path = System.getProperty("java.home") + separator + "bin" + separator + "java";
		List<String> commands = new LinkedList<>(Arrays.asList(path, "-cp", classpath, clazz.getCanonicalName()));
		commands.addAll(args);

		new ClusterGraph(args.toArray(new String[0]));
		// ProcessBuilder processBuilder = new ProcessBuilder(commands);
		// processBuilder.redirectErrorStream(redirectStream);
		// Process process = processBuilder.start();
		// process.waitFor();
		System.out.println("startGraphVisualizer: Fin");
	}
}
