package gui;

import java.util.Collection;
import java.util.Set;

import sim.MiniJClass;

public class ProjectVertex extends SimVertex{
	public ProjectVertex(String projectName){
		super(projectName, projectName);
	}
	public ProjectVertex(String projectName, Collection<MiniJClass> set){
		super(projectName, projectName);
		set.forEach(m->operations.add(m.getClassName()));
		operations.sort(null);
	}
	public String htmlDesc(){
		String s="<html>"+name+"<br>Classes (#"+operations.size()+"):";
		for(String op:operations){
			s+="<br>"+op;	
		}
		s+="</html>";
		return s;
	}
}
