package gui;

import java.awt.event.MouseEvent;

import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;

public class MultiMouse<K,V> extends DefaultModalGraphMouse<K,V>{
	public void mousePressed(MouseEvent e)
	 {
		if(e.getButton()==MouseEvent.BUTTON1){
			this.setMode(DefaultModalGraphMouse.Mode.TRANSFORMING);
			super.mousePressed(e);
		}
		else
			if(e.getButton()==MouseEvent.BUTTON3){
				this.setMode(DefaultModalGraphMouse.Mode.PICKING);
				MouseEvent e2=new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), MouseEvent.BUTTON1_MASK,
						 e.getX(), e.getY(), e.getClickCount(), e.isPopupTrigger());
				super.mousePressed(e2);
			}
			else
				super.mousePressed(e);
	 }
	public void mouseClicked(MouseEvent e)
	{			
		if(e.getButton()==MouseEvent.BUTTON1){
			this.setMode(DefaultModalGraphMouse.Mode.TRANSFORMING);
			super.mouseClicked(e);
		}
		else
			if(e.getButton()==MouseEvent.BUTTON3){
				this.setMode(DefaultModalGraphMouse.Mode.PICKING);
				MouseEvent e2=new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), MouseEvent.BUTTON1_MASK,
						 e.getX(), e.getY(), e.getClickCount(), e.isPopupTrigger());
				super.mouseClicked(e2);
			}
			else
				super.mouseClicked(e);
	 }
}
