package gui;

public class SimEdgeExt extends SimEdge {
	private int avgSim;
	public SimEdgeExt(String pro1, String pro2, String cls1, String cls2, int useSim, int refSim, int extSim,
			int refExtSim, int overSim, SimType type, ParamArr pr) {
		super(pro1, pro2, cls1, cls2, useSim, refSim, extSim, refExtSim, overSim, type, pr);
		int cntr=0;
		if(useSim>0)
			++cntr;
		if(refSim>0)
			++cntr;
		if(extSim>0)
			++cntr;
		if(refExtSim>0)
			++cntr;
		if(overSim>0)
			++cntr;
		if(cntr==0)
			avgSim=0;
		else
			avgSim=(useSim+refSim+extSim+refExtSim+overSim)/cntr;
	}
	public String toString(){
		SimType type = getType();
		if(type.equals(SimType.UNDEFINED)||type.equals(SimType.NULL))
			return type.name();
		return avgSim+"% in "+type.name();
	}
}
