package gui;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

	/**
	 * This class defines all the screens of the application.
	 * @author AsiM
	 */
public class ScreensFramework extends Application
{

	// Main form
	public static String screen1ID = "MainFormController";
	public static String screen1File = "MainForm.fxml";

	/**
	 * The method loads all the screens framework to the main container.
	 */
	@Override
	public void start (Stage PrimaryStage)
	{
		ScreensController mainContainer = new ScreensController();
		mainContainer.loadScreen(ScreensFramework.screen1ID, ScreensFramework.screen1File);

		// Sets the first screen (main screen)
		mainContainer.setScreen(ScreensFramework.screen1ID);

		Group root = new Group();
		root.getChildren().addAll(mainContainer);
		Scene scene = new Scene(root);
		PrimaryStage.setTitle("VarMeR");
		PrimaryStage.setScene(scene);
		PrimaryStage.show();
	}

}

