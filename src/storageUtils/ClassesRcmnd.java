package storageUtils;

import com.google.gson.Gson;

import sim.SDCompBase;

public class ClassesRcmnd {
	private String prog1,prog2;
	private String class1, class2;
	private SDCompBase sdCompBase;
	private int parametric,subTyping,overloading,elementsAmount;
	private double parametricRecmnd,subTypingRcmnd,overloadingRcmnd;
	
	
	
	public SDCompBase getSdCompBase() {
		return sdCompBase;
	}
	public void setSdCompBase(SDCompBase sdCompBase) {
		this.sdCompBase = sdCompBase;
	}
	public ClassesRcmnd() {
		// TODO Auto-generated constructor stub
	}
	public String getJson()
	{
		return new Gson().toJson(this);
	}
	public String getProg1() {
		return prog1;
	}

	public void setProg1(String prog1) {
		this.prog1 = prog1;
	}

	public String getProg2() {
		return prog2;
	}

	public void setProg2(String prog2) {
		this.prog2 = prog2;
	}

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public int getParametric() {
		return parametric;
	}

	public void setParametric(int parametric) {
		this.parametric = parametric;
	}

	public int getSubTyping() {
		return subTyping;
	}

	public void setSubTyping(int subTyping) {
		this.subTyping = subTyping;
	}

	public int getOverloading() {
		return overloading;
	}

	public void setOverloading(int overloading) {
		this.overloading = overloading;
	}

	public int getElementsAmount() {
		return elementsAmount;
	}

	public void setElementsAmount(int elementsAmount) {
		this.elementsAmount = elementsAmount;
	}

	

	public double getParametricRecmnd() {
		return parametricRecmnd;
	}
	public void setParametricRecmnd(double parametricRecmnd) {
		this.parametricRecmnd = parametricRecmnd;
	}
	public double getSubTypingRcmnd() {
		return subTypingRcmnd;
	}
	public void setSubTypingRcmnd(double subTypingRcmnd) {
		this.subTypingRcmnd = subTypingRcmnd;
	}
	public double getOverloadingRcmnd() {
		return overloadingRcmnd;
	}
	public void setOverloadingRcmnd(double overloadingRcmnd) {
		this.overloadingRcmnd = overloadingRcmnd;
	}
	public void setOverloadingRcmnd(int overloadingRcmnd) {
		this.overloadingRcmnd = overloadingRcmnd;
	}
	@Override
	public String toString() {
		return "ClassesRcmnd [prog1=" + prog1 + ", prog2=" + prog2 + ", class1=" + class1 + ", class2=" + class2
				+ ", sdCompBase=" +/* sdCompBase +*/", parametric=" + parametric + ", subTyping=" + subTyping
				+ ", overloading=" + overloading + ", elementsAmount=" + elementsAmount + ", parametricRecmnd="
				+ parametricRecmnd + ", subTypingRcmnd=" + subTypingRcmnd + ", overloadingRcmnd=" + overloadingRcmnd
				+ "]";
	}
	

}
