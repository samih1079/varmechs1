package storageUtils;

import com.google.gson.Gson;

import sim.ClassSim;
import sim.MethodSim;

public class MethodsCat extends MethodSim {
	private String prog1, prog2;
	// private String class1, class2;
	// private String method1, method2;
	// private String cat;

	// public MethodsCat(String prog1, String prog2, String class1, String class2,
	// String method1, String method2,
	// String cat) {
	// super();
	// this.prog1 = prog1;
	// this.prog2 = prog2;
	// this.class1 = class1;
	// this.class2 = class2;
	// this.method1 = method1;
	// this.method2 = method2;
	// this.cat = cat;
	// }
	public MethodsCat(String cl1, String cl2, String m1, String m2, ClassSim res, String prog1, String prog2) {
		super(cl1, cl2, m1, m2, res);
		this.prog1 = prog1;
		this.prog2 = prog2;
	}
	public MethodsCat() {
		// TODO Auto-generated constructor stub
	}
	public String toJson() {
		return new Gson().toJson(this);
	}
	public String getProg1() {
		return prog1;
	}
	public void setProg1(String prog1) {
		this.prog1 = prog1;
	}
	public String getProg2() {
		return prog2;
	}
	public void setProg2(String prog2) {
		this.prog2 = prog2;
	}
	@Override
	public String toString() {
		return "MethodsCat [prog1=" + prog1 + ", prog2=" + prog2 + ", toString()=" + super.toString() + "]";
	}
	
	
}
