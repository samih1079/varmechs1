package storageUtils;

import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

//import shapeless.record;
import sim.MiniJClass;
import sim.ProjectSim;
import sim.SDComp;

public class CvisReccs 
{
	
	public String proj1Name;
	public String proj2Name;	
	public List<SDComp> reccs;		

	public CvisReccs() {
		// TODO Auto-generated constructor stub
	}

	public String toJson()
	{
		return new Gson().toJson(this);
	}
	public static CvisReccs fromJson(String json)
	{
		return new Gson().fromJson(json, CvisReccs.class);
		
	}

	@Override
	public String toString() {
		return toJson();
	}
	
}
