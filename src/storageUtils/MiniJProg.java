package storageUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import sim.MiniJClass;

public class MiniJProg  {
	private String progName;
	public List<MiniJClass> json1Classes;
	public MiniJProg(String progName, List<MiniJClass> json1Classes) {
		super();
		this.progName = progName;
		this.json1Classes = json1Classes;
	}
	public String getProgName() {
		return progName;
	}
	public void setProgName(String progName) {
		this.progName = progName;
	}
	public List<MiniJClass> getJson1Classes() {
		return  json1Classes;
	}
	public void setJson1Classes(List<MiniJClass> json1Classes) {
		this.json1Classes = json1Classes;
	}
	
	
	
	

}
