package storageUtils;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.settings.Setting;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
//import org.netlib.util.booleanW;

import com.google.gson.Gson;

import sim.CommutativePointerPair;
import sim.JMethod;
import sim.MethodSim;
import sim.MiniJClass;
import sim.ProjectSim;
import sim.SDComp;

final public class ESUtils {
	public static final int JUST_ES = 1, BOTH = 2, JUST_FILES = 3;
	public static int EsORFiles = JUST_ES;
	public static String BaseINDEX = "indextest";
	public static final String TYPe_CVIS = "cvis_type";

	public static final String PARAMS_TYPE = "params";
	public static final String MINIJPROG_TYPE = "minijprog";
	public static final String PROJECT_SIM_TYPE = "projectsim";
	public static final String CLASSES_RCMNDS_TYPE = "classrmnds";
	public static final String METHODS_CAT_TYPE = "methodscat";
	public static String PARAMS_Index = "";
	public static String MINIJPROG_Index;
	public static String PROJECT_SIM_Index;
	public static String CLASSES_RCMNDS_Index;
	public static String METHODS_CAT_Index;

	public static ESUtils els;
	private TransportClient client;
	private BulkRequestBuilder bulkRequest = null;
	private BulkProcessor bulkProcessor;
	private static boolean IS_ALL_INDICES_CREATED = false;

	private ESUtils() throws IOException {
		initSever();
	}

	public static synchronized ESUtils getInstance() throws IOException {
		if (els == null)
			els = new ESUtils();
		return els;
	}

	public void initSever() throws IOException {
		// TODO Auto-generated constructor stub
		Settings settings = Settings.builder().put("cluster.name", "mycluster")
				.put("client.transport.ignore_cluster_name", false)
				// .put("node.client", true)
				.put("client.transport.sniff", true).build();
		client = new PreBuiltTransportClient(settings)
				.addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300))
				.addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));
		System.out.println("Elastic Search Ready- client: " + client);
	}

	public TransportClient getClient() {
		return client;
	}

	public BulkProcessor getBulkProssesor() {
		if (bulkProcessor == null) {
			bulkProcessor = BulkProcessor.builder(client, new BulkProcessor.Listener() {
				@Override
				public void beforeBulk(long executionId, BulkRequest request) {
				}

				@Override
				public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
				}

				@Override
				public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
				}
			}).setBulkActions(10000).setBulkSize(new ByteSizeValue(5, ByteSizeUnit.MB))
					.setFlushInterval(TimeValue.timeValueSeconds(5)).setConcurrentRequests(1)
					.setBackoffPolicy(BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)).build();
		}
		return bulkProcessor;
	}

	public synchronized void  addJsonToBulk(String index, String type, String json, String id, String op) {
	
		printLog("addJsonToBulk",index);

		if (bulkRequest == null)
			bulkRequest = client.prepareBulk();
		if (op.equals("add")) {
			printLog("addJsonToBulk add",index);

			bulkRequest.add(addJsonToTypeRequest(index, type, json, id));
		}
		if (op.equals("get")) {
			if (bulkRequest.numberOfActions() > 0) {
				BulkResponse bulkResponse = bulkRequest.get();
				if (bulkResponse.hasFailures()) {
					printLog("addJsonToBulk", bulkResponse.buildFailureMessage());
				} else {
					printLog("addJsonToBulk", ": OK");

				}
			}
			 else
					printLog("addJsonToBulk:", "no requests!!");
		}

	}

	public void addJsonToType(String index, String type, String json, String id) {
		index = index.toLowerCase();
		// TODO samih: save index at field dont check it always
		if (IS_ALL_INDICES_CREATED = false)
			createIfNotFound(index, type);
		// String smap = getMappingType(type);
		// if (smap != null && smap.length() > 0) {
		// client.admin().indices().preparePutMapping(index).setType(type).setSource(smap,
		// XContentType.JSON).get();
		// }
		long s = System.currentTimeMillis();
		printLog("addJsonToType: start " + type, s);
		IndexResponse response = null;
		if (id == null) {
			response = client.prepareIndex(index, type).setSource(json, XContentType.JSON).get();
			// System.out.println("addJsonToType :getResult:"+response.getIndex()+"," +
			// response.getResult());
		} else {
			response = client.prepareIndex(index, type, id).setSource(json, XContentType.JSON).get();
			// System.out.println("addJsonToType :getResult:" +response.getIndex()+","+
			// response.getResult());
		}
		printLog("addJsonToType: end", (s - System.currentTimeMillis()));
		if (response != null) {
			System.out.println("addJsonToType:" + response.status());
		}
	}

	public IndexRequestBuilder addJsonToTypeRequest(String index, String type, String json, String id) {
		index = index.toLowerCase();
		// createIfNotFound(index, type);

		// long s = System.currentTimeMillis();
		// printLog("addJsonToType: start "+type,s );
		if (id == null) {
			// bulkProcessor.add(client.prepareIndex(index, type).setSource(json,
			// XContentType.JSON).request());
			return client.prepareIndex(index, type).setSource(json, XContentType.JSON);
			// System.out.println("addJsonToType :getResult:"+response.getIndex()+"," +
			// response.getResult());
		} else {
			return client.prepareIndex(index, type, id).setSource(json, XContentType.JSON);
			// System.out.println("addJsonToType :getResult:" +response.getIndex()+","+
			// response.getResult());
		}

	}

	public static void printLog(String tag, Object o) {
		System.out.println(tag + ":" + o);

	}

	public void createEmptyIndex(String index, String type) {

		CreateIndexResponse res = client.admin().indices().prepareCreate(index).get();

		System.out.println("createEmptyIndex:" + res.index());
		if (type != null) {
			String smap = getMappingType(type);
			if (smap != null && smap.length() > 0) {
				client.admin().indices().preparePutMapping(index).setType(type).setSource(smap, XContentType.JSON)
						.get();
			}
		}

	}

	public CreateIndexRequestBuilder getPreparedIndex(String index, String type) {

		CreateIndexRequestBuilder res = client.admin().indices().prepareCreate(index);
		if (type != null) {
			String smap = getMappingType(type);
			if (smap != null && smap.length() > 0) {
				res.addMapping(type, smap, XContentType.JSON);
			}

		}
		return res;
	}

	public void createAllIndices(String index) {
		createEmptyIndex(index, null);

		PARAMS_Index = index + PARAMS_TYPE;
		createEmptyIndex(PARAMS_Index, PARAMS_TYPE);
		MINIJPROG_Index = index + MINIJPROG_TYPE;
		createEmptyIndex(MINIJPROG_Index, MINIJPROG_TYPE);
		PROJECT_SIM_Index = index + PROJECT_SIM_TYPE;
		createEmptyIndex(PROJECT_SIM_Index, PROJECT_SIM_TYPE);
		CLASSES_RCMNDS_Index = index + CLASSES_RCMNDS_TYPE;
		createEmptyIndex(CLASSES_RCMNDS_Index, CLASSES_RCMNDS_TYPE);
		METHODS_CAT_Index = index + METHODS_CAT_TYPE;
		createEmptyIndex(METHODS_CAT_Index, METHODS_CAT_TYPE);
		IS_ALL_INDICES_CREATED = true;
	}

	public void overwriteAllIndex(String index) {
		overwriteIndex(index, null);
		PARAMS_Index = index + PARAMS_TYPE;
		overwriteIndex(PARAMS_Index, PARAMS_TYPE);
		MINIJPROG_Index = index + MINIJPROG_TYPE;
		overwriteIndex(MINIJPROG_Index, MINIJPROG_TYPE);
		PROJECT_SIM_Index = index + PROJECT_SIM_TYPE;
		overwriteIndex(PROJECT_SIM_Index, PROJECT_SIM_TYPE);
		CLASSES_RCMNDS_Index = index + CLASSES_RCMNDS_TYPE;
		overwriteIndex(CLASSES_RCMNDS_Index, CLASSES_RCMNDS_TYPE);
		METHODS_CAT_Index = index + METHODS_CAT_TYPE;
		overwriteIndex(METHODS_CAT_Index, METHODS_CAT_TYPE);
		IS_ALL_INDICES_CREATED = true;
	}

	/**
	 * check if index is found, otherwise new index will be created and return false
	 * 
	 * @param indexname
	 * @param type
	 * @return true if found, otherwise new index will be created and return false
	 */
	public boolean createIfNotFound(String indexname, String type) {

		IndicesExistsResponse res = client.admin().indices().prepareExists(indexname).get();
		if (res.isExists()) {
			return true;
		} else {
			if (type == null)
				createAllIndices(indexname);
			else
				createEmptyIndex(indexname, type);
			return false;
		}

		// String[] indices =
		// client.admin().indices().prepareGetIndex().setFeatures().get().getIndices();
		//
		// for (String indx : indices) {
		// if(indx.equalsIgnoreCase(indexname))
		// return true;
		// }

		// SearchResponse response = client.prepareSearch(indexname).get();
		// int status=response.status().getStatus();
		// //index found
		// return true;

	}

	public void overwriteIndex(String index, String type) {
		IndicesExistsResponse res = client.admin().indices().prepareExists(index).get();
		if (res.isExists()) {
			DeleteIndexResponse res2 = client.admin().indices().prepareDelete(index).get();
			System.out.println("deleteIndex:" + index + "," + res2.isAcknowledged());
			createEmptyIndex(index, type);

		} else
			createEmptyIndex(index, type);
	}

	public static String getMappingType(String type) {
		String mapping = "";
		Path currentRelativePath = Paths.get("");
		String path = currentRelativePath.toAbsolutePath().toString();
		try {
			mapping = new String(Files.readAllBytes(Paths.get(path + "\\src\\storageUtils\\" + type + ".json")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mapping;
		}
		return mapping;
	}

	public void addCVISTable(CvisObjToJson cvisObjToJson, String missionName) {

		Path currentRelativePath = Paths.get("");
		String path = currentRelativePath.toAbsolutePath().toString();
		String smap;
		try {
			smap = new String(Files.readAllBytes(Paths.get(path + "\\src\\storageUtils\\cvismaping.json")));
			client.admin().indices().preparePutMapping(missionName).setType(TYPe_CVIS)
					.setSource(smap, XContentType.JSON).get();
			IndexResponse response = client
					.prepareIndex(missionName, TYPe_CVIS, cvisObjToJson.proj1Name + "_" + cvisObjToJson.proj2Name)
					.setSource(cvisObjToJson.toJson(), XContentType.JSON).get();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * get all cvis of mission
	 * 
	 * @param missionName
	 * @param type
	 * @return
	 */
	public CvisObjToJson getCVISTable(String missionName, String type) {
		SearchRequest request = new SearchRequest(missionName);
		request.types(type);
		SearchResponse searchResponse = client.search(request).actionGet();
		return CvisObjToJson.fromJson(searchResponse.getHits().getHits()[0].getSourceAsString());
	}

	public CvisObjToJson read2ProgCvis(String missionName, String type, String proj1, String proj2) {
		SearchResponse searchResponse = client.prepareSearch(missionName).setTypes(type)
				.setQuery(QueryBuilders.termQuery("_id", proj1 + "_" + proj2)).get();
		if (searchResponse.getHits().getTotalHits() > 0) {
			return CvisObjToJson.fromJson(searchResponse.getHits().getHits()[0].getSourceAsString());
		} else
			return null;
	}

	public CvisObjToJson read2ProgCvis(String missionName, String type, String cvisFilename) {
		SearchResponse searchResponse = client.prepareSearch(missionName).setTypes(type)
				.setQuery(QueryBuilders.termQuery("_id", cvisFilename)).get();
		if (searchResponse.getHits().getTotalHits() > 0) {
			return CvisObjToJson.fromJson(searchResponse.getHits().getHits()[0].getSourceAsString());
		} else
			return null;
	}

	public Integer[] getParams(String proj1, String proj2, String index) {
		if (index == null)
			index = PARAMS_Index;
		SearchResponse searchResponse = client.prepareSearch(index).setTypes(PARAMS_TYPE)
				.setQuery(QueryBuilders.termQuery("_id", proj1 + "#" + proj2)).get();
		if (searchResponse.getHits().getTotalHits() > 0) {
			ParamsObj parms = new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(),
					ParamsObj.class);
			return parms.getParams2();
		} else
			return null;
	}

	public List<MiniJClass> getMiniClasses(String projName, String index) {
		if (index == null)
			index = MINIJPROG_Index;
		SearchResponse searchResponse = client.prepareSearch(index).setTypes(MINIJPROG_TYPE)
				.setQuery(QueryBuilders.termQuery("_id", projName)).get();
		if (searchResponse.getHits().getTotalHits() > 0) {
			MiniJProg parms = new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(),
					MiniJProg.class);
			return parms.getJson1Classes();
		} else
			return null;
	}

	public ProjectSim getProjectSim(String proj1, String proj2, String index) {
		if (index == null)
			index = PROJECT_SIM_Index;
		SearchResponse searchResponse = client.prepareSearch(index).setTypes(PROJECT_SIM_TYPE)
				.setQuery(QueryBuilders.termQuery("_id", proj1 + "#" + proj2)).get();
		if (searchResponse.getHits().getTotalHits() > 0) {
			ProjectSim obj = new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(),
					ProjectSim.class);
			return obj;
		} else
			return null;
	}

	public List<ClassesRcmnd> getCLassRcmnds(String proj1, String proj2, String index) {
		List<ClassesRcmnd> list = new LinkedList<ClassesRcmnd>();
		if (index == null)
			index = CLASSES_RCMNDS_Index;
		SearchResponse searchResponse = client
				.prepareSearch(index).setTypes(CLASSES_RCMNDS_TYPE).setQuery(QueryBuilders.boolQuery()
						.must(QueryBuilders.termQuery("prog1", proj1)).must(QueryBuilders.termQuery("prog2", proj2)))
				.get();
		// System.out.println(new
		// Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(),ClassesRcmnd.class));
		for (SearchHit sh : searchResponse.getHits().getHits()) {
			list.add(
					new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(), ClassesRcmnd.class));
		}

		return list;
	}

	public List<MethodsCat> getMethodsCat(String proj1, String proj2, String clas1, String clas2, String index) {
		List<MethodsCat> list = new LinkedList<MethodsCat>();
		if (index == null)
			index = METHODS_CAT_Index;
		SearchResponse searchResponse = client.prepareSearch(index).setTypes(METHODS_CAT_TYPE)
				.setQuery(QueryBuilders.boolQuery().must(QueryBuilders.termQuery("prog1", proj1))
						.must(QueryBuilders.termQuery("prog2", proj2)).must(QueryBuilders.termQuery("cl1", clas1))
						.must(QueryBuilders.termQuery("cl2", clas2)))
				.get();
		System.out.println(
				new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(), ClassesRcmnd.class));
		for (SearchHit sh : searchResponse.getHits().getHits()) {
			list.add(new Gson().fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(), MethodsCat.class));
		}

		return list;
	}

	/**
	 * check if tow programs if found (using params index and type )
	 * 
	 * @param proj1
	 * @param proj2
	 * @param index
	 * @return
	 */
	public boolean isFoundPrg1Prg2(String proj1, String proj2, String index) {

		boolean isFound = false;
		if (index != null) {
			SearchResponse searchResponse = client.prepareSearch(index).setTypes(ESUtils.PARAMS_TYPE)
					.setQuery(QueryBuilders.termQuery("_id", proj1 + "#" + proj2)).get();
			isFound = searchResponse.getHits().getTotalHits() > 0;
		} else {
			SearchResponse searchResponse = client.prepareSearch(ESUtils.PARAMS_Index).setTypes(ESUtils.PARAMS_TYPE)
					.setQuery(QueryBuilders.termQuery("_id", proj1 + "#" + proj2)).get();
			isFound = searchResponse.getHits().getTotalHits() > 0;
		}
		return isFound;
	}

	/**
	 * 
	 * @param db
	 *            = Index ElasticSearch
	 * @param table
	 *            =type ElasticSearch
	 * @param id
	 *            = tbl_vis
	 * @param prog2name
	 * 
	 * @param projectSim
	 *            =
	 * @param reccs
	 * @throws IOException
	 */
	public void addCVISDocumnet(String db, String table, String prog1name, String prog2name, List<SDComp> reccs,
			ProjectSim projectSim) throws IOException {
		String id = prog1name + "_" + prog2name;
		XContentBuilder builder = jsonBuilder().startObject().field("reccs", new Gson().toJson(reccs))
				.field("projectSim", new Gson().toJson(projectSim)).endObject();
		String json = Strings.toString(builder);
		System.out.println("addDocumnet json" + json);
		IndexResponse response = client.prepareIndex(db, table, id).setSource(builder).get();
		// Index name
		String _index = response.getIndex();
		// Type name
		String _type = response.getType();
		// Document ID (generated or not)
		String _id = response.getId();
		// Version (if it's the first time you index this document, you will get: 1)
		long _version = response.getVersion();

		System.out.println("addDocumnet response:" + _index + "," + _type + "," + _id + "," + _version);
	}

	public void addSimilarityMethodDoc(String missionName, String projname1, String projname2,
			Map<CommutativePointerPair<JMethod, JMethod>, MethodSim> metRes) {
		String index = missionName + "_" + projname1 + "_" + projname2;
		String type = "methods";

		PutMappingRequest request = new PutMappingRequest(index);
		request.type(type);
		try {
			XContentBuilder mappingbuilder = jsonBuilder();
			mappingbuilder.startObject();
			{
				mappingbuilder.startObject("properties").startObject("Class1Name").field("type", "keyword").endObject()
						.startObject("Method1Name").field("type", "keyword").endObject().startObject("Class2Name")
						.field("type", "keyword").endObject().startObject("Method2Name").field("type", "keyword")
						.endObject().startObject("cat").field("type", "keyword").endObject();
				mappingbuilder.endObject();
			}
			request.source(mappingbuilder);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (MethodSim metComp : metRes.values()) {
			XContentBuilder builder;
			try {
				builder = jsonBuilder().startObject().field("Class1Name", metComp.getClass1())
						.field("Method1Name", metComp.getMethod1()).field("Class2Name", metComp.getClass2())
						.field("Method2Name", metComp.getMethod2()).field("cat", metComp.getType().name()).endObject();
				// String json = Strings.toString(builder);
				// System.out.println("addDocumnet json" + json);
				IndexResponse response = client.prepareIndex(index, type).setSource(builder).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void addSimilarityMichanismDoc(String missionName, String projname1, String projname2, List<SDComp> reccs) {
		String index = missionName + "_" + projname1 + "_" + projname2;
		String type = "mechanism";

		PutMappingRequest request = new PutMappingRequest(index);
		request.type(type);
		try {
			XContentBuilder mappingbuilder = jsonBuilder();
			mappingbuilder.startObject();
			{
				mappingbuilder.startObject("properties").startObject("Class1Name").field("type", "keyword").endObject()
						.startObject("Class2Name").field("type", "keyword").endObject().startObject("parametric")
						.field("type", "double").endObject().startObject("subtyping").field("type", "double")
						.endObject().startObject("overloading").field("type", "double").endObject()
						.startObject("elementsAmount").field("type", "double").endObject()
						.startObject("parametricRecommendation").field("type", "double").endObject()
						.startObject("subTypingRecommendation").field("type", "double").endObject()
						.startObject("overloadingRecommendation").field("type", "double").endObject();
				mappingbuilder.endObject();
			}
			request.source(mappingbuilder);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (SDComp tempInf : reccs) {

			int subTyping = tempInf.getRef() + tempInf.getExt() + tempInf.getRefExt();
			int total = tempInf.getTot();
			double subPre = ((double) subTyping) / total;
			XContentBuilder builder;
			try {
				builder = jsonBuilder().startObject().field("Class1Name", tempInf.getClass1())
						.field("Class2Name", tempInf.getClass2()).field("parametric", tempInf.getUse())
						.field("subtyping", subTyping).field("overloading", tempInf.getOverloading())
						.field("elementsAmount", total).field("parametricRecommendation", tempInf.getUsePart())
						.field("subTypingRecommendation", subPre)
						.field("overloadingRecommendation", tempInf.getOverloadingPart()).endObject();
				// String json = Strings.toString(builder);
				// System.out.println("addDocumnet json" + json);
				IndexResponse response = client.prepareIndex(index, type).setSource(builder).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
