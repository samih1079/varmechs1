package storageUtils;

import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

//import shapeless.record;
import sim.MiniJClass;
import sim.ProjectSim;
import sim.SDComp;

public class CvisObjToJson 
{
	//public String  missionName;
	public Integer[] params;
	public String proj1Name;
	public String proj2Name;
	public List<MiniJClass> json1Classes;
	public List<MiniJClass> json2Classes;
	
	public List<SDComp> reccs;
	public ProjectSim projectSim;
	
	
	public CvisObjToJson(String missionName, Integer[] params, String proj1Name, String proj2Name,
			List<MiniJClass> json1Classes, List<MiniJClass> json2Classes, List<SDComp> reccs, ProjectSim projectSim) {
		//this.missionName = missionName;
		this.params = params;
		this.proj1Name = proj1Name;
		this.proj2Name = proj2Name;
		this.json1Classes = json1Classes;
		this.json2Classes = json2Classes;
		this.reccs = reccs;
		this.projectSim = projectSim;
	}

	public CvisObjToJson() {
		// TODO Auto-generated constructor stub
	}

	public String toJson()
	{
		return new Gson().toJson(this);
	}
	public static CvisObjToJson fromJson(String json)
	{
		return new Gson().fromJson(json, CvisObjToJson.class);
		
	}

	@Override
	public String toString() {
		return toJson();
	}
	
}
