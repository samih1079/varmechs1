package sim;

public class JPointer<T> {
	final private T obj;
	public JPointer(T obj){
		this.obj=obj;
	}
	public T getObj() {
		return obj;
	}
	@Override
	public int hashCode(){
		return obj.hashCode();
	}
	@Override
	public boolean equals(Object other){
		if (other instanceof JPointer)
			return obj==((JPointer)other).obj;
		return false;
	}
}
