package sim;


import javafx.util.Pair;

public class CommutativePair<K,V> extends Pair<K,V> {
	private static final long serialVersionUID = 7971847932701896293L;
	public CommutativePair(K arg0, V arg1) {
		super(arg0, arg1);
	}
	@Override
	public int hashCode() {
		return getKey().hashCode()+getValue().hashCode();
	}
	public boolean equals(Object other){
		if (other instanceof CommutativePair) {
			CommutativePair<Object,Object> otherPair=(CommutativePair) other;
			if((otherPair.getKey().equals(getKey()))&&(otherPair.getValue().equals(getValue())))
				return true;
			return (otherPair.getKey().equals(getValue()))&&(otherPair.getValue().equals(getKey()));
		}	
		return false;
	}
}
