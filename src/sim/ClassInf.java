package sim;

public class ClassInf {
	private String cl1;
	private String cl2;
	private int inh;
	private int tmp;
	private int met;
	public ClassInf(){
		this.cl1="";
		this.cl2="";
		this.inh=0;
		this.tmp=0;
		this.met=0;
	}
	public ClassInf(String cl1, String cl2, int inh, int tmp, int met){
		this.cl1=cl1;
		this.cl2=cl2;
		this.inh=inh;
		this.tmp=tmp;
		this.met=met;
	}
	public String getClass1(){
		return this.cl1;
	}
	public String getClass2(){
		return this.cl2;
	}
	public int getInh(){
		return this.inh;
	}
	public int getTemp(){
		return this.tmp;
	}
	public int getMet(){
		return this.met;
	}
}
