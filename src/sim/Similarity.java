package sim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Semaphore;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import java.sql.Connection;

import entities.DBconn;
import gui.MainFormController;
import gui.runApplication;
import javafx.beans.property.DoubleProperty;
import javafx.util.Pair;
import storageUtils.ClassesRcmnd;
import storageUtils.CvisObjToJson;
import storageUtils.ESUtils;
import storageUtils.MethodsCat;
import storageUtils.MiniJProg;
import storageUtils.ParamsObj;

public class Similarity {

	// ----------------------------------------------- Class Attributes
	// ------------------------------------------------
	// Variables from GUI
	private String simtype; // Represents the similarity tool
	private double elNameWeight;
	private double parNameWeight;
	private double simTh;

	// Similarity tools
	private MCS mcs = new MCS();
	private UMBCSimilarity umbs = new UMBCSimilarity();
	private LSAsimilarity lsa = new LSAsimilarity();
	// private static final int catNum=8;
	// Program & classes names
	private String prog1name;
	private String prog2name;

	private List<SDComp> reccs;
	private Map<CommutativePointerPair<JMethod, JMethod>, MethodSim> metRes;
	/*
	 * private int SpTH; private int TmpTH; private String[] cats;
	 */
	String paramSuffix;
	int totMethods1 = 0;
	int totMethods2 = 0;
	private int[] totFromCat;
	private int[] sameClsFromCat;

	public enum Polymorphizm {
		CLOSE, FAR, BETWEEN
	};

	final static Semaphore xlSem = new Semaphore(1, true);

	final private DoubleProperty progress;

	private double edgesThres;

	private Map<String, JClass> nameToCls;
	private Map<PointerPair<JClass, JClass>, SDComp> clsRes;

	private static PrintStream debugCSV = null;

	private static PrintStream resultsCSV = null;

	private Map<CommutativePair<String, String>, Double> typesMap;
	public static final Gson GSON = new Gson();

	// ----------------------------------------------- Constructors
	// ------------------------------------------------

	/**
	 * Constructor.
	 * 
	 * @param simtype
	 * @param progress
	 * @param eSim
	 * @param program1
	 *            name
	 * @param program2
	 *            name
	 */
	public Similarity(String simtype, String prog1name, String prog2name, DoubleProperty progress, boolean isDebug,
			Map<CommutativePair<String, String>, Double> typesMap, double edgesThres) { // "MCS","UMB","LSA"
		this.edgesThres = edgesThres;
		this.typesMap = typesMap;
		File dbrsDir = new File("debug and results\\");
		if (!dbrsDir.exists())
			dbrsDir.mkdirs();
		if (isDebug && debugCSV == null)
			try {
				debugCSV = new PrintStream(
						new File("debug and results\\debugInfo_" + new GregorianCalendar().getTimeInMillis() + ".csv"));
				addCsvLine(debugCSV, "Method 1,Class 1,Program 1,Method 2,Class 2,Program 2,"
						+ "Params 1, Params2,Return 1,Return 2,Used 1, Used 2,Mod 1,Mod 2,Shallow,Deep,Result");
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		try {
			if (resultsCSV == null) {
				resultsCSV = new PrintStream(
						new File("debug and results\\results_" + new GregorianCalendar().getTimeInMillis() + ".csv"));
				addCsvLine(resultsCSV,
						"Project 1,Project 2,wa,wp,th,#parametric (>80%),#subtyping (>80%),#overloading (>80%), #total connections");
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		this.simtype = simtype;
		this.prog1name = prog1name;
		this.prog2name = prog2name;
		this.progress = progress;
		reccs = new LinkedList<SDComp>();
		metRes = new LinkedHashMap<>();
		totFromCat = new int[ClassSim.values().length];
		sameClsFromCat = new int[ClassSim.values().length];
		nameToCls = new LinkedHashMap<>();
		clsRes = new LinkedHashMap<PointerPair<JClass, JClass>, SDComp>() {
			private static final long serialVersionUID = 7364867164621817630L;

			public SDComp get(Object key) {
				SDComp retVal = super.get(key);
				if (retVal == null) {
					try {
						@SuppressWarnings("unchecked")
						PointerPair<JClass, JClass> pairK = (PointerPair<JClass, JClass>) key;
						// System.err.println(pairK.getKey()+" "+pairK.getValue());
						Connection conn = ((ConnThread) Thread.currentThread()).getConn();
						retVal = classcompar(pairK.getKey(), pairK.getValue(), conn);
						put(pairK, retVal);
					} catch (ClassCastException e) {
						e.printStackTrace();
					}
				}
				return retVal;
			}
		};
	}
	// ----------------------------- Similarity Calculations + Clustering operations
	// --------------------------------

	private void addCsvLine(PrintStream stream, Object... objectArr) {
		if (stream == null)
			return;
		String data = "";
		for (Object str : objectArr) {
			data += str + ",";
		}
		stream.println(data);
	}

	/**
	 * Main "compare and cluster" method
	 * 
	 * @param cvisObjToJson
	 * 
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void calcsim(final List<JClass> json1Classes, final List<JClass> json2Classes, double elNameWeight,
			double parNameWeight, double simTh, final String path) throws IOException, InvalidFormatException {
		paramSuffix = "#nw" + String.format("%.2f", elNameWeight) + "_pnw" + String.format("%.2f", parNameWeight)
				+ "_th" + String.format("%.2f", simTh) + "_";
		Calendar calendar = GregorianCalendar.getInstance();
		long timeBegin = calendar.getTimeInMillis();
		// System.err.println();
		int prg1Size = json1Classes.size();
		int prg2Size = json2Classes.size();
		final PrintStream bw = new PrintStream(
				new FileOutputStream(path + "\\output\\logFiles\\logfile_" + prog1name + "#" + prog2name + ".txt"));
		if (prg1Size * prg2Size == 0) {
			bw.println("One or more projects are empty");
			bw.close();
			return;
		}
		int totComp = prg1Size * prg2Size;
		// Counter i = new Counter();
		Counter i = new Counter();
		this.elNameWeight = elNameWeight;
		this.parNameWeight = parNameWeight;
		this.simTh = simTh;
		Map<JClass, Pair<JClass, Double>> simEdge = new LinkedHashMap<>();

		// initialise name-to-class' map
		for (final JClass cl1 : json1Classes) {
			nameToCls.put(cl1.getClassName() + "##1", cl1);
			totMethods1 += cl1.getMethods().size();
		}
		for (final JClass cl2 : json2Classes) {
			nameToCls.put(cl2.getClassName() + "##2", cl2);
			totMethods2 += cl2.getMethods().size();
		}

		// initialise classes' compare results' map
		for (final JClass cl1 : json1Classes)
			for (final JClass cl2 : json2Classes)
				clsRes.put(new PointerPair<>(cl1, cl2), null);

		/*
		 * json1Classes.parallelStream().forEach(cl1->{
		 * json2Classes.parallelStream().forEach(cl2->{
		 * //System.err.println(cl1.getClassName()+" "+cl1.getClassName());
		 * //System.err.println(); double sim=clsRes.get(new
		 * PointerPair<>(cl1,cl2)).getHuerVal(); String cl1Name=cl1.getClassName();
		 * String cl2Name=cl2.getClassName(); double progressPre=i.inc()/totComp;
		 * progress.set(progressPre); bw.println((int)100*progressPre +
		 * "% - calculated classes: " + cl1Name + " - " + cl2Name); Pair<JClass, Double>
		 * tUs1 = simEdge.get(cl1); if(tUs1==null||tUs1.getValue()<sim) simEdge.put(cl1,
		 * new Pair<JClass, Double>(cl2,sim)); Pair<JClass, Double> tUs2 =
		 * simEdge.get(cl2); if(tUs2==null||tUs2.getValue()<sim) simEdge.put(cl2, new
		 * Pair<JClass, Double>(cl1,sim)); }); });
		 */
		for (final JClass cl1 : json1Classes)
			for (final JClass cl2 : json2Classes) {
				ConnThread tr = new ConnThread(DBconn.getConn()) {
					public void run() {
						// System.err.println(cl1+" "+cl2);
						// System.err.println(cl1.getClassName()+" "+cl1.getClassName());
						// System.err.println();
						double sim = clsRes.get(new PointerPair<>(cl1, cl2)).getHuerVal();
						DBconn.closeConn(getConn());
						String cl1Name = cl1.getClassName();
						String cl2Name = cl2.getClassName();
						double progressPre = i.inc() / totComp;
						progress.set(progressPre);
						bw.println((int) 100 * progressPre + "% - calculated classes: " + cl1Name + " - " + cl2Name);
						Pair<JClass, Double> tUs1 = simEdge.get(cl1);
						if (tUs1 == null || tUs1.getValue() < sim)
							simEdge.put(cl1, new Pair<JClass, Double>(cl2, sim));
						Pair<JClass, Double> tUs2 = simEdge.get(cl2);
						if (tUs2 == null || tUs2.getValue() < sim)
							simEdge.put(cl2, new Pair<JClass, Double>(cl1, sim));
					}
				};
				tr.start();
				try {
					tr.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		// System.err.println(simEdge);
		Set<CommutativePointerPair<JClass, JClass>> listCls = new LinkedHashSet<>();
		simEdge.entrySet().forEach(
				a -> listCls.add(new CommutativePointerPair<JClass, JClass>(a.getKey(), a.getValue().getKey())));
		int arrLen = ClassSim.values().length;
		int[] realTotFromCat = new int[arrLen];
		int realTotMetRes = 0;
		for (CommutativePointerPair<JClass, JClass> params : listCls) {
			int[] totFromCat = new int[arrLen];
			int[] sameClsFromCat = new int[arrLen];
			JClass cl1 = params.getKey();
			JClass cl2 = params.getValue();
			boolean sameCls = cl1.getClassName().equals(cl2.getClassName());
			Set<JMethod> cls1methods = cl1.getMethods();
			Set<JMethod> cls2methods = cl2.getMethods();
			// totMethods2+=cls2methods.size();
			int totMetRes = 0;
			for (JMethod m1 : cls1methods) {
				for (JMethod m2 : cls2methods) {
					MethodSim methodSim = metRes.get(new CommutativePointerPair<>(m1, m2));
					if (methodSim != null) {
						totMetRes++;
						ClassSim res = methodSim.getType();
						totFromCat[res.ordinal()]++;
						realTotFromCat[res.ordinal()]++;
						if (sameCls)
							sameClsFromCat[res.ordinal()]++;
					}
				}
			}
			realTotMetRes += totMetRes;
			if (totMetRes > 0)
				for (int j = 0; j < arrLen; j++) {
					this.totFromCat[j] += totFromCat[j] * 100 / totMetRes;
					this.sameClsFromCat[j] += sameClsFromCat[j] * 100 / totMetRes;
					// System.err.println(this.totFromCat[j]+" "+this.sameClsFromCat[j]);
				}
			// System.err.println();
		}

		/*
		 * int maxMethods=Math.max(totMethods1, totMethods2); fixedCatSt(totFromCat,
		 * maxMethods); fixedCatSt(sameClsFromCat, maxMethods);
		 */
		this.reccs.sort((a, b) -> {
			int c1Vc1 = a.getClass1().compareTo(b.getClass1());
			if (c1Vc1 != 0)
				return c1Vc1;
			return a.getClass2().compareTo(b.getClass2());
		});
		ProjectSim projectSim = new ProjectSim(totFromCat[ClassSim.USE.ordinal()],
				totFromCat[ClassSim.REFINEMENT.ordinal()], totFromCat[ClassSim.EXTENSION.ordinal()],
				totFromCat[ClassSim.REFINED_EXTENSION.ordinal()], totFromCat[ClassSim.OVERLOADING.ordinal()],
				100 * listCls.size());

		PrintStream visInfo = new PrintStream(new FileOutputStream(
				path + "\\output\\cvisFiles\\ProjectComp_" + prog1name + "#" + prog2name + paramSuffix + ".cvis",
				true));// the file format to show visualisation
		visInfo.println(new Gson().toJson(reccs));// writes all the compare results the the vis file
		visInfo.println(new Gson().toJson(projectSim));// writes project similarity
		visInfo.close();
		addCsvLine(resultsCSV, prog1name, prog2name, String.format("%.2f", elNameWeight),
				String.format("%.2f", parNameWeight), String.format("%.2f", simTh),
				realTotFromCat[ClassSim.USE.ordinal()],
				realTotFromCat[ClassSim.REFINEMENT.ordinal()] + realTotFromCat[ClassSim.EXTENSION.ordinal()]
						+ realTotFromCat[ClassSim.REFINED_EXTENSION.ordinal()],
				realTotFromCat[ClassSim.OVERLOADING.ordinal()], realTotMetRes);
		toExcel(path);
		sumProject(path);

		calendar = GregorianCalendar.getInstance();
		long def = (calendar.getTimeInMillis() - timeBegin);
		System.err.println("calacsim time ms:" + def);
		bw.close();
		System.err.println("calacsim time ms:" + def);
		// cvisObjToJson.reccs=reccs;
		// cvisObjToJson.projectSim=projectSim;
		// ESUtils.getInstance().addCVISTable(cvisObjToJson,ESUtils.BaseINDEX);

	}

	public void calcsimBYES(Integer[] params2, final List<JClass> json1Classes, final List<JClass> json2Classes,
			double elNameWeight, double parNameWeight, double simTh, final String path)
			throws IOException, InvalidFormatException {

		Calendar calendar = GregorianCalendar.getInstance();
		long timeBegin = calendar.getTimeInMillis();
		// System.err.println();

		int prg1Size = json1Classes.size();
		int prg2Size = json2Classes.size();
		final PrintStream bw = new PrintStream(
				new FileOutputStream(path + "\\output\\logFiles\\logfile" + prog1name + "#" + prog2name + ".txt"));
		if (prg1Size * prg2Size == 0) {
			bw.println("One or more projects are empty");
			bw.close();
			return;
		}
		bw.println("calcsimBYES");
		int totComp = prg1Size * prg2Size + 1;
		// Counter i = new Counter();
		Counter i = new Counter();
		this.elNameWeight = elNameWeight;
		this.parNameWeight = parNameWeight;
		this.simTh = simTh;
		Map<JClass, Pair<JClass, Double>> simEdge = new LinkedHashMap<>();

		// initialise name-to-class' map
		for (final JClass cl1 : json1Classes) {
			nameToCls.put(cl1.getClassName() + "##1", cl1);
			totMethods1 += cl1.getMethods().size();
		}
		for (final JClass cl2 : json2Classes) {
			nameToCls.put(cl2.getClassName() + "##2", cl2);
			totMethods2 += cl2.getMethods().size();
		}

		// initialise classes' compare results' map
		for (final JClass cl1 : json1Classes)
			for (final JClass cl2 : json2Classes)
				clsRes.put(new PointerPair<>(cl1, cl2), null);

		for (final JClass cl1 : json1Classes)
			for (final JClass cl2 : json2Classes) {
				ConnThread tr = new ConnThread(DBconn.getConn()) {
					public void run() {
						// System.err.println(cl1+" "+cl2);
						// System.err.println(cl1.getClassName()+" "+cl1.getClassName());
						// System.err.println();
						double sim = clsRes.get(new PointerPair<>(cl1, cl2)).getHuerVal();
						DBconn.closeConn(getConn());
						String cl1Name = cl1.getClassName();
						String cl2Name = cl2.getClassName();
						double progressPre = i.inc() / totComp;
						progress.set(progressPre);
						bw.println((int) 100 * progressPre + "% - calculated classes: " + cl1Name + " - " + cl2Name);
						Pair<JClass, Double> tUs1 = simEdge.get(cl1);
						if (tUs1 == null || tUs1.getValue() < sim)
							simEdge.put(cl1, new Pair<JClass, Double>(cl2, sim));
						Pair<JClass, Double> tUs2 = simEdge.get(cl2);
						if (tUs2 == null || tUs2.getValue() < sim)
							simEdge.put(cl2, new Pair<JClass, Double>(cl1, sim));
					}
				};
				tr.start();
				try {
					tr.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		// System.err.println(simEdge);
		Set<CommutativePointerPair<JClass, JClass>> listCls = new LinkedHashSet<>();
		simEdge.entrySet().forEach(
				a -> listCls.add(new CommutativePointerPair<JClass, JClass>(a.getKey(), a.getValue().getKey())));
		int arrLen = ClassSim.values().length;
		int[] realTotFromCat = new int[arrLen];
		int realTotMetRes = 0;
		for (CommutativePointerPair<JClass, JClass> params : listCls) {
			int[] totFromCat = new int[arrLen];
			int[] sameClsFromCat = new int[arrLen];
			JClass cl1 = params.getKey();
			JClass cl2 = params.getValue();
			boolean sameCls = cl1.getClassName().equals(cl2.getClassName());
			Set<JMethod> cls1methods = cl1.getMethods();
			Set<JMethod> cls2methods = cl2.getMethods();
			// totMethods2+=cls2methods.size();
			int totMetRes = 0;
			for (JMethod m1 : cls1methods) {
				for (JMethod m2 : cls2methods) {
					MethodSim methodSim = metRes.get(new CommutativePointerPair<>(m1, m2));
					if (methodSim != null) {
						totMetRes++;
						ClassSim res = methodSim.getType();
						totFromCat[res.ordinal()]++;
						realTotFromCat[res.ordinal()]++;
						if (sameCls)
							sameClsFromCat[res.ordinal()]++;
					}
				}
			}
			realTotMetRes += totMetRes;
			if (totMetRes > 0)
				for (int j = 0; j < arrLen; j++) {
					this.totFromCat[j] += totFromCat[j] * 100 / totMetRes;
					this.sameClsFromCat[j] += sameClsFromCat[j] * 100 / totMetRes;
					// System.err.println(this.totFromCat[j]+" "+this.sameClsFromCat[j]);
				}
			// System.err.println();
		}

		/*
		 * int maxMethods=Math.max(totMethods1, totMethods2); fixedCatSt(totFromCat,
		 * maxMethods); fixedCatSt(sameClsFromCat, maxMethods);
		 */
		this.reccs.sort((a, b) -> {
			int c1Vc1 = a.getClass1().compareTo(b.getClass1());
			if (c1Vc1 != 0)
				return c1Vc1;
			return a.getClass2().compareTo(b.getClass2());
		});
		ProjectSim projectSim = new ProjectSim(totFromCat[ClassSim.USE.ordinal()],
				totFromCat[ClassSim.REFINEMENT.ordinal()], totFromCat[ClassSim.EXTENSION.ordinal()],
				totFromCat[ClassSim.REFINED_EXTENSION.ordinal()], totFromCat[ClassSim.OVERLOADING.ordinal()],
				100 * listCls.size());

		// TODO samih

		String sjson = "";

		ESUtils es = ESUtils.getInstance();
		long s = System.currentTimeMillis();
		sjson = GSON.toJson(new ParamsObj(params2));
		es.printLog("clacsimSE", "Params");
		es.addJsonToType(ESUtils.PARAMS_Index, ESUtils.PARAMS_TYPE, GSON.toJson(new ParamsObj(params2)),
				prog1name + "#" + prog2name);

		sjson = GSON.toJson(projectSim);
		es.printLog("clacsimSE", "project");
		es.addJsonToType(ESUtils.PROJECT_SIM_Index, ESUtils.PROJECT_SIM_TYPE, new Gson().toJson(projectSim),
				prog1name + "#" + prog2name);

		MiniJProg jProg1 = new MiniJProg(prog1name, runApplication.toMini(json1Classes));
		MiniJProg jProg2 = new MiniJProg(prog2name, runApplication.toMini(json2Classes));
		es.printLog("clacsimSE", "classes");
		es.addJsonToType(ESUtils.MINIJPROG_Index, ESUtils.MINIJPROG_TYPE, new Gson().toJson(jProg1), prog1name);
		es.addJsonToType(ESUtils.MINIJPROG_Index, ESUtils.MINIJPROG_TYPE, new Gson().toJson(jProg2), prog2name);
		es.printLog("clacsimSE", "classrcmnds");
		addClassesRcmnds(es);
		es.printLog("clacsimSE", "methodremnds");
		addMethodsMchnzm(es);
		double progressPre = i.inc() / totComp;
		progress.set(progressPre);
		calendar = GregorianCalendar.getInstance();
		long def = (calendar.getTimeInMillis() - timeBegin);
		System.err.println("calacsim by ES time ms:" + def);
		bw.close();
		// System.err.println("calacsim by ES time ms:" +def);

	}

	private void addMethodsMchnzm(ESUtils es) {
		int count = 0;
		int batchsize = 1000;
		for (MethodSim metComp : metRes.values()) {
			if (metComp.getType() != ClassSim.NULL) {
				MethodsCat mt = new MethodsCat();
				mt.setProg1(prog1name);
				mt.setProg2(prog2name);
				mt.setCl1(metComp.getClass1());
				mt.setCl2(metComp.getClass2());
				mt.setM1(metComp.getMethod1());
				mt.setM2(metComp.getMethod2());
				mt.setType(metComp.getType());
				// System.out.println("addMethodsMchnzm:"+mt);
				// es.addJsonToType(ESUtils.BaseINDEX+ESUtils.METHODS_CAT_TYPE,
				// ESUtils.METHODS_CAT_TYPE, new Gson().toJson(mt),null);
				count++;
				if (count % batchsize != 0) {
					es.addJsonToBulk(ESUtils.METHODS_CAT_Index, ESUtils.METHODS_CAT_TYPE,
							new Gson().toJson(mt), null, "add");
				} else {
					es.addJsonToBulk(null, null, null, null, "get");

				}
			}
		}
		if (count % batchsize != 0) {
			es.addJsonToBulk(null, null, null, null, "get");
			es.printLog("addMethodsMchnzm:get count", count);

		}

	}

	private void addClassesRcmnds(ESUtils es) {
		// TODO Auto-generated method stub
		int count = 0;
		int batchsize = 1000;
		for (SDComp tempInf : reccs) {
			ClassesRcmnd mt = new ClassesRcmnd();
			mt.setSdCompBase(tempInf.getSDCompBaseCopy());
			mt.setProg1(prog1name);
			mt.setProg2(prog2name);
			mt.setClass1(tempInf.getClass1());
			mt.setClass2(tempInf.getClass2());
			int subTyping = tempInf.getRef() + tempInf.getExt() + tempInf.getRefExt();
			int total = tempInf.getTot();

			mt.setParametric(tempInf.getUse());
			mt.setSubTyping(subTyping);
			mt.setOverloading(tempInf.getOverloading());
			mt.setElementsAmount(total);
			mt.setParametricRecmnd(tempInf.getUsePart());
			mt.setSubTypingRcmnd(((double) subTyping) / total);
			mt.setOverloadingRcmnd(tempInf.getOverloadingPart());
			count++;
			if (count % batchsize != 0) {
				es.addJsonToBulk(ESUtils.CLASSES_RCMNDS_Index, ESUtils.CLASSES_RCMNDS_TYPE,
						GSON.toJson(mt), null, "add");
			} else {
				es.addJsonToBulk(null, null, null, null, "get");
				es.printLog("addClassesRcmnds:get count", count);
			}
		}
		if (count % batchsize != 0)
			es.addJsonToBulk(null, null, null, null, "get");

	}

	private void fixedCatSt(int[] fromCat, int maxMethods) {
		int totCntr = 0;
		int i = 0;
		for (; i < fromCat.length; i++) {
			int currInCat = fromCat[i];
			totCntr += currInCat;
			if (totCntr >= maxMethods) {
				fromCat[i] = maxMethods - (totCntr - currInCat);
				i++;
				break;
			}
		}
		for (; i < fromCat.length; i++)
			fromCat[i] = 0;
	}

	/**
	 * Class comparing.
	 * 
	 * @param cl1
	 * @param cl2
	 * @throws IOException
	 */
	public SDComp classcompar(JClass cl1, JClass cl2, Connection connection) {
		List<MethodSim> simL = new LinkedList<>();
		Set<JMethod> cls1methods = cl1.getMethods();
		Set<JMethod> cls2methods = cl2.getMethods();
		// System.err.println(ops1.size()+" "+ops2.size());
		Map<JMethod, Pair<JMethod, ClassSim>> simEdge = new LinkedHashMap<>();
		int[] allRes = new int[ClassSim.values().length];
		for (JMethod m1 : cls1methods) {
			if (m1.getVisab().equals(VisabilityEnum.PRIVATE))
				continue;
			for (JMethod m2 : cls2methods) {
				if (m2.getVisab().equals(VisabilityEnum.PRIVATE))
					continue;
				StringBuilder shallowSimPairsStr;
				if (debugCSV != null)
					shallowSimPairsStr = new StringBuilder();
				else
					shallowSimPairsStr = null;
				ClassSim res = calShallow(m1, m2, cl1, cl2, shallowSimPairsStr, connection);
				StringBuilder deepSimPairsStr;
				if (debugCSV != null)
					deepSimPairsStr = new StringBuilder();
				else
					deepSimPairsStr = null;
				if (res == ClassSim.USE) {
					res = calDeep(m1, m2, cl1, cl2, deepSimPairsStr, connection);
					if (res == ClassSim.NULL)
						res = ClassSim.OVERLOADING;
				} else
					res = ClassSim.NULL;
				Pair<JMethod, ClassSim> tM1 = simEdge.get(m1);
				if (tM1 == null || tM1.getValue().ordinal() > res.ordinal())
					simEdge.put(m1, new Pair<JMethod, ClassSim>(m2, res));
				Pair<JMethod, ClassSim> tM2 = simEdge.get(m2);
				if (tM2 == null || tM2.getValue().ordinal() > res.ordinal())
					simEdge.put(m2, new Pair<JMethod, ClassSim>(m1, res));
				if (debugCSV != null) {
					// basic info
					String infStr = m1.getName() + "," + cl1.getClassName() + "," + prog1name + "," + m2.getName() + ","
							+ cl2.getClassName() + "," + prog2name + ",";

					// parameters
					for (Parameter param : m1.getParams())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";
					for (Parameter param : m2.getParams())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";

					// return type
					infStr += m1.getReturnType() + ",";
					infStr += m2.getReturnType() + ",";

					// used
					for (Parameter param : m1.getUsed().keySet())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";
					for (Parameter param : m2.getUsed().keySet())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";

					// modified
					for (Parameter param : m1.getModified().keySet())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";
					for (Parameter param : m2.getModified().keySet())
						infStr += "(" + param.name + ":" + param.getType() + ");";
					infStr += ",";

					// add deep and shallow results
					infStr += shallowSimPairsStr + ",";
					infStr += deepSimPairsStr + ",";

					// result (catagory)
					infStr += res.name();

					debugCSV.println(infStr);
				}
			}
		}
		Map<CommutativePointerPair<JMethod, JMethod>, ClassSim> methodConn = new LinkedHashMap<>();
		simEdge.forEach(
				(a, b) -> methodConn.put(new CommutativePointerPair<JMethod, JMethod>(a, b.getKey()), b.getValue()));
		methodConn.forEach((a, b) -> {
			MethodSim mSim = null;
			if (cls1methods.contains(a.getKey()))
				mSim = new MethodSim(cl1.getClassName(), cl2.getClassName(), a.getKey().getName(),
						a.getValue().getName(), b);
			else
				mSim = new MethodSim(cl1.getClassName(), cl2.getClassName(), a.getValue().getName(),
						a.getKey().getName(), b);
			metRes.put(a, mSim);
			simL.add(mSim);
			allRes[b.ordinal()]++;
		});
		int methodAmount = methodConn.size();
		SDComp aRes = new SDComp(cl1.getClassName(), cl2.getClassName(), allRes[0], allRes[1], allRes[2], allRes[3],
				allRes[4], allRes[5], methodAmount);
		aRes.addAllMethodSim(simL);
		reccs.add(aRes);
		return aRes;
	}

	@SuppressWarnings("unchecked")
	private ClassSim calDeep(JMethod m1, JMethod m2, JClass cl1, JClass cl2, StringBuilder deepSimPairsStr,
			Connection conn) {
		// System.err.println(m1+"\n"+m2);
		// System.err.println("deep");
		boolean isDebug = deepSimPairsStr != null;
		boolean covered = true;
		boolean multi = false;
		List<Parameter> used1 = new LinkedList<>();
		List<Parameter> used2 = new LinkedList<>();
		List<Parameter> mod1 = new LinkedList<>();
		List<Parameter> mod2 = new LinkedList<>();
		Map<ExtParam, Pair<ExtParam, Double>[]> simEdge = new LinkedHashMap<>();
		m1.getUsed().forEach((p, i) -> {
			for (int j = 0; j < i; j++)
				used1.add(new Parameter(p.name, p.type));
			ExtParam extP = new ExtParam(p, m1, true);
			Pair<ExtParam, Double>[] pArr = new Pair[i];
			simEdge.put(extP, pArr);
		});
		m2.getUsed().forEach((p, i) -> {
			for (int j = 0; j < i; j++)
				used2.add(new Parameter(p.name, p.type));
			ExtParam extP = new ExtParam(p, m2, true);
			Pair<ExtParam, Double>[] pArr = new Pair[i];
			simEdge.put(extP, pArr);
		});
		m1.getModified().forEach((p, i) -> {
			for (int j = 0; j < i; j++)
				mod1.add(new Parameter(p.name, p.type));
			ExtParam extP = new ExtParam(p, m1, false);
			Pair<ExtParam, Double>[] pArr = new Pair[i];
			simEdge.put(extP, pArr);
		});
		m2.getModified().forEach((p, i) -> {
			for (int j = 0; j < i; j++)
				mod2.add(new Parameter(p.name, p.type));
			ExtParam extP = new ExtParam(p, m2, false);
			Pair<ExtParam, Double>[] pArr = new Pair[i];
			simEdge.put(extP, pArr);
		});
		for (Parameter us1 : used1) {
			for (Parameter us2 : used2) {
				double sim = elNameWeight * nameSim(us1.getName(), us2.getName(), conn)
						+ (1 - elNameWeight) * typeSim(us1.getType(), us2.getType(), cl1, cl2);
				if (sim > simTh) {
					ExtParam extUs1 = new ExtParam(us1, m1, true);
					ExtParam extUs2 = new ExtParam(us2, m2, true);
					Pair<ExtParam, Double>[] simArr = simEdge.get(extUs1);
					Pair<ExtParam, Double> tUs1 = simArr[0];
					if (tUs1 == null) {
						if (simArr[simArr.length - 1] == null)
							simArr[simArr.length - 1] = new Pair<ExtParam, Double>(extUs2, sim);
						else
							for (int i = 0; i < simArr.length - 1; i++) {
								if (simArr[i + 1] != null) {
									simArr[i] = new Pair<ExtParam, Double>(extUs2, sim);
								}
							}
					} else {
						if (tUs1.getValue() < sim) {
							simArr[0] = new Pair<ExtParam, Double>(extUs2, sim);
							bubbleStep(simArr);
						}
					}
					simArr = simEdge.get(extUs2);
					Pair<ExtParam, Double> tUs2 = simArr[0];
					if (tUs2 == null) {
						if (simArr[simArr.length - 1] == null)
							simArr[simArr.length - 1] = new Pair<ExtParam, Double>(extUs1, sim);
						else
							for (int i = 0; i < simArr.length - 1; i++) {
								if (simArr[i + 1] != null) {
									simArr[i] = new Pair<ExtParam, Double>(extUs1, sim);
								}
							}
					} else {
						if (tUs2.getValue() < sim) {
							simArr[0] = new Pair<ExtParam, Double>(extUs1, sim);
							bubbleStep(simArr);
						}
					}
				}
			}
		}
		for (Parameter us1 : mod1) {
			for (Parameter us2 : mod2) {
				double sim = elNameWeight * nameSim(us1.getName(), us2.getName(), conn)
						+ (1 - elNameWeight) * typeSim(us1.getType(), us2.getType(), cl1, cl2);
				if (sim > simTh) {
					ExtParam extUs1 = new ExtParam(us1, m1, false);
					ExtParam extUs2 = new ExtParam(us2, m2, false);
					Pair<ExtParam, Double>[] simArr = simEdge.get(extUs1);
					Pair<ExtParam, Double> tUs1 = simArr[0];
					if (tUs1 == null) {
						if (simArr[simArr.length - 1] == null)
							simArr[simArr.length - 1] = new Pair<ExtParam, Double>(extUs2, sim);
						else
							for (int i = 0; i < simArr.length - 1; i++) {
								if (simArr[i + 1] != null) {
									simArr[i] = new Pair<ExtParam, Double>(extUs2, sim);
								}
							}
					} else {
						if (tUs1.getValue() < sim) {
							simArr[0] = new Pair<ExtParam, Double>(extUs2, sim);
							bubbleStep(simArr);
						}
					}
					simArr = simEdge.get(extUs2);
					Pair<ExtParam, Double> tUs2 = simArr[0];
					if (tUs2 == null) {
						if (simArr[simArr.length - 1] == null)
							simArr[simArr.length - 1] = new Pair<ExtParam, Double>(extUs1, sim);
						else
							for (int i = 0; i < simArr.length - 1; i++) {
								if (simArr[i + 1] != null) {
									simArr[i] = new Pair<ExtParam, Double>(extUs1, sim);
								}
							}
					} else {
						if (tUs2.getValue() < sim) {
							simArr[0] = new Pair<ExtParam, Double>(extUs1, sim);
							bubbleStep(simArr);
						}
					}
				}
			}
		}
		int paramNegCount = 0;
		Map<ExtParam, Integer> paramP = new LinkedHashMap<>();
		List<ExtParam> listP = new LinkedList<>();
		for (Entry<ExtParam, Pair<ExtParam, Double>[]> e : simEdge.entrySet()) {
			ExtParam a = e.getKey();
			Pair<ExtParam, Double>[] b = e.getValue();
			if (isDebug)
				deepSimPairsStr.append("(" + a + ":" + Arrays.toString(b).replace(",", "|") + ");");
			paramP.put(a, -b.length);
			paramNegCount -= b.length;
			for (Pair<ExtParam, Double> p : b)
				if (p != null)
					listP.add(p.getKey());
		}
		for (ExtParam param : listP) {
			// if(m1.getName().equals("draw")&&m2.getName().equals("draw"))
			// System.err.println(para1+" "+para2);
			paramP.put(param, paramP.get(param) + 1);
		}
		/*
		 * if(m1.getName().equals("getJRadioButton")&&m2.getName().equals(
		 * "getJRadioButton")){ System.err.print("{");
		 * simEdge.forEach((a,b)->System.err.print("("+a+","+b.length+"),"));
		 * System.err.println("}"); System.err.println(listP);
		 * System.err.println(paramP); System.err.println(used1);
		 * System.err.println(used2); System.err.println(mod1);
		 * System.err.println(mod2); System.err.println(); }
		 */
		if (paramP.size() == 0)
			return ClassSim.USE;
		int simCounter = 0;
		for (Integer paramV : paramP.values())
			simCounter += paramV;
		if (((double) simCounter - paramNegCount) / paramP.size() < edgesThres)
			return ClassSim.NULL;
		for (Integer paramV : paramP.values()) {
			if (paramV > 0)
				multi = true;
			if (paramV < 0)
				covered = false;
			if (!covered && multi)
				break;
		}
		if (covered && !multi)
			return ClassSim.USE;
		if (covered && multi)
			return ClassSim.REFINEMENT;
		if (!covered && multi)
			return ClassSim.REFINED_EXTENSION;
		if (!covered && !multi)
			return ClassSim.EXTENSION;
		return ClassSim.NULL;
	}

	private void bubbleStep(Pair<ExtParam, Double>[] simArr) {
		for (int i = 0; i < simArr.length - 1; i++) {
			if (simArr[i + 1] == null)
				break;
			if (simArr[i].getValue() > simArr[i + 1].getValue()) {
				Pair<ExtParam, Double> t = simArr[i];
				simArr[i] = simArr[i + 1];
				simArr[i + 1] = t;
			}
		}
	}

	private ClassSim calShallow(JMethod m1, JMethod m2, JClass cl1, JClass cl2, StringBuilder shallowSimPairsStr,
			Connection conn) {
		boolean isDebug = shallowSimPairsStr != null;
		boolean covered = true;
		boolean multi = false;
		Collection<Parameter> params1 = m1.getParams();
		Collection<Parameter> params2 = m2.getParams();
		Map<Parameter, Pair<Parameter, Double>> simEdge = new PointerHashMap<>();
		for (Parameter param1 : params1) {
			for (Parameter param2 : params2) {
				double sim = parNameWeight * nameSim(param1.getName(), param2.getName(), conn)
						+ (1 - parNameWeight) * typeSim(param1.getType(), param2.getType(), cl1, cl2);
				if (sim > simTh) {
					Pair<Parameter, Double> tParam1 = simEdge.get(param1);
					if (tParam1 == null || tParam1.getValue() < sim)
						simEdge.put(param1, new Pair<Parameter, Double>(param2, sim));
					Pair<Parameter, Double> tParam2 = simEdge.get(param2);
					if (tParam2 == null || tParam2.getValue() < sim)
						simEdge.put(param2, new Pair<Parameter, Double>(param1, sim));
				}
			}
		}
		String m1Name = m1.getName();
		String m2Name = m2.getName();
		String m1Type = m1.getReturnType();
		String m2Type = m2.getReturnType();
		Parameter m1Sig = new Parameter(m1Name, m1Type);
		Parameter m2Sig = new Parameter(m2Name, m2Type);
		double sigSim = parNameWeight * nameSim(m1Name, m2Name, conn)
				+ (1 - parNameWeight) * typeSim(m1Type, m2Type, cl1, cl2);
		if (sigSim > simTh)
			simEdge.put(m1Sig, new Pair<Parameter, Double>(m2Sig, sigSim));
		Set<CommutativePointerPair<Parameter, Parameter>> listP = new LinkedHashSet<>();
		if (isDebug)
			shallowSimPairsStr.append(simEdge.toString().replace("),", ");").replace(",", ":"));
		simEdge.entrySet().forEach(
				a -> listP.add(new CommutativePointerPair<Parameter, Parameter>(a.getKey(), a.getValue().getKey())));
		Map<Parameter, Integer> paramP = new PointerHashMap<>();
		params1.forEach(p -> paramP.put(p, 0));
		params2.forEach(p -> paramP.put(p, 0));
		paramP.put(m1Sig, 0);
		paramP.put(m2Sig, 0);
		/*
		 * if(m1.getName().equals("SelectWindowAction")&&m2.getName().equals(
		 * "SelectWindowAction")){ System.err.println(paramP);
		 * System.err.println(params1); System.err.println(params2); }
		 */
		for (Pair<Parameter, Parameter> params : listP) {
			Parameter para1 = params.getKey();
			Parameter para2 = params.getValue();
			// System.err.println(para1+" "+para2);
			paramP.put(para1, paramP.get(para1) + 1);
			paramP.put(para2, paramP.get(para2) + 1);
		}
		if (paramP.size() == 0)
			return ClassSim.USE;
		int simCounter = 0;
		for (Integer paramV : paramP.values())
			simCounter += paramV;
		// System.err.println(simCounter);
		if (((double) simCounter) / paramP.size() < edgesThres)
			return ClassSim.NULL;
		for (Integer paramV : paramP.values()) {
			if (paramV > 1)
				multi = true;
			if (paramV == 0)
				covered = false;
			if (!covered && multi)
				break;
		}
		if (covered && !multi)
			return ClassSim.USE;
		if (covered && multi)
			return ClassSim.REFINEMENT;
		if (!covered && multi)
			return ClassSim.REFINED_EXTENSION;
		if (!covered && !multi)
			return ClassSim.EXTENSION;
		return ClassSim.NULL;
	}

	// ------------------------------------------------ Printing Methods
	// -------------------------------------------------
	private void sumProject(String path) {
		try {
			// System.err.println(path);
			File baseProject = new File(path);
			// System.err.println(baseProject);
			File t = baseProject.getParentFile();
			if (t != null && t.getAbsolutePath().length() > 0) {
				t = t.getParentFile();
				/*
				 * if(t!=null&&t.getAbsolutePath().length()>0){ baseProject=t; }
				 */
			}

			// System.err.println(baseProject);
			String projectName = baseProject.getName();
			File baseVarMech = baseProject;
			t = baseVarMech.getParentFile();
			if (t != null && t.getAbsolutePath().length() > 0) {
				baseVarMech = baseVarMech.getParentFile();
				t = baseVarMech.getParentFile();
				if (t != null && t.getAbsolutePath().length() > 0) {
					baseVarMech = baseVarMech.getParentFile();
				}
			}
			String xlName = baseVarMech + "\\projects_summary" + paramSuffix + ".xlsx";
			// System.err.println(xlName);
			try {
				xlSem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			File xlFile = new File(xlName);
			if (!xlFile.exists())
				createFirstSumPage(xlName);
			FileInputStream file = new FileInputStream(xlFile);
			XSSFWorkbook wb = new XSSFWorkbook(file);
			CellStyle style = wb.createCellStyle();
			style.setDataFormat(wb.createDataFormat().getFormat("0%"));
			XSSFSheet sheet = wb.getSheet(projectName);
			if (sheet == null) {
				sheet = wb.createSheet(projectName);
				XSSFRow row = sheet.createRow(0);
				row.createCell(0).setCellValue("Component");
				row.createCell(1).setCellValue("Version 1");
				row.createCell(2).setCellValue("Version Number");
				row.createCell(3).setCellValue("# of methods");
				row.createCell(4).setCellValue("Version 2");
				row.createCell(5).setCellValue("Version Number");
				row.createCell(6).setCellValue("# of methods");
				row.createCell(7).setCellValue("Distance");
				row.createCell(8).setCellValue("% in Parametric");
				row.createCell(9).setCellValue("% in Sub Typing");
				row.createCell(10).setCellValue("% in Overloading");
				row.createCell(11).setCellValue("Same class % in Parametric");
				row.createCell(12).setCellValue("Same class % in Sub Typing");
				row.createCell(13).setCellValue("Same class % in Overloading");
			}
			int numOfRows = sheet.getLastRowNum();
			// Update the value of cell
			XSSFRow row = sheet.createRow(numOfRows + 1);
			boolean isWithin = areKEqual(stripProgName(prog1name), stripProgName(prog2name), 3);
			// System.err.println(stripProgName(prog1name)+" "+stripProgName(prog2name));
			double v1 = findNum(prog1name);
			double v2 = findNum(prog2name);
			double programDis = Math.abs(v2 - v1);
			Polymorphizm polyType = null;
			if (isWithin) {
				if (programDis <= 2) {
					row.createCell(0).setCellValue("Within-close");
					polyType = Polymorphizm.CLOSE;
				} else {
					row.createCell(0).setCellValue("Within-far");
					polyType = Polymorphizm.FAR;
				}
			} else {
				row.createCell(0).setCellValue("Between");
				polyType = Polymorphizm.BETWEEN;
			}
			row.createCell(1).setCellValue(prog1name);
			row.createCell(2).setCellValue(v1);
			row.createCell(3).setCellValue(totMethods1);
			row.createCell(4).setCellValue(prog2name);
			row.createCell(5).setCellValue(v2);
			row.createCell(6).setCellValue(totMethods2);
			row.createCell(7).setCellValue(programDis);
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(getProjectsUse());
			cell8.setCellStyle(style);
			XSSFCell cell9 = row.createCell(9);
			cell9.setCellValue(getProjectsSubTyping());
			cell9.setCellStyle(style);
			XSSFCell cell10 = row.createCell(10);
			cell10.setCellValue(getProjectsOverloading());
			cell10.setCellStyle(style);
			XSSFCell cell11 = row.createCell(11);
			cell11.setCellValue(getSameClassProjectsUse());
			cell11.setCellStyle(style);
			XSSFCell cell12 = row.createCell(12);
			cell12.setCellValue(getSameClassProjectsSubTyping());
			cell12.setCellStyle(style);
			XSSFCell cell13 = row.createCell(13);
			cell13.setCellValue(getSameClassProjectsOverloading());
			cell13.setCellStyle(style);
			for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++)
				sheet.autoSizeColumn(i);
			file.close();
			FileOutputStream outFile = new FileOutputStream(xlFile);
			wb.write(outFile);
			outFile.close();
			wb.close();
			updateFirstSumPage(xlName, polyType);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			xlSem.release();
		}
	}

	private boolean areKEqual(String s1, String s2, int k) {
		int cntr = 0;
		int s1Len = s1.length();
		int s2Len = s2.length();
		cntr += Math.abs(s1Len - s2Len);
		if (cntr > k)
			return false;
		cntr *= s1Len * s2Len;
		for (int i = 0; i < s1Len; i++) {
			for (int j = 0; j < s2Len; j++) {
				char s1Char = s1.charAt(i);
				char s2Char = s2.charAt(j);
				if (s1Char != s2Char)
					cntr++;
			}
		}
		return cntr / s1Len / s2Len < k;
	}

	private String stripProgName(String progName) {
		return progName.toLowerCase().replaceAll("[^a-z]", "");
	}

	/*
	 * private double getDis(String prog1name, String prog2name) { double
	 * num1=findNum(prog1name, false); double num2=findNum(prog2name, false);
	 * System.err.println(num1+" "+num2); return Math.abs(num1-num2); }
	 */
	/*
	 * private double findNum(String s, boolean hasNum) {
	 * //System.err.println("s: "+s+" "+hasNum); int stringLen=s.length(); double
	 * num=0; int i=0; boolean foundNum=false; int pointCounter=0;
	 * for(;i<stringLen;i++){ char sChar=s.charAt(i); if(sChar=='.'){ num*=10;
	 * pointCounter++; } if(sChar>='0'&&sChar<='9'){ foundNum=true; num*=10;
	 * num+=(sChar-'0'); } else if(foundNum){ if(i+1<stringLen)
	 * num=num*10+findNum(s.substring(i+1), true); pointCounter++; break; } else {
	 * if(hasNum) break; } } //System.err.println(i); double num1=num; num=0;
	 * foundNum=false; int pointCounter1=pointCounter; pointCounter=0;
	 * for(;i<s.length();i++){ char sChar=s.charAt(i); //System.err.println(sChar);
	 * if(sChar=='.'){ num*=10; pointCounter++; } if(sChar>='0'&&sChar<='9'){
	 * foundNum=true; num*=10; num+=(sChar-'0'); //System.err.println(num); } else
	 * if(foundNum){ if(i+1<stringLen) num=num*10+findNum(s.substring(i+1), true);
	 * pointCounter++; break; } else { if(hasNum) break; } }
	 * if(foundNum&&i==stringLen) pointCounter++;
	 * //System.err.println(s+" "+num+" "+num1+" "+pointCounter1+" "+pointCounter);
	 * if(pointCounter1>=pointCounter) return num1/Math.pow(10, pointCounter1);
	 * return num/Math.pow(10, pointCounter); }
	 */
	private double findNum(String str) {
		int stringLen = str.length();
		int s = 0;
		int e = stringLen;
		boolean foundNum = false;
		boolean breakedNum = false;
		for (int i = 0; i < stringLen; i++) {
			char sChar = str.charAt(i);
			// System.err.println(sChar);
			if (sChar == '.' || (sChar >= '0' && sChar <= '9')) {
				if (!foundNum)
					s = i;
				foundNum = true;
				breakedNum = false;
			} else {
				if (foundNum) {
					if (breakedNum) {
						e = i;
						break;
					}
					breakedNum = true;
				}
			}
		}
		// System.err.println(s+" "+e);
		str = str.substring(s, e);
		stringLen = str.length();
		String numStr = "";
		boolean wasDot = false;
		char firstChar = str.charAt(0);
		if (firstChar == '0' || firstChar == '.') {
			numStr += "0.";
			wasDot = true;
		} else
			numStr += firstChar;
		for (int i = 1; i < stringLen; i++) {
			char sChar = str.charAt(i);
			if (sChar >= '0' && sChar <= '9')
				numStr += sChar;
			if (sChar == '.' && !wasDot) {
				numStr += sChar;
				wasDot = true;
			}
		}
		double num;
		try {
			num = Double.parseDouble(numStr);
		} catch (Exception ex) {
			// ex.printStackTrace();
			num = 0;
		}
		return num;
	}

	private void createFirstSumPage(String xlName) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Sum Sheet");
		XSSFRow row = sheet.createRow(0);
		row.createCell(0).setCellValue("Component");
		row.createCell(1).setCellValue("Total in Component");
		row.createCell(2).setCellValue("% in Parametric");
		row.createCell(3).setCellValue("% in Sub Typing");
		row.createCell(4).setCellValue("% in Overloading");
		row.createCell(5).setCellValue("Same class % in Parametric");
		row.createCell(6).setCellValue("Same class % in Sub Typing");
		row.createCell(7).setCellValue("Same class % in Overloading");
		row = sheet.createRow(1);
		row.createCell(0).setCellValue("Within-close");
		for (int i = 1; i < 8; i++)
			row.createCell(i).setCellValue(0);
		row = sheet.createRow(2);
		row.createCell(0).setCellValue("Within-far");
		for (int i = 1; i < 8; i++)
			row.createCell(i).setCellValue(0);
		row = sheet.createRow(3);
		row.createCell(0).setCellValue("Between");
		for (int i = 1; i < 8; i++)
			row.createCell(i).setCellValue(0);

		FileOutputStream out = new FileOutputStream(xlName);
		wb.write(out);
		wb.close();
		out.close();
	}

	private void updateFirstSumPage(String xlName, Polymorphizm polyType) throws IOException {
		FileInputStream file = new FileInputStream(xlName);
		XSSFWorkbook wb = new XSSFWorkbook(file);
		CellStyle style = wb.createCellStyle();
		style.setDataFormat(wb.createDataFormat().getFormat("0%"));
		XSSFSheet sheet = wb.getSheet("Sum Sheet");
		XSSFRow row;
		row = sheet.getRow(polyType.ordinal() + 1);
		double totalIn = row.getCell(1).getNumericCellValue();
		double paramPre = row.getCell(2).getNumericCellValue();
		double subPre = row.getCell(3).getNumericCellValue();
		double overPre = row.getCell(4).getNumericCellValue();
		double sameClsParamPre = row.getCell(5).getNumericCellValue();
		double sameClsSubPre = row.getCell(6).getNumericCellValue();
		double sameClsOverPre = row.getCell(7).getNumericCellValue();
		row.getCell(1).setCellValue(totalIn + 1);
		row.getCell(2).setCellValue((paramPre * totalIn + getProjectsUse()) / (totalIn + 1));
		row.getCell(3).setCellValue((subPre * totalIn + getProjectsSubTyping()) / (totalIn + 1));
		row.getCell(4).setCellValue((overPre * totalIn + getProjectsOverloading()) / (totalIn + 1));
		row.getCell(5).setCellValue((sameClsParamPre * totalIn + getSameClassProjectsUse()) / (totalIn + 1));
		row.getCell(6).setCellValue((sameClsSubPre * totalIn + getSameClassProjectsSubTyping()) / (totalIn + 1));
		row.getCell(7).setCellValue((sameClsOverPre * totalIn + getSameClassProjectsOverloading()) / (totalIn + 1));
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		for (int i = 2; i < 8; i++) {
			sheet.autoSizeColumn(i);
			row.getCell(i).setCellStyle(style);
		}

		file.close();
		FileOutputStream outFile = new FileOutputStream(xlName);
		wb.write(outFile);
		outFile.close();
		wb.close();
	}

	private double getProjectsOverloading() {
		// int maxMethodsSize=Math.max(totMethods1,totMethods2);
		return ((double) totFromCat[ClassSim.OVERLOADING.ordinal()]) / 100;
	}

	private double getProjectsSubTyping() {
		return ((double) (totFromCat[ClassSim.REFINEMENT.ordinal()] + totFromCat[ClassSim.EXTENSION.ordinal()]
				+ totFromCat[ClassSim.REFINED_EXTENSION.ordinal()])) / 100;
	}

	private double getProjectsUse() {
		return ((double) totFromCat[ClassSim.USE.ordinal()]) / 100;
	}

	private double getSameClassProjectsOverloading() {
		return ((double) sameClsFromCat[ClassSim.OVERLOADING.ordinal()]) / 100;
	}

	private double getSameClassProjectsSubTyping() {
		return ((double) (sameClsFromCat[ClassSim.REFINEMENT.ordinal()] + sameClsFromCat[ClassSim.EXTENSION.ordinal()]
				+ sameClsFromCat[ClassSim.REFINED_EXTENSION.ordinal()])) / 100;
	}

	private double getSameClassProjectsUse() {
		return ((double) sameClsFromCat[ClassSim.USE.ordinal()]) / 100;
	}

	/**
	 * Excel outputting.
	 * 
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	private void toExcel(String path) throws InvalidFormatException {
		try {
			File compResFile = new File(
					path + "\\output\\exelFiles\\CompRes_" + prog1name + "#" + prog2name + paramSuffix + ".xlsx");
			runApplication.initXlFile(compResFile);
			FileInputStream file = new FileInputStream(compResFile);
			XSSFWorkbook wb = new XSSFWorkbook(file);
			CreationHelper createHelper = wb.getCreationHelper();

			// Cosmetics
			XSSFFont defaultFont = wb.createFont();
			defaultFont.setFontHeightInPoints((short) 10);
			defaultFont.setFontName("Arial");
			defaultFont.setBold(true);

			XSSFFont font = wb.createFont();
			font.setFontHeightInPoints((short) 10);
			font.setFontName("Arial");
			font.setBold(true);
			font.setItalic(false);

			CellStyle style = wb.createCellStyle();
			style.setAlignment(CellStyle.ALIGN_CENTER);
			style.setFont(font);
			// wb = sheetMaker(wb, this.attStringList, this.attLeaves, style, createHelper,
			// "Attributes");
			sheetMethods(wb, style, createHelper, "Methods");
			sheetRecommendationMaker(wb, style, createHelper, "Mechanism Recommendation");

			// Write the output to a file.
			file.close();
			FileOutputStream fileOut = new FileOutputStream(compResFile);
			wb.write(fileOut);
			fileOut.close();
			wb.close();

			System.out.println("Endded Outputing\n");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private XSSFWorkbook sheetMethods(XSSFWorkbook wb, CellStyle style, CreationHelper createHelper, String sheetName) {
		XSSFSheet sheet = wb.createSheet(sheetName);
		CellStyle style_prct = wb.createCellStyle();
		style_prct.setDataFormat(wb.createDataFormat().getFormat("0%"));

		// Creating the header for methods
		XSSFRow row0 = sheet.createRow((short) 0);
		row0.createCell(0).setCellValue(createHelper.createRichTextString("Class 1 Name:"));
		row0.createCell(1).setCellValue(createHelper.createRichTextString("Method 1 Name:"));
		row0.createCell(2).setCellValue(createHelper.createRichTextString("Class 2 Name:"));
		row0.createCell(3).setCellValue(createHelper.createRichTextString("Method 2 Name:"));
		row0.createCell(4).setCellValue(createHelper.createRichTextString("Cat.:"));
		int rowCount = 1;
		// Putting data inside the table
		for (MethodSim metComp : metRes.values()) {
			XSSFCell cell = null;
			XSSFRow row = sheet.createRow(rowCount++);
			// Class 1 Name
			cell = row.createCell(0);
			cell.setCellValue(createHelper.createRichTextString(metComp.getClass1()));
			cell.setCellStyle(style);

			// Class 2 Name
			cell = row.createCell(1);
			cell.setCellValue(createHelper.createRichTextString(metComp.getMethod1()));
			cell.setCellStyle(style);

			cell = row.createCell(2);
			cell.setCellValue(createHelper.createRichTextString(metComp.getClass2()));
			cell.setCellStyle(style);

			cell = row.createCell(3);
			cell.setCellValue(createHelper.createRichTextString(metComp.getMethod2()));
			cell.setCellStyle(style);

			cell = row.createCell(4);
			cell.setCellValue(createHelper.createRichTextString(metComp.getType().name()));
			cell.setCellStyle(style);
		}
		for (int i = 0; i < row0.getLastCellNum(); i++)
			sheet.autoSizeColumn(i);
		return wb;
	}

	private XSSFWorkbook sheetRecommendationMaker(XSSFWorkbook wb, CellStyle style, CreationHelper createHelper,
			String sheetName) {
		XSSFSheet sheet = wb.createSheet(sheetName);
		CellStyle style_prct = wb.createCellStyle();
		style_prct.setDataFormat(wb.createDataFormat().getFormat("0%"));

		// Creating the header for methods
		XSSFRow row0 = sheet.createRow((short) 0);
		row0.createCell(0).setCellValue(createHelper.createRichTextString("Class 1 Name:"));
		row0.createCell(1).setCellValue(createHelper.createRichTextString("Class 2 Name:"));
		row0.createCell(2).setCellValue(createHelper.createRichTextString("Parametric:"));
		row0.createCell(3).setCellValue(createHelper.createRichTextString("Syb Typing:"));
		row0.createCell(4).setCellValue(createHelper.createRichTextString("Overloading:"));
		row0.createCell(5).setCellValue(createHelper.createRichTextString("Elements amount:"));
		row0.createCell(6).setCellValue(createHelper.createRichTextString("Parametric Recommendation:"));
		row0.createCell(7).setCellValue(createHelper.createRichTextString("Sub Typing Recommendation:"));
		row0.createCell(8).setCellValue(createHelper.createRichTextString("Overloading Recommendation:"));
		int rowCount = 1;
		// Putting data inside the table
		for (SDComp tempInf : reccs) {
			XSSFCell cell = null;
			XSSFRow row = sheet.createRow(rowCount++);
			String cls1 = tempInf.getClass1();
			String cls2 = tempInf.getClass2();
			int subTyping = tempInf.getRef() + tempInf.getExt() + tempInf.getRefExt();
			int total = tempInf.getTot();
			// Class 1 Name
			cell = row.createCell(0);
			cell.setCellValue(createHelper.createRichTextString(cls1));
			cell.setCellStyle(style);
			cell.getRichStringCellValue().toString();

			// Class 2 Name
			cell = row.createCell(1);
			cell.setCellValue(createHelper.createRichTextString(cls2));
			cell.setCellStyle(style);
			cell.getRichStringCellValue().toString();

			cell = row.createCell(2);
			cell.setCellValue(tempInf.getUse());
			cell.setCellStyle(style);

			cell = row.createCell(3);
			cell.setCellValue(subTyping);
			cell.setCellStyle(style);

			cell = row.createCell(4);
			cell.setCellValue(tempInf.getOverloading());
			cell.setCellStyle(style);

			// Elements amount
			cell = row.createCell(5);
			cell.setCellValue(total);
			cell.setCellStyle(style);

			// Inheritance recommendation
			cell = row.createCell(6);
			cell.setCellValue(tempInf.getUsePart());
			cell.setCellStyle(style_prct);

			cell = row.createCell(7);
			double subPre = ((double) subTyping) / total;
			cell.setCellValue(subPre);
			cell.setCellStyle(style_prct);

			// Template recommendation
			cell = row.createCell(8);
			cell.setCellValue(tempInf.getOverloadingPart());
			cell.setCellStyle(style_prct);
		}
		for (int i = 0; i < row0.getLastCellNum(); i++)
			sheet.autoSizeColumn(i);
		return wb;
	}

	/*
	 * private int calCat(double tmpPre, double inhPre, int NotSameName) { boolean
	 * goodTmp=100*tmpPre>TmpTH; boolean goodInh=100*inhPre>SpTH;
	 * if(goodTmp&&goodInh) return 0+NotSameName*4; if(!goodTmp&&goodInh) return
	 * 1+NotSameName*4; if(goodTmp&&!goodInh) return 2+NotSameName*4;
	 * if(!goodTmp&&!goodInh) return 3+NotSameName*4; return 0; }
	 */
	// ------------------------------------------------ Mechanism Recommendation
	// -------------------------------------------------
	/**
	 * Type comparing.
	 * 
	 * @param type1
	 * @param type2
	 * @return
	 */
	private double typeSim(String type1, String type2, JClass clss1, JClass clss2) {
		// System.err.println("***********");
		// System.err.println(clss1.getClassName()+" "+clss2.getClassName()+" "+type1+"
		// "+type2);
		Double simByMap = typesMap.get(new CommutativePair<>(type1, type2));
		if (simByMap != null)
			return simByMap;
		JClass cls1 = nameToCls.get(type1 + "##1");
		JClass cls2 = nameToCls.get(type2 + "##2");
		if (cls1 == null || cls2 == null || ((ConnThread) Thread.currentThread()).addPair(type1 + "#" + type2)) {
			// System.err.println();
			if ((type1 != null) && (type2 != null))
				if (type1.toLowerCase().equals(type2.toLowerCase()))
					return 1.0;
			return 0.0;
		}
		// System.err.println(cls1.getClassName()+" "+cls2.getClassName());
		// System.err.println();
		return clsRes.get(new PointerPair<>(cls1, cls2)).getUsePart();
	}

	/**
	 * Name parsing;
	 * 
	 * @param name
	 * @return
	 */

	private String nameParsing(String name) {
		String sparatedName = new String();
		sparatedName = "";

		if (name.contains("_")) {
			String aa[] = name.split("_");

			for (int i = 0; i < aa.length; i++)
				if (aa[i].length() >= 2)
					sparatedName += aa[i] + " ";
		} else
			sparatedName = name;

		String cleanName = new String();
		cleanName = "";

		for (int i = 0; i < sparatedName.length(); i++) {
			if (Character.isUpperCase(sparatedName.charAt(i)) && (i < sparatedName.length() - 1)
					&& Character.isLowerCase(sparatedName.charAt(i + 1)))
				cleanName += " ";
			if (Character.isDigit(sparatedName.charAt(i)))
				continue;
			cleanName += sparatedName.charAt(i);
		}

		if (cleanName.contains(" ")) {
			String aa[] = cleanName.split(" ");
			cleanName = "";
			for (int i = 0; i < aa.length; i++)
				if (aa[i].length() >= 2) {
					if (i == aa.length - 1)
						cleanName += (aa[i]);
					else
						cleanName += (aa[i] + " ");
				}
		} else
			cleanName = sparatedName;

		// System.err.println("Original Name: "+name+", Separated Name:" + sparatedName
		// + ", Cleaned Name: "+cleanName);

		return cleanName;
		// return name.replaceAll(" ", "").replaceAll("_", "");
	}

	/**
	 * Name similarity calculation.
	 * 
	 * @param name1
	 * @param name2
	 * @return
	 */
	private double nameSim(String name1, String name2, Connection conn) {

		if ((name1 == null) || (name2 == null))
			return 0.0;
		if (name1.equals(name2))
			return 1.0;

		String cleanName1 = new String();
		String cleanName2 = new String();

		cleanName1 = nameParsing(name1);
		cleanName2 = nameParsing(name2);

		if (cleanName1.equals(cleanName2))
			return 0.9999999;

		double outcome = 0.0;
		if (!cleanName1.isEmpty() && !cleanName2.isEmpty()) {
			if (this.simtype.equals("MCS"))
				outcome = mcs.calculateSimilarity(cleanName1, cleanName2, conn);
			if (this.simtype.equals("UMBS"))
				outcome = umbs.calculateSimilarity(cleanName1, cleanName2, conn);
			if (this.simtype.equals("LSA"))
				outcome = lsa.calculateSimilarity(cleanName1, cleanName2, conn);
		}
		if (outcome < 0.0)
			outcome = 0.0;
		return outcome - 0.0000001;
	}
}
