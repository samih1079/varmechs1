package sim;

public class ProjectSim {
	private int use;
	private int ref;
	private int ext;
	private int ref_ext;
	private int overloading;
	private int tot;
	public ProjectSim(int use, int ref, int ext, int ref_ext, int overloading, int tot){
		this.use=use;
		this.ref=ref;
		this.ext=ext;
		this.ref_ext=ref_ext;
		this.overloading=overloading;
		this.tot=tot;
	}
	public int getUse(){
		return this.use;
	}
	public int getRef(){
		return this.ref;
	}
	public int getExt(){
		return this.ext;
	}
	public int getRefExt(){
		return this.ref_ext;
	}
	public int getTot(){
		return this.tot;
	}
	public int getOverloading() {
		return overloading;
	}
	@Override
	public String toString() {
		return "ProjectSim [use=" + use + ", ref=" + ref + ", ext=" + ext + ", ref_ext=" + ref_ext + ", overloading="
				+ overloading + ", tot=" + tot + "]";
	}
	
}
