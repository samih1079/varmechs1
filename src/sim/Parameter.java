package sim;

public class Parameter {
	String name;
	String type;

	
	public String toString() {
		return name + ":" + type;
	}

	public Parameter(String name, String type) {
		this.name = name;
		this.type = type;
	}
	public Parameter() {
		this.name = null;
		this.type = null;
	}
	public Parameter(Parameter p) {
		this.name = p.name;
		this.type = p.type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Parameter)
			if (this.getName().equals(((Parameter) obj).getName()))
				if (this.getType().equals(((Parameter) obj).getType()))
					return true;
		return false;
	}

}
