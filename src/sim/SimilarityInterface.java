package sim;

import java.sql.Connection;

public interface SimilarityInterface {
    
    public double calculateSimilarity(String s1, String s2, Connection conn);
    
}

