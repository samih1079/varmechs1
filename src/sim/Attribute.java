package sim;

public class Attribute {
	
	private String name, type, visibility;

	public Attribute(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public Attribute(String name, String type, String visibility) {
		super();
		this.name = name;
		this.type = type;
		this.visibility = visibility;
	}

	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Attribute [name=" + name + ", type=" + type + ", visibility="
				+ visibility + "]\n";
	}
}
