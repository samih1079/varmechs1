package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class JClass{
	private String className;
	private Set<JMethod> methods=new HashSet<>();
	public JClass(String className) {
		this.className=className;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Set<JMethod> getMethods() {
		return Collections.unmodifiableSet(methods);
	}
	public void addMethod(JMethod method) {
		if(!methods.contains(method))
			this.methods.add(method);
	}
	public void addAllMethods(Collection<JMethod> methods) {
		this.methods.addAll(methods);
	}
	public int hashCode() {
		return className.hashCode()+31*methods.hashCode();
	}
	public String toString(){
		return className+" : "+methods;
	}
}
