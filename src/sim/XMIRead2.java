package sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

/**
 * @author Iris - replaces XMIReadFromFolder
 */

public class XMIRead2{

	private final static String ATTRIBUTES = "ownedAttribute";
	private final static String METHODS = "ownedOperation";
	private final static String CLASSES = "ownedMember";
	private final static String PARAMETERS = "ownedParameter";

	private final static String COUPLINGS = "UML:Associationend";
	private final static String CLASS_INHERITANCES = "generalization";

	private String folders[];
	private String progs[];
	private ArrayList<XMIStruct> [] classes;
	/* String temp;
	String prog1name;
	String prog2name;
	public static File fileUse; */

	public XMIRead2(String folder1, String folder2, String prog1name, String prog2name)
	{
		super();
		this.folders= new String[2];
		this.folders[0] = folder1;
		this.folders[1] = folder2;
		this.progs=new String[2];
		this.progs[0]=prog1name;
		this.progs[1]=prog2name;
		this.classes=null;
	}

	public ArrayList[] ReadFromFile()
	{
		classes = new ArrayList [2];
		try
		{
	        //File folder = new File(folderPath + "\\");	//program 1
	        for (int w=0; w<2 ; w++)					// for each application
	        {
	    		classes [w] = new ArrayList<XMIStruct>();

	    		String tempFolderStr=this.folders[0];
	    		if (w>0)
	    			tempFolderStr=this.folders[1];
	    		File tempFolder=new File(tempFolderStr);
		        File[] listOfFiles = tempFolder.listFiles();
		        File xmiFile=null;
		        for(File file : listOfFiles){
		        	if((file.isFile()) && file.getName().endsWith(".xmi")){
		        		xmiFile=file;
		        		break;
		        	}
		        }
		        System.out.println("FILE READ: "+ xmiFile.getName() + "\n");
		        try {
		        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		        	Document doc = dBuilder.parse(xmiFile);

		        	//optional, but recommended - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		        	doc.getDocumentElement().normalize();
		        	//NodeList listParameters;
		        	Map<String,String> IdToName = new HashMap<>();
		        	Map<String,String> clsToInh = new HashMap<>();
		        	List<Element> elCls = new LinkedList<>();
		        	// retrieve classes
		        	NodeList listClass = doc.getElementsByTagName("reference");
		        	
		        	//set the maps
		        	for (int j = 0; j < listClass.getLength(); j++) {
		            	Element classLocEl = (Element) listClass.item(j);//the class location element
		            	//the class it self
		            	Element realClass=(Element) classLocEl.getParentNode().getParentNode().getParentNode();
		            	String clsName=realClass.getAttribute("name");
		            	if(!isInteger(clsName.substring(clsName.indexOf('$')+1, clsName.length()))){
		            		String url=classLocEl.getAttribute("url");//the location
			            	String xmiName=xmiFile.getName();
			            	//the location relative to the project
			            	String base=xmiName.substring(0, xmiName.length()-".xmi".length());
			            	//the class name using import like structure
			            	String className=url.substring(url.lastIndexOf(base)+base.length()+1, url.length()-".class".length()).replace('\\', '.');
		            		IdToName.put(realClass.getAttribute("xmi:id"),className);
		            		elCls.add(realClass);
		            	}
		        	}
		        	IdToName.forEach((a,b)->System.out.println(a+" , "+b));
		        	//System.out.println();
		            for (Element currClass: elCls) {
		            	System.out.println(currClass);
		            	String typeClass = currClass.getAttribute("xmi:type");
		            	if ((typeClass != null) && (typeClass.equals ("uml:Class"))) {
		            		String className=IdToName.get(currClass.getAttribute("xmi:id"));
		            		XMIStruct xmiC = new XMIStruct (progs[w], className, currClass.getAttribute("xmi:id"));
		            		// retrieve attributes
		            		NodeList listAttributes = currClass.getElementsByTagName(ATTRIBUTES);
				            for (int k = 0; k < listAttributes.getLength(); k++) {
				            	Element currAtt = (Element) listAttributes.item(k);
				            	String typeAtt = currAtt.getAttribute("xmi:type");
				            	if ((typeAtt !=null) && (typeAtt.equals ("uml:Property")) && !currAtt.getAttribute("name").contains("this$")) { // this$ - avoid subclasses ???
				            		Attribute att = new  Attribute(currAtt.getAttribute("name"), updateType(currAtt.getAttribute("type"),IdToName), currAtt.getAttribute("visibility"));
				            		xmiC.addAttribute(att);
				            	}
				            }
				            // retrieve methods
				            NodeList listMethods = currClass.getElementsByTagName(METHODS);
				            for (int k = 0; k < listMethods.getLength(); k++) {
				            	Element currMethod = (Element) listMethods.item(k);
				            	String typeMethod = currMethod.getAttribute("xmi:type");
				            	if ((typeMethod !=null) && (typeMethod.equals ("uml:Operation")) && !currMethod.getAttribute("name").contains("access$") && !isInteger(currMethod.getAttribute("name"))) { // access$ - avoid subclasses
				            		Shallow mthd = new Shallow(currMethod.getAttribute("name"), "void", VisabilityEnum.valueOf(currMethod.getAttribute("visibility").toUpperCase()));
				            		NodeList params = currMethod.getElementsByTagName(PARAMETERS);		// retrieve the parameters of the method
				            		for (int l = 0; l < params.getLength(); l++) {
				            			Element currParam = (Element) params.item(l);
				            			if ((currParam.getAttribute("xmi:type") !=null) && (currParam.getAttribute("xmi:type").equals ("uml:Parameter"))) {
				            				String paramType=currParam.getAttribute("type");     				
				            				paramType=updateType(paramType,IdToName);
				            				if (currParam.getAttribute("kind").equals ("return"))
				            					mthd.setReturnType(paramType);
				            				else
				            					mthd.addParameter(new Parameter (currParam.getAttribute("name"), paramType));
				            			}
				            		}
				            		//System.out.println(mthd.getName()+" "+mthd.getReturnType()+" "+mthd.getPara());
				            		xmiC.addMethod(mthd);
				            	}
				            }
				            NodeList listInh = currClass.getElementsByTagName(CLASS_INHERITANCES);
							for (int k = 0; k < listInh.getLength(); k++) {								
								Element currInh = (Element) listInh.item(k);
								System.out.println(currInh+" "+k);
								System.out.println(currInh.getAttribute("general"));
								String classInhName=IdToName.get(currInh.getAttribute("general"));
								if(className!=null&&classInhName!=null)
									clsToInh.put(className,classInhName);
							}
							
				            classes[w].add(xmiC);
		            	}
		            }
		            clsToInh.forEach((a,b)->System.out.println(a+" "+b));
		            List<Set<String>> inhList=new LinkedList<>();
		            System.out.println();
		            while(clsToInh.size()>0){
			            boolean first=true;
			            boolean added=false;
			            List<Entry<String, String>> l=new LinkedList<>();
		        		for(Entry<String, String> entry2:clsToInh.entrySet()){
		        			if(first){
		        				l.add(entry2);
				        		first=false;
		        			}
		        			else{
		        				added=false;
			        			String cls2=entry2.getKey();
				        		String cls2inh=entry2.getValue();
				        		for(Entry<String, String> entry1:l){
				        			String cls1=entry1.getKey();
					        		String cls1inh=entry1.getValue();
					        		if(cls2.equals(cls1inh)||cls2inh.equals(cls1)||
					        				cls2inh.equals(cls1inh)||cls2.equals(cls1)){
					        			added=true;
					        			break;
					        		}
				        		}
				        		if(added)
				        			l.add(entry2);
		        			}
			        	}
		        		l.forEach(a->clsToInh.remove(a.getKey()));
		        		l.sort((a,b)->a.getKey().compareTo(b.getValue()));     		
		            	Set<String> ll=new LinkedHashSet<>();
		            	System.out.println(l);
		            	Collections.reverse(l);
		            	//add by inheritance order
		            	l.forEach(a->ll.add(a.getValue()));  
		            	//add unbound descendants
		            	l.forEach(a->{
		            		if(!ll.contains(a.getKey()))
		            			ll.add(a.getKey());
		            	});  
		        		inhList.add(ll);
		            }		            
		            inhList.forEach(a->System.out.println(a));
			    } catch (SAXParseException err) {
					System.out.println("** Parsing error" + ", line "
							+ err.getLineNumber() + ", uri " + err.getSystemId());
					System.out.println(" " + err.getMessage());
				} catch (SAXException e) {
					Exception x = e.getException();
					((x == null) ? e : x).printStackTrace();
				} catch (Throwable t) {
					t.printStackTrace();
				}
		        toExcel(classes[w], progs[w], folders[w]);
	        }
	      return classes;
		}
		 catch (Exception e)
		{
			 e.printStackTrace();
		}
		return null;
	}
	//update typeID with there names according to the map
	private String updateType(String type, Map<String, String> idToName) {
		if (type.endsWith("_id"))
			type=type.substring(0, type.length()-3);
		else
			if((type=idToName.get(type))==null)
				type="";
		return type;
	}

	public static boolean isInteger(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch(NumberFormatException e) {
	        return false;
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}

   public static String findActType (String typeCode, ArrayList<XMIStruct> array) {
	   String result="";
	   for (XMIStruct it:array) {
		   if (it.getXmiID().equals(typeCode)) {
				result = it.getName();
	     		//System.out.println(typeCode+" "+result);
	     		break;
		   }
	   }
	   return result;
   }

	/**
     *
     * @param classes
     * @throws IOException
     */
    public static void toExcel(ArrayList<XMIStruct> classes, String i, String folderPath) throws IOException
    {
		try
		{
			XSSFWorkbook wb = new XSSFWorkbook();
			XSSFSheet sheet = wb.createSheet("XMI Table");
			CreationHelper createHelper = wb.getCreationHelper();

			 // Cosmetics
			XSSFFont defaultFont= wb.createFont();
		    defaultFont.setFontHeightInPoints((short)10);
		    defaultFont.setFontName("Arial");
		    defaultFont.setBold(true);

		    XSSFFont font= wb.createFont();
		    font.setFontHeightInPoints((short)10);
		    font.setFontName("Arial");
		    font.setBold(true);
		    font.setItalic(false);

		    CellStyle style = wb.createCellStyle();
		    style.setAlignment(CellStyle.ALIGN_CENTER);
		    style.setFont(font);

			 // Creating the header for methods	    
			 XSSFRow row0 = sheet.createRow((short)0);
			 row0.createCell(0).setCellValue(createHelper.createRichTextString("Class:"));
			 row0.createCell(1).setCellValue(createHelper.createRichTextString("Category:"));
			 row0.createCell(2).setCellValue(createHelper.createRichTextString("Element Name:"));
			 row0.createCell(3).setCellValue(createHelper.createRichTextString("Parent Name:"));
			 row0.createCell(4).setCellValue(createHelper.createRichTextString("Type:"));
			 row0.createCell(5).setCellValue(createHelper.createRichTextString("Project:"));

			 // Putting data inside the table
			 int rowCount = 1;

			 for (XMIStruct ds : classes)
			 {
				 String className=ds.getName();
		    	 String fixedName=className.substring(className.lastIndexOf(".")+1,
		    			 className.length());
				 XSSFCell cell = null;
				 XSSFRow row = sheet.createRow(rowCount++);
		    	 // Attributes output
				 for (Attribute att : ds.getPara())
				 {
					 row = sheet.createRow(rowCount++);
			    	 cell = row.createCell(0); 
			    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
			    	 cell.setCellStyle(style);
			    	 cell = row.createCell(1);
			    	 cell.setCellValue(createHelper.createRichTextString("Attribute"));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(2);
			    	 cell.setCellValue(createHelper.createRichTextString(att.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(3);
			    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(4);
			    	 cell.setCellValue(createHelper.createRichTextString(att.getType()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(5);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
					 cell.setCellStyle(style);
				 }
		    	 // Methods output
				 for (Shallow met : ds.getMethods())
				 {
					 row = sheet.createRow(rowCount++);
					 cell = row.createCell(0);
			    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
			    	 cell.setCellStyle(style);
			    	 cell = row.createCell(1);
			    	 cell.setCellValue(createHelper.createRichTextString("Operation"));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(2);
			    	 cell.setCellValue(createHelper.createRichTextString(met.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(3);
			    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(4);
			    	 cell.setCellValue(createHelper.createRichTextString(met.getReturnType()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(5);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
					 cell.setCellStyle(style);

			    	 // Parameters output
					 for (Parameter par : met.getPara())
					 {
						 row = sheet.createRow(rowCount++);
						 cell = row.createCell(0);
				    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
				    	 cell.setCellStyle(style);
				    	 cell = row.createCell(1);
				    	 cell.setCellValue(createHelper.createRichTextString("Parameter"));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(2);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getName()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(3);
				    	 cell.setCellValue(createHelper.createRichTextString(met.getName()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(4);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getType()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(5);
				    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
						 cell.setCellStyle(style);
					 }
				 }
			 }
			 // Cosmetics
			 sheet.autoSizeColumn(0);
			 sheet.autoSizeColumn(1);
			 sheet.autoSizeColumn(2);
			 sheet.autoSizeColumn(3);
			 sheet.autoSizeColumn(4);
			 sheet.autoSizeColumn(5);
			 // Write the output to a file.
			 File f = new File(folderPath);
			 // System.out.println ("folder for xmi file" + i + ": "+f.getParent());
			 FileOutputStream fileOut = new FileOutputStream(f.getParent()+"\\XmiDataFile_" + i +  ".xlsx");
			 wb.write(fileOut);
			 fileOut.close();
			 wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }

}
