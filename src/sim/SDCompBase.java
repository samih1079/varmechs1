package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

public class SDCompBase {
	protected String cl1;
	protected String cl2;
	protected int use;
	protected int ref;
	protected int ext;
	protected int ref_ext;
	protected int overloading;
	protected int nullAmnt;
	protected int tot;
	public SDCompBase(String cl1, String cl2, int use, int ref, int ext, int ref_ext, int overloading, int nullAmnt, int tot){
		this.cl1=cl1;
		this.cl2=cl2;
		this.use=use;
		this.ref=ref;
		this.ext=ext;
		this.ref_ext=ref_ext;
		this.overloading=overloading;
		this.nullAmnt=nullAmnt;
		this.tot=tot;
	}
	public String toJson()
	{
		return new Gson().toJson(this);
	}
	public String getClass1(){
		return this.cl1;
	}
	public String getClass2(){
		return this.cl2;
	}
	public int getUse(){
		return this.use;
	}
	public int getRef(){
		return this.ref;
	}
	public int getExt(){
		return this.ext;
	}
	public int getRefExt(){
		return this.ref_ext;
	}
	public int getOverloading() {
		return overloading;
	}
	public int getNullAmnt() {
		return nullAmnt;
	}
	public void setNullAmnt(int nullAmnt) {
		this.nullAmnt = nullAmnt;
	}
	public double getUsePart(){
		return ((double)use)/tot;
	}
	public double getRefPart(){
		return ((double)ref)/tot;
	}
	public double getExtPart(){
		return ((double)ext)/tot;
	}
	public double getRefExtPart(){
		return ((double)ref_ext)/tot;
	}
	public double getOverloadingPart() {
		return ((double)overloading)/tot;
	}	
	public double getNullPart() {
		return ((double)nullAmnt)/tot;
	}
	public int getTot(){
		return this.tot;
	}
	public double getHuerVal(){
		return ((double)(use*5+ref*4+ext*3+ref_ext*2+overloading))/tot;
	}
}
