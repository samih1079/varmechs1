package sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author Iris - replaces XMIReadFromFolder
 */

public class XMIRead{

	private final static String ATTRIBUTES = "ownedAttribute";
	private final static String METHODS = "ownedOperation";
	private final static String CLASSES = "ownedMember";
	private final static String PARAMETERS = "ownedParameter";

	private final static String COUPLINGS = "UML:Associationend";
	private final static String CLASS_INHERITANCES = "generalization";

	private String folders[];
	private String progs[];
	private  XmiAdditionalInfo [] classes;
	/* String temp;
	String prog1name;
	String prog2name;
	public static File fileUse; */

	public XMIRead(String folder1, String folder2, String prog1name, String prog2name)
	{
		super();
		this.folders= new String[2];
		this.folders[0] = folder1;
		this.folders[1] = folder2;
		this.progs=new String[2];
		this.progs[0]=prog1name;
		this.progs[1]=prog2name;
		this.classes=null;
	}

	public XmiAdditionalInfo[] ReadFromFile()
	{
		classes = new XmiAdditionalInfo[2];
		classes[0]=readFromFile(folders[0], progs[0]);
		classes[1]=readFromFile(folders[1], progs[1]);
		return classes;
	}
	
   private static String updateType(String type, Map<String, String> idToName) {
		if (type.endsWith("_id"))
			type=type.substring(0, type.length()-3);
		else
			if((type=idToName.get(type))==null)
				type="";
		return type;
	}

	public static XmiAdditionalInfo readFromFile(String xmiFolder, String progName) {
        File[] listOfFiles = new File(xmiFolder).listFiles();
        File xmiFile=null;
        for(File file : listOfFiles){
        	if((file.isFile()) && file.getName().endsWith(".xmi")){
        		xmiFile=file;
        		break;
        	}
        }
        System.out.println("FILE READ: "+ xmiFile.getName() + "\n");
        
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    	DocumentBuilder dBuilder;
    	Document doc;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmiFile);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
    	//optional, but recommended - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
    	doc.getDocumentElement().normalize();
    	//NodeList listParameters;
    	Map<String,String> IdToName = new HashMap<>();
    	Map<String,String> clsToInh = new HashMap<>();
    	List<Element> elCls = new LinkedList<>();
    	// retrieve classes
    	NodeList listClass = doc.getElementsByTagName("reference");
    	
    	//set the maps
    	for (int j = 0; j < listClass.getLength(); j++) {
        	Element classLocEl = (Element) listClass.item(j);//the class location element
        	//the class it self
        	Element realClass=(Element) classLocEl.getParentNode().getParentNode().getParentNode();
        	//if(!ReaderHelper.isInteger(clsName.substring(clsName.indexOf('$')+1, clsName.length()))){
        	String className=realClass.getAttribute("name");
    		//System.out.println(className);
    		String packagePath="";
    		Element parentNode = (Element) realClass.getParentNode();
    		while(parentNode!=null&&!parentNode.getNodeName().equals("uml:Model")){
    			//System.out.println(parentNode);
    			if(parentNode.getNodeName().equals("ownedMember")){
    				String ptypeClass = parentNode.getAttribute("xmi:type");
    				//System.out.println(ptypeClass);
        			if((ptypeClass != null) && ptypeClass.equals("uml:Package")){
        				String packageName = parentNode.getAttribute("name");
        				if(packageName != null)
        					packagePath=packageName+"."+packagePath;
        			}
    			}
    			parentNode=(Element) parentNode.getParentNode();
    		}
    		className=packagePath+className;
    		IdToName.put(realClass.getAttribute("xmi:id"),className);
    		elCls.add(realClass);
    	}
    	//IdToName.forEach((a,b)->System.out.println(a+" , "+b));
    	//System.out.println();
    	XmiAdditionalInfo addInfo = new XmiAdditionalInfo();
        for (Element currClass: elCls) {
        	//System.out.println(currClass);
        	String typeClass = currClass.getAttribute("xmi:type");
        	if ((typeClass != null) && (typeClass.equals ("uml:Class"))) {
        		String className=IdToName.get(currClass.getAttribute("xmi:id"));
	            NodeList listInh = currClass.getElementsByTagName(CLASS_INHERITANCES);
	           // System.out.println(className);
				for (int k = 0; k < listInh.getLength(); k++) {								
					Element currInh = (Element) listInh.item(k);
					/*System.out.println(currInh+" "+k);
					System.out.println(currInh.getAttribute("general"));*/
					String classInhName=IdToName.get(currInh.getAttribute("general"));
					if(className!=null&&classInhName!=null)
						clsToInh.put(className,classInhName);
				}
		    	addInfo.setClassHir(ReaderHelper.dominoSort(clsToInh.entrySet()));
		    	NodeList listMethods = currClass.getElementsByTagName(METHODS);
		        for (int k = 0; k < listMethods.getLength(); k++) {
		        	Element currMethod = (Element) listMethods.item(k);
		        	String typeMethod = currMethod.getAttribute("xmi:type");
		        	if ((typeMethod !=null) && (typeMethod.equals ("uml:Operation")) && !currMethod.getAttribute("name").contains("access$") && !ReaderHelper.isInteger(currMethod.getAttribute("name"))) { // access$ - avoid subclasses
		        		Shallow mthd = new Shallow(currMethod.getAttribute("name"), "void", VisabilityEnum.valueOf(currMethod.getAttribute("visibility").toUpperCase()));
		        		NodeList params = currMethod.getElementsByTagName(PARAMETERS);		// retrieve the parameters of the method
		        		for (int l = 0; l < params.getLength(); l++) {
		        			Element currParam = (Element) params.item(l);
		        			if ((currParam.getAttribute("xmi:type") !=null) && (currParam.getAttribute("xmi:type").equals ("uml:Parameter"))) {
		        				String paramType=currParam.getAttribute("type");  
		        				paramType=updateType(paramType,IdToName);
		        				NodeList childNodes = currParam.getElementsByTagName("typeModifier");
		        				//System.out.println(childNodes);
			        			int childNodesLen = childNodes.getLength();
			        			for(int i=0;i<childNodesLen;i++){
			        				Node childOfParam=childNodes.item(i);
			        				String typeVal = ((Element)childOfParam).getAttribute("xmi:value");
		        					if(typeVal!=null)
		        						paramType+=typeVal;
			        			}
			        			
		        				
		        				if (currParam.getAttribute("kind").equals("return"))
		        					mthd.setReturnType(paramType);
		        				else
		        					mthd.addParameter(new Parameter (currParam.getAttribute("name"), paramType));	
		        			}
		        		}
						//System.out.println(mthd.getName()+" "+mthd.getReturnType()+" "+mthd.getPara());
		        		//System.out.println(mthd);
		        		addInfo.addMethod(className, mthd);
		        	}
		        }
        	}
        }
        
        //clsToInh.forEach((a,b)->System.out.println(a+" "+b));		           
        return addInfo; 	 
	}


}
