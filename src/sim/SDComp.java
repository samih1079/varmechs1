package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SDComp extends SDCompBase{
	
	private List<MethodSim> msL=new LinkedList<>();
	public SDComp(String cl1, String cl2, int use, int ref, int ext, int ref_ext, int overloading, int nullAmnt, int tot)
	{
		super(cl1, cl2, use, ref, ext, ref_ext, overloading, nullAmnt, tot);		
	}
	public void addMethodSim(MethodSim sim){
		if(sim.getType()!=ClassSim.NULL)
			msL.add(sim);
	}
	public List<MethodSim> getMethodSim(){
		return Collections.unmodifiableList(msL);
	}
	public void addAllMethodSim(Collection<MethodSim> simL){
		for(MethodSim sim : simL) {
			this.addMethodSim(sim);
		}
	}
	public SDCompBase getSDCompBaseCopy()
	{
		return new SDCompBase(cl1, cl2, use, ref, ext, ref_ext, overloading, nullAmnt, tot);
	}
	
}
