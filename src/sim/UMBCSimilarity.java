package sim;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
//import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import java.sql.Connection;

import semilar.config.ConfigManager;
import semilar.data.Sentence;
import semilar.tools.preprocessing.SentencePreprocessor;

/**
 * ``
 *
 * @author Lenovo1
 */
public class UMBCSimilarity implements SimilarityInterface {

	private ILexicalDatabase db;
	private RelatednessCalculator rc;

	public UMBCSimilarity() {
		db = new NictWordNet();
		rc = new WuPalmer(db);
	}

	public double calculateSimilarity(String s1, String s2, Connection conn) {
		char c = '"';
		String[] ftrimNoArticle;
		String[] ftrimNoArticle2;
		HashSet<String> articles = new HashSet<String>();
		s1 = s1.replaceAll("" + c, "");
		s2 = s2.replaceAll("" + c, "");
		articles.add("a");
		articles.add("an");
		articles.add("the");
		articles.add("that");
		articles.add("this");
		articles.add("these");
		articles.add("those");
		articles.add("any");

		Double sim = 0.0;
		String operation = "api";
		String corpus = "webbase";
		String type = "relation";

		String url = "http://swoogle.umbc.edu/SimService/GetSimilarity";

		// HttpClient client = new HttpClient();
		PostMethod method;
		WS4JConfiguration.getInstance().setMFS(false);

		if (s1 == null || s1 == "" || s2 == null || s2 == "") {
			return 0.0;
		} else {
			String ftrim = s1.replaceAll("`", "'");
			String ftrim2 = s2.replaceAll("`", "'");
			// ConfigManager.setSemilarDataRootFolder("C://SEMILARhome/");
			// ConfigManager.setSemilarDataRootFolder("C://Users/Nili/workspace/.metadata/.plugins/org.eclipse.ltk.core.refactoring/.refactorings/");
			// ConfigManager.setSemilarDataRootFolder("C://UFromDeskTop/WorkSpaces/workspace/.metadata/.plugins/org.eclipse.ltk.core.refactoring/.refactorings/");
			// SentencePreprocessor preprocessor = new
			// SentencePreprocessor(SentencePreprocessor.TokenizerType.STANFORD,
			// SentencePreprocessor.TaggerType.STANFORD,
			// SentencePreprocessor.StemmerType.PORTER,
			// SentencePreprocessor.ParserType.STANFORD);
			// Sentence sentence1 = preprocessor.preprocessSentence(ftrim);
			// Sentence sentence2 = preprocessor.preprocessSentence(ftrim2);

			ftrimNoArticle = ftrim.trim().split(" ");
			ftrimNoArticle2 = ftrim2.trim().split(" ");

			if (ftrimNoArticle != null && ftrimNoArticle2 != null) {
				if ((ftrim.trim().toLowerCase()).equals((ftrim2.trim().toLowerCase()))) {
					sim = 1.0;
				} else if ((ftrimNoArticle.length == 1 && ftrimNoArticle2.length == 1)) {
					sim = rc.calcRelatednessOfWords(ftrimNoArticle[0], ftrimNoArticle2[0]);
				} else if ((ftrimNoArticle.length == 2 && ftrimNoArticle2.length == 2)
						&& (articles.contains(ftrimNoArticle[0].toLowerCase()))
						&& (articles.contains(ftrimNoArticle2[0].toLowerCase()))) {
					if (ftrimNoArticle[1].trim().equals(ftrimNoArticle2[1].trim())) {
						sim = 1.0;
					} else {
						sim = rc.calcRelatednessOfWords(ftrimNoArticle[1], ftrimNoArticle2[1]);
					}
				} else if (ftrimNoArticle.length == 2 && articles.contains(ftrimNoArticle[0].toLowerCase())
						&& ftrimNoArticle2.length == 1) {
					if (ftrimNoArticle[1].trim().equals(ftrimNoArticle2[0].trim())) {
						sim = 1.0;
					} else {
						sim = rc.calcRelatednessOfWords(ftrimNoArticle[1], ftrimNoArticle2[0]);
					}
				} else if (ftrimNoArticle2.length == 2 && articles.contains(ftrimNoArticle2[0].toLowerCase())
						&& ftrimNoArticle.length == 1) {
					if (ftrimNoArticle2[1].trim().equals(ftrimNoArticle[0].trim())) {
						sim = 1.0;
					} else {
						sim = rc.calcRelatednessOfWords(ftrimNoArticle[0], ftrimNoArticle2[1]);
					}
				} else {
					method = new PostMethod(url);
					method.addParameter("operation", operation);
					method.addParameter("phrase1", s1);
					method.addParameter("phrase2", s2);
					method.addParameter("type", type);
					method.addParameter("corpus", corpus);

					/*
					 * try { //method.addParameter("p", "\"java2s\""); int statusCode =
					 * client.executeMethod(method); if(statusCode == HttpStatus.SC_NOT_IMPLEMENTED)
					 * { System.err.println("The Post method is not implemented by this URI"); //
					 * still consume the response body method.getResponseBodyAsString(); } else { br
					 * = new BufferedReader(new
					 * InputStreamReader(method.getResponseBodyAsStream())); String readLine; if
					 * ((readLine = br.readLine()) != null) { try{
					 * sim=Double.parseDouble(readLine.trim()); if (sim.isInfinite()) { sim=0.0; }
					 * }catch (NumberFormatException e) { sim=0.0; } } }
					 * 
					 * } catch (IOException e) { System.err.println(e); } finally {
					 * method.releaseConnection(); if(br != null) try { br.close(); } catch
					 * (IOException e) {} }
					 */
				}
			}
		}

		System.out.print("**sim=");
		System.out.println(sim.toString() + "**");
		return sim;
	}
}
