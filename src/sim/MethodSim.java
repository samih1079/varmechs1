package sim;
public class MethodSim {
	private String m1;
	private String m2;
	private String cl1;
	private String cl2;
	private ClassSim type;
	public MethodSim(String cl1, String cl2, String m1, String m2, ClassSim res){
		this.cl1=cl1;
		this.cl2=cl2;
		this.m1=m1;
		this.m2=m2;
		this.type=res;
	}
	public MethodSim() {
		// TODO Auto-generated constructor stub
	}
	public String getMethod1() {
		return m1;
	}
	public String getMethod2() {
		return m2;
	}
	public String getClass1() {
		return cl1;
	}
	public String getClass2() {
		return cl2;
	}
	public ClassSim getType() {
		return type;
	}
	public String getM1() {
		return m1;
	}
	public void setM1(String m1) {
		this.m1 = m1;
	}
	public String getM2() {
		return m2;
	}
	public void setM2(String m2) {
		this.m2 = m2;
	}
	public String getCl1() {
		return cl1;
	}
	public void setCl1(String cl1) {
		this.cl1 = cl1;
	}
	public String getCl2() {
		return cl2;
	}
	public void setCl2(String cl2) {
		this.cl2 = cl2;
	}
	public void setType(ClassSim type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "MethodSim [m1=" + m1 + ", m2=" + m2 + ", cl1=" + cl1 + ", cl2=" + cl2 + ", type=" + type + "]";
	}
	
}
