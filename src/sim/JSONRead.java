package sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

import gui.runApplication;
import javafx.util.Pair;
import sun.reflect.generics.factory.CoreReflectionFactory;
import sun.reflect.generics.parser.SignatureParser;
import sun.reflect.generics.tree.MethodTypeSignature;
import sun.reflect.generics.tree.ReturnType;
import sun.reflect.generics.tree.TypeSignature;
import sun.reflect.generics.visitor.Reifier;

/**
 * @author Iris - replaces JSONReadFromFolder
 */

public class JSONRead
{
	String folders[];
	String progs[];
	List<JClass>[] classes;

	public JSONRead(String folder1, String folder2, String prog1name, String prog2name)
	{
		this.folders= new String[2];
		this.folders[0] = folder1;
		this.folders[1] = folder2;
		this.progs=new String[2];
		this.progs[0]=prog1name;
		this.progs[1]=prog2name;
		this.classes=null;
	}
	@SuppressWarnings("unchecked")
	public static List<JClass> readFromFile(String folder, String progname) {
		LinkedList<JClass> classes = new LinkedList<JClass>();
        try
        {
        	System.out.println("FILE READ: JSON for "+ progname + "\n");
    		JSONParser parser = new JSONParser();
    		JSONArray Jinst = (JSONArray) parser.parse(new FileReader(folder+"\\instr.txt"));
    		JSONArray Jmethod = (JSONArray) parser.parse(new FileReader(folder+"\\method.txt"));
    		JSONArray Jclass = (JSONArray) parser.parse(new FileReader(folder+"\\class.txt"));
    		JSONArray Jvar = (JSONArray) parser.parse(new FileReader(folder+"\\variable.txt"));
    		if(Jclass.size()==0)
    			return classes;
    		int metSize = Jmethod.size()+1;
    		Map<Integer,Parameter>[] sigMap=new Map[metSize];
    		JVariable[] vars=new JVariable[Jvar.size()+1];
    		for(int i=0;i<metSize;i++)
    			sigMap[i] = new TreeMap<>();
    		for(Object var : Jvar){
    			JSONObject tVar = (JSONObject) var;
    			int metID = Integer.parseInt((String)tVar.get("METHODID"));
				String varType = (String)tVar.get("VARTYPE");
    			String varName=(String)tVar.get("VARNAME");
    			int varI=Integer.parseInt((String)tVar.get("VARINDEX"));
    			int varID=Integer.parseInt((String)tVar.get("VARID"));
    			String varSig = (String)tVar.get("VARSIGNATURE");
    			SignatureParser sp = SignatureParser.make();
    			JVariable varP = new JVariable(varName, getNormalType(sp.parseTypeSig(varSig)), varType);
    			vars[varID]=varP;
    			if(metID!=-1){
        			if(varType.equals("FormalIn")){
        				//if(!varName.equals("this"))
            				sigMap[metID].put(varI, varP);			
        			}
    			}
    		}
    		JMethod[] methodList=new JMethod[metSize];
			List<Integer>[] invokeList=new List[metSize];
    		for(int i=0;i<metSize;i++)
    			invokeList[i]=new LinkedList<>();
    		Map<String, String>[] sigLists=new Map[metSize];
    		for(int i=0;i<metSize;i++)
    			sigLists[i] = new LinkedHashMap<>();
			for(int i=0;i<metSize;i++)
    			for(Parameter p:sigMap[i].values())
    				sigLists[i].put(p.name, p.type);
    		int clsSize = Integer.parseInt((String)((JSONObject)Jclass.get(Jclass.size()-1)).get("CLASSID"))+1;
    		List<JMethod>[] metLists=new List[clsSize];
    		for(int i=0;i<clsSize;i++)
    			metLists[i] = new LinkedList<>();
    		for(Object m : Jmethod){
    			JSONObject tMet = (JSONObject) m;
    			int metID = Integer.parseInt((String)tMet.get("METHODID"));
    			int metClsId = Integer.parseInt((String)tMet.get("CLASSID"));
    			String metName = (String)tMet.get("METHODNAME");
    			if(metClsId>-1&&!metName.equals("<clinit>")&&!metName.startsWith("access$")){
    				String metSig = (String)tMet.get("METHODSIGNATURE");
    				//System.out.println(metSig);
    				SignatureParser sp = SignatureParser.make();
    				MethodTypeSignature parsedSig = sp.parseMethodSig(metSig);
    				//TypeSignature[] params = parsedSig.getParameterTypes();
    				ReturnType retType = parsedSig.getReturnType();
    				String retTypeName=getNormalType(retType);      				
    				JMethod jMethod = new JMethod(metClsId, metID, metName, retTypeName);
    				
    				//int paramLen = params.length;
        			Iterator<Entry<String, String>> varsIt = sigLists[metID].entrySet().iterator();
        			//System.out.println(metName);
    				while(varsIt.hasNext()){
    					//TypeSignature tType = params[i];
    					//String typeName=getNormalType(tType);     					
    					Entry<String, String> vn = varsIt.next();
    					//System.out.println(typeName.equals(vn.getValue())+" typeCheck "+typeName+" "+vn.getValue());
    					String varName = vn.getKey();
    					Parameter p = new Parameter(varName, vn.getValue());
    					if(varName.equals("this"))
    						jMethod.setHiddenParam(p);
    					else
    						jMethod.addParams(p);
    					
    				}
    				//System.out.println("*****************"+jMethod+"*****************"+metClsId);
    				//System.out.println();
    				//System.out.println();
    				if(!jMethod.getName().equals("main"))
    					metLists[metClsId].add(jMethod);
    				methodList[metID]=jMethod;
				}
    		}
    		InstLine[] instLines=new InstLine[metSize];
    		for(Object inst:Jinst){
    			JSONObject tInst = (JSONObject) inst;
    			String instrOpCode = (String) tInst.get("INSTROPCODE");
    		    //String instType = (String) tInst.get("INSTRTYPE");
    		    int metID = Integer.parseInt((String) tInst.get("METHODID"));
    		    String operand = (String) tInst.get("INSTROPERAND");
    		    InstLine instLine=instLines[metID];
    		    //System.out.println(instrOpCode+" "+metID);
    		    if(instLine==null){
    		    	instLine=new InstLine(methodList[metID]);
    		    	instLines[metID]=instLine;
    		    }
    		    //simple load
    		    if(instrOpCode.matches("([ilfda]load)(_[0123])?")||instrOpCode.equals("getstatic")){
    		    	JVariable varP;
	    			if(instrOpCode.equals("getstatic"))
	    				varP=getVarPutGet(operand, vars);
	    			else
	    				varP=getVarDotsOp(operand, vars);
    		    	instLine.load(getType(varP.getVartype()), varP);
    		    } else {
    		    	//load from array
    		    	if(instrOpCode.matches("([ilfdabcs]aload)")){
        		    	instLine.arrLoad();
        		    } else {
        		    	//simple store
            		    if(instrOpCode.matches("([ilfda]store)(_[0123])?")){
            		    	JVariable varP=getVarDotsOp(operand, vars);
            		    	instLine.store(getType(varP.getVartype()), varP);
            		    } else {
            		    	//store from array
            		    	if(instrOpCode.matches("([ilfdabcs]astore)")){
                		    	instLine.arrStore();
                		    } else {
                		    	//simple 2 arguments math operation
                		    	if(instrOpCode.matches("[ilfd](add|sub|mul|div|rem|and|or|xor)")){
                    		    	instLine.math2args();
                    		    } else {
                    		    	//simple 1 argument math operation
                    		    	if(instrOpCode.matches("[ilfd](neg|shl|shr|ushr|rem)")){
                        		    	instLine.math1arg();
                        		    } else {
                        		    	//int increase
                        		    	if(instrOpCode.equals("iinc")){
                        		    		JVariable varP=getVarDotsOp(operand, vars);
	                            		    instLine.iinc(getType(varP.getVartype()), varP);
                            		    } else {
                            		    	//compare
                            		    	if(instrOpCode.matches("([lfd]cmp)([lg])?")){
                                		    	instLine.cmp();
                                		    } else {
                                		    	//conversion
                                		    	if(instrOpCode.matches("[ilfd]2[ilfdbcs]")){
                                		    		instLine.conv(instrOpCode.charAt(2));
                                		    	} else {
                                		    		//push constant
                                		    		if(instrOpCode.contains("const")||
                                		    				instrOpCode.contains("push")){
            	                            		    instLine.pushConst(instrOpCode.charAt(0));
                                        		    } else {
                                        		    	//branch operation
                                    		    		if(instrOpCode.startsWith("if")){
                	                            		    if(instrOpCode.contains("_")){
                	                            		    	instLine.brn2arg(); //2 args branch
                	                            		    } else {
                	                            		    	instLine.brn1arg(); //1 arg branch
                	                            		    }
                                            		    } else {
                                            		    	if(instrOpCode.equals("putstatic")){
                                            		    		JVariable varP=getVarPutGet(operand, vars);
                                                		    	instLine.putstatic(getType(varP.getVartype()), varP);
                                                		    } else {
                                                		    	if(instrOpCode.equals("putfield")){
                                                		    		JVariable varP=getVarPutGet(operand, vars);
                                                    		    	instLine.putfield(getType(varP.getVartype()), varP);
                                                    		    } else {
                                                    		    	if(instrOpCode.equals("getfield")){
                                                    		    		JVariable varP=getVarPutGet(operand, vars);
                                                        		    	instLine.getfield(getType(varP.getVartype()), varP);
                                                    		    	} else {
                                                        		    	if(instrOpCode.contains("newarray")){
                                                        		    		if(instrOpCode.startsWith("multi"))
                                                        		    			instLine.mulnewarray(Integer.parseInt(operand));
                                                        		    		else
                                                        		    			instLine.newarray();
                                                            		    } else {
                                                            		    	if(instrOpCode.equals("arraylength")){
                                                                		    	instLine.arraylength();
                                                                		    } else {
                                                                		    	if(instrOpCode.contains("return")){
                                                                    		    	instLine.doReturn();
                                                                    		    } else {
                                                                    		    	if(instrOpCode.startsWith("invoke")){
                                                                    		    		String[] operands=operand.split("\\s", 2);
                                                                    		    		JMethod otherMethod=null;
                                                                    		    		System.out.println(instrOpCode);
                                                                    		    		try{
	                                                                    		    		int otherMetId=Integer.parseInt(operands[1]);
																								if(otherMetId!=-1)
		                                                                	        		    	otherMethod=methodList[otherMetId];
                                                                    		    		} catch(Exception e){}
                                                                	        		    String realPart = operands[0];
                                                                	        		    String metSig = realPart
                                                                	        		    		.substring(realPart.indexOf('('),
                                                                	        		    				realPart.length());
                                                                	        		    SignatureParser sp = SignatureParser.make();
                                                                	    				MethodTypeSignature parsedSig = 
                                                                	    						sp.parseMethodSig(metSig);
                                                                	    				TypeSignature[] mParams = 
                                                                	    						parsedSig.getParameterTypes();
                                                                	    				ReturnType retType = parsedSig.getReturnType();
                                                                	    				String retTypeName=getNormalType(retType);
                                                                	    				int paramLen = mParams.length;
                                                                	    				Parameter retParam = 
                                                                	    						new Parameter("retVal", retTypeName);
                                                                    		    		if(instrOpCode.endsWith("ic")){
                                                                            		    	instLine.invokeIc(paramLen, retParam, otherMethod);
                                                                            		    } else {
                                                                            		    	instLine.invokeRef(paramLen, retParam, otherMethod);
                                                                            		    }
                                                                        		    } else {
                                                                        		    	if(instrOpCode.contains("dup")){
                                                                        		    		String[] dupNumX=operand.split("_x");
                                                                        		    		if(instrOpCode.startsWith("dup2")){
                                                                        		    			if(dupNumX.length==2)
                                                                            		    			instLine.dup2(Integer.parseInt(dupNumX[1]));
                                                                            		    		else
                                                                            		    			instLine.dup2();
                                                                        		    		} else {
                                                                        		    			if(dupNumX.length==2)
                                                                            		    			instLine.dup(Integer.parseInt(dupNumX[1]));
                                                                            		    		else
                                                                            		    			instLine.dup();
                                                                        		    		}    		
                                                                        		    	} else {
                                                                        		    		if(instrOpCode.equals("new")){
                                                                            		    		instLine.newObj();
                                                                            		    	} else {
                                                                            		    		if(instrOpCode.contains("jsr")){
                                                                                		    		instLine.jsr();
                                                                                		    	}
                                                                            		    	} if(instrOpCode.contains("ldc")){
                                                                            		    		if(instrOpCode.contains("2"))
                                                                            		    			instLine.pushConst('l');
                                                                            		    		else
                                                                            		    			instLine.pushConst('i');
                                                                            		    	} else {
                                                                            		    		if(instrOpCode.equals("swap")){
                                                                            		    			instLine.swap();
                                                                            		    		} else {
                                                                            		    			if(instrOpCode.equals("tableswitch")){
                                                                                		    			instLine.tableswitch();
                                                                                		    		}
                                                                            		    		}
                                                                            		    	}
                                                                        		    	}
                                                                        		    }
                                                                    		    }
                                                                		    }
                                                            		    } 
                                                        		    }
                                                    		    } 
                                                		    }
                                            		    }
                                        		    }
                                		    	}
                                		    }
                            		    }
                        		    }
                    		    }
                		    }
            		    }
        		    }
    		    }
    		}
    		/*for(Object inst:Jinst){
    			JSONObject tInst = (JSONObject) inst;
    		    String instrOpCode = (String) tInst.get("INSTROPCODE");
    		    int metID = Integer.parseInt((String) tInst.get("METHODID"));
    		    boolean isField=instrOpCode.equals("putfield") || instrOpCode.equals("getfield");
    		    boolean isMem=false;//instrOpCode.contains("aload_") || instrOpCode.contains("astore_");
    		    if (metID>-1 && (isField || isMem)){
        		    String operand = (String) tInst.get("INSTROPERAND");
        		    String operands [] = operand.split (":", 2);
        		    if(isField){
	        		    String instrOperand = operands[0];
	        		    if(instrOperand.endsWith(";"))
	        		    	instrOperand=instrOperand.substring(0, instrOperand.length()-1);
	        		    instrOperand=instrOperand.substring(instrOperand.lastIndexOf('.')+1, instrOperand.length());
	        		    String instrType = null;
	        		    if (!operands[0].equals(operand)) {
	        		    	instrType = operands[1];
	        		    }
	        		    SignatureParser sp = SignatureParser.make();
	        		    TypeSignature instParsedType = sp.parseTypeSig(instrType);
	        		    Parameter param=new Parameter(instrOperand, getNormalType(instParsedType));
	        		    if(methodList[metID]==null)
	        		    	methodList[metID]=new JMethod(metID,"name","type");
	        		    if (instrOpCode.equals("putfield")){
	        		    	methodList[metID].addModified(param, 1);
	        		    }
						else{ //instrOpCode.equals("getfield")
							methodList[metID].addUsed(param, 1);
							//System.out.println(methodList[metID]+" added used "+metID);
	        		    }
        		    } else { //isMem
        		    	/*Parameter varP = vars[Integer.parseInt(operands[1])];
        		    	if(!varP.name.equals("this"))
	        		    	if (instrOpCode.contains("aload_")){
		        		    	methodList[metID].addModified(varP, 1);
		        		    }
							else{ //instrOpCode.contains("astore_")
								methodList[metID].addUsed(varP, 1);
		        		    }
        		    }
    		    }
    		    if (metID>-1 && instrOpCode.equals("invokestatic")){
        		    String operand = (String) tInst.get("INSTROPERAND");
        		    try{
        		    	//System.out.println(operand+" op inv");
	        		    int otherMetId=Integer.parseInt(operand.substring(operand.indexOf(' ')+1,
	        		    		operand.length()));
	        		    if(otherMetId!=-1)
	        		    	invokeList[metID].add(otherMetId);
        		    } catch(NumberFormatException e){}
    		    }
    		}*/
    		/*for(int i=0;i<metSize;i++){
    			JMethod m = methodList[i];
    			if(m!=null)
        			invokeList[m.getId()].forEach(inst->{
    					Pair<Set<Parameter>, Set<Parameter>> tInst = instLists[inst];
    					tInst.getKey().forEach(mod->m.addModified(mod, 0));
    					tInst.getValue().forEach(used->m.addUsed(used, 0));
    				});
    		}*/
    		/*for(int j=0;j<metSize;j++){
        		for(int i=0;i<metSize;i++){
        			JMethod m = methodList[i];
        			if(m!=null)
        				invokeList[m.getId()].forEach(inst->{
        					JMethod invoked = methodList[inst];
        					//System.out.println("inst "+inst);
        					//System.out.println(m+" "+invoked);
        					if(invoked!=null) {
	        					invoked.getModified().forEach((mod,amount)->{
	        						//System.out.println("added"+amount);
	        						m.addModified(mod,amount);
	        					});
	        					invoked.getUsed().forEach((used,amount)->{
	        						//System.out.println("added"+amount);
	        						m.addUsed(used,amount);
	        					});
	        					//System.out.println();
        					}
        				});
        		}
    		}*/
    		for(int j=0;j<metSize;j++){
        		for(int i=0;i<metSize;i++){
        			InstLine insts = instLines[i];
        			if(insts!=null)
        				insts.setInvokes();
        		}
    		}
    		for (Object c : Jclass)
    		{    			     			
    			JSONObject cls = (JSONObject) c;
    			String clsName = (String)cls.get("CLASSNAME");
    			if(!ReaderHelper.isInteger(clsName.substring(clsName.indexOf('$')+1, clsName.length()))){
        			int	clsID = Integer.parseInt((String)cls.get("CLASSID"));
        			JClass jCls = new JClass(clsName);
        			metLists[clsID].forEach(m->{
        				if(m.getName().equals("<init>"))
        					m.setName(clsName);
        				//System.out.println(m);
        				if(!(m.getParams().isEmpty()&&m.getModified().isEmpty()&&m.getUsed().isEmpty()))
        					jCls.addMethod(m);
        				//else
        				//	System.out.println(m);
        			});
        			if(jCls.getMethods().size()!=0)
        			classes.add(jCls);
    			}
    			//System.out.println(jCls+"");
    		}
    		// Writes the extracted data to excel
		    return classes;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
	}
	private static JVariable getVarPutGet(String operand, JVariable[] vars) {
		String operands [] = operand.split(":", 2);
		String instrOperand = operands[0];
	    if(instrOperand.endsWith(";"))
	    	instrOperand=instrOperand.substring(0, instrOperand.length()-1);
	    instrOperand=instrOperand.substring(instrOperand.lastIndexOf('.')+1, instrOperand.length());
	    String instrType = null;
	    if (!operands[0].equals(operand)) {
	    	instrType = operands[1];
	    }
	    SignatureParser sp = SignatureParser.make();
	    TypeSignature instParsedType = sp.parseTypeSig(instrType);
	    return new JVariable(instrOperand, getNormalType(instParsedType), "Class");
	}
	private static JVariable getVarDotsOp(String operand, JVariable[] vars) {
		String operands [] = operand.split(":", 2);
    	int varIndex = Integer.parseInt(operands[1]);
    	JVariable varP;
    	if(varIndex!=-1)
    		varP = vars[varIndex];
    	else 
    		varP = new JVariable("", "", "Local");
    	return varP;
	}
	private static ObjectType getType(String vartype) {
		switch(vartype){
		case "Class":
			return ObjectType.FIELD;
		case "FormalIn":
			return ObjectType.PARAMETER;
		case "Local":
			return ObjectType.TEMP;
		}
		return ObjectType.RESULT;
	}
	/**
	 * Method reads from JSON files and parses.
	 * @param paramSuffix 
	 * @return
	 */
    public List<JClass>[] ReadFromFile(String paramSuffix)
    {
		classes = new List[2];
		classes[0] = readFromFile(folders[0], progs[0]);
		classes[1] = readFromFile(folders[1], progs[1]);
        return classes;
    }

    private static String getNormalType(ReturnType retType) {
    	String typeName="";
    	Reifier paramVisit = Reifier.make(CoreReflectionFactory.make(retType.getClass(), null) );
    	try{
    		retType.accept(paramVisit);
			typeName=paramVisit.getResult().getTypeName();
		} catch(TypeNotPresentException e){
			typeName=e.typeName();
		}
    	return typeName;
	}
    
    /**
    *
    * @param classes 
     * @param classes
     * @param progName 
    * @throws IOException
     * @throws InvalidFormatException 
    */
   public static void toExcel(List<JClass> classes, String folder, String name)
   {
		try
		{
			File compResFile=new File(folder+"//"+name+"_JSON.xlsx");
			runApplication.initXlFile(compResFile);
			FileInputStream file = new FileInputStream(compResFile);
			XSSFWorkbook wb = new XSSFWorkbook(file);
			XSSFSheet sheet = wb.createSheet(name+" info");
			CreationHelper createHelper = wb.getCreationHelper();
			//Creating the header for methods	    
			XSSFRow row0 = sheet.createRow((short)0);
			row0.createCell(0).setCellValue(createHelper.createRichTextString("Class:"));
			row0.createCell(1).setCellValue(createHelper.createRichTextString("Method:"));
			row0.createCell(2).setCellValue(createHelper.createRichTextString("Category:"));
			row0.createCell(3).setCellValue(createHelper.createRichTextString("Name:"));
			row0.createCell(4).setCellValue(createHelper.createRichTextString("Type:"));
			
			//Putting data inside the table
			int rowCount = 1;
			for(JClass cls:classes){
				 
				String className=cls.getClassName();
		    	 String fixedName=className.substring(className.lastIndexOf(".")+1,
		    			 className.length());
				 for (JMethod method : cls.getMethods())
				 {
					 String metName = method.getName();
					 XSSFCell cell = null;
					 XSSFRow row = sheet.createRow(rowCount++);
					 cell = row.createCell(0);
			    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
					 cell = row.createCell(1);
			    	 cell.setCellValue(createHelper.createRichTextString(metName));
			    	 cell = row.createCell(2);
			    	 cell.setCellValue(createHelper.createRichTextString("Return type"));
			    	 cell = row.createCell(3);
			    	 cell.setCellValue(createHelper.createRichTextString(metName));
			    	 cell = row.createCell(4);
			    	 cell.setCellValue(createHelper.createRichTextString(method.getReturnType()));
			    	 //Parameters output
					 for (Parameter par : method.getParams())
					 {
						 row = sheet.createRow(rowCount++);
						 cell = row.createCell(0);
				    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
						 cell = row.createCell(1);
				    	 cell.setCellValue(createHelper.createRichTextString(metName));
				    	 cell = row.createCell(2);
				    	 cell.setCellValue(createHelper.createRichTextString("Parameter"));
				    	 cell = row.createCell(3);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getName()));
				    	 cell = row.createCell(4);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getType()));
					 }
					 //getfield output
					 for (Entry<Parameter, Integer> par : method.getUsed().entrySet())
					 {
						 row = sheet.createRow(rowCount++);
						 cell = row.createCell(0);
				    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
						 cell = row.createCell(1);
				    	 cell.setCellValue(createHelper.createRichTextString(metName));
				    	 cell = row.createCell(2);
				    	 cell.setCellValue(createHelper.createRichTextString("Att. Used "+par.getValue()));
				    	 cell = row.createCell(3);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getKey().getName()));
				    	 cell = row.createCell(4);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getKey().getType()));
					 }
					 //putfield output
					 for (Entry<Parameter, Integer> par : method.getModified().entrySet())
					 {
						 row = sheet.createRow(rowCount++);
						 cell = row.createCell(0);
				    	 cell.setCellValue(createHelper.createRichTextString(fixedName));
						 cell = row.createCell(1);
				    	 cell.setCellValue(createHelper.createRichTextString(metName));
				    	 cell = row.createCell(2);
				    	 cell.setCellValue(createHelper.createRichTextString("Att. Mod. "+par.getValue()));
				    	 cell = row.createCell(3);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getKey().getName()));
				    	 cell = row.createCell(4);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getKey().getType()));
					 }
				 }
				 short cols = sheet.getRow(0).getLastCellNum();
				 // Cosmetics
				 for(int i=0;i<cols;i++)
					 sheet.autoSizeColumn(i);
			}
			file.close();
			// Write the output to a file.
			FileOutputStream fileOut = new FileOutputStream(compResFile);
			wb.write(fileOut);
			fileOut.close();
			wb.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
   }

	
}
