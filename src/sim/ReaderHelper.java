package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

public class ReaderHelper {
	public static String primitiveToObject(String prim){
		int dimCounter=0;
		while(prim.endsWith("[]")){
			dimCounter++;
			prim=prim.substring(0, prim.length()-2);
		}
		prim=primTypeToObject(prim);
		for(int i=0;i<dimCounter;i++)
			prim+="[]";
		return prim;
	}
	private static String primTypeToObject(String prim){
		switch(prim){
		case "byte":
			return "java.lang.Byte";
		case "short":
			return "java.lang.Short";
		case "int":
			return "java.lang.Integer";
		case "long":
			return "java.lang.Long";
		case "float":
			return "java.lang.Float";
		case "double":
			return "java.lang.Double";
		case "char":
			return "java.lang.Character";
		case "boolean":
			return "java.lang.Boolean";
		case "void":
			return "java.lang.Void";
		case "enum":
			return "java.lang.Enum";
		default:
			return prim;
		}	
	}
	public static boolean isInteger(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch(NumberFormatException e) {
	        return false;
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	public static <K> List<Entry<K, K>> dominoSort(Collection<Entry<K, K>> dominos) {
        //System.out.println(dominos);
        List<Entry<K, K>> sortedDominos=new LinkedList<>();
        sortedDominos.addAll(dominos);
        sortedDominos.sort((a,b)->{
			if(a.getKey().equals(b.getValue()))
				return 1;
			if(b.getKey().equals(a.getValue()))
				return -1;
			return 0;
		});  
        //Collections.reverse(sortedDominos);
		//System.out.println(sortedDominos);
    	/*Set<K> sortedDominos=new LinkedHashSet<>();
    	//System.out.println(l);
    	Collections.reverse(l);
    	//add by inheritance order
    	l.forEach(a->sortedDominos.add(a.getValue()));  
    	//add unbound descendants
    	l.forEach(a->{
    		if(!sortedDominos.contains(a.getKey()))
    			sortedDominos.add(a.getKey());
    	});  */
        return sortedDominos;
	}
}
