package sim;

public class Counter{
	  private double count;
	  public Counter(){
		  count = 0;
	  }
	  public Counter(double c){
		  count=c;
	  }
	  public void set(double n){
		    synchronized(this){
		      count=n;
		    }
		  }
	  public double inc(){
	    synchronized(this){
	      return ++count;
	    }
	  }
	  public double dec(){
		    synchronized(this){
		      return --count;
		}
	  }
	  public double get(){
		    synchronized(this){
		      return count;
		}
	  }
	public double inc(double size) {
		synchronized(this){
			count+=size;
			return count;
		}
	}
}