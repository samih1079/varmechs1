package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MiniJClass{
	private String className;
	private Set<MiniJMethod> methods=new HashSet<>();
	public MiniJClass(String className) {
		this.className=className;
	}
	public String getClassName() {
		return className;
	}
	public Set<MiniJMethod> getMethods() {
		return Collections.unmodifiableSet(methods);
	}
	public void addMethods(MiniJMethod method) {
		this.methods.add(method);
	}
	public void addAllMethods(Collection<MiniJMethod> methods) {
		this.methods.addAll(methods);
	}
	public String toString(){
		return className+" : "+methods;
	}
}
