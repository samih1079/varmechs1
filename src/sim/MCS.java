package sim;

import java.io.*;
import java.sql.Connection;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import entities.TempMCSSimilarityEntity;
import entities.TempMcsSimilarityHandler;
import javafx.util.Pair;
import semilar.data.Sentence;
import semilar.sentencemetrics.CorleyMihalceaComparer;
import semilar.tools.preprocessing.SentencePreprocessor;


// use this   umbs ,lsa,mcs,mong
/**
 *
 * @author Lenovo1
 */
public class MCS implements SimilarityInterface{
	private static Map<Pair<String, String>, Double> resMap=new HashMap<>();
	public static void main(String... args) throws InterruptedException{
		MCS mcs = new MCS();
		Scanner sc = new Scanner(System.in);
		String s1=sc.nextLine();
		String s2;
		while(s1.length()>0){
			s2=sc.nextLine();
			System.out.println(mcs.calculateSimilarity(s1, s2, null));
			s1=sc.nextLine();			
		}
		sc.close();
		SentencePreprocessor preprocessor =
	 			new SentencePreprocessor(SentencePreprocessor.TokenizerType.STANFORD,
	 			SentencePreprocessor.TaggerType.STANFORD,
	 			SentencePreprocessor.StemmerType.PORTER,
	 			SentencePreprocessor.ParserType.STANFORD);
     	CorleyMihalceaComparer cmComparer = new CorleyMihalceaComparer(0.3f, false, "NONE", "par");
     	
    	Sentence sentence1 =preprocessor.preprocessSentence("a");
 		Sentence sentence2 =preprocessor.preprocessSentence("a");
		try{    	    
        	cmComparer.computeSimilarity(sentence1, sentence2);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		} 
		
		
		
		long timeBegin=GregorianCalendar.getInstance().getTimeInMillis();
		System.out.println(timeBegin+" **********");
		List<Thread> l=new LinkedList<>();
		for(int i=0;i<10000;i++){
			Thread t = new Thread(){
				public void run(){
					String s1=Math.random()*10000000+"";
					String s2=Math.random()*10000000+"";
					//mcs.calculateSimilarity(s1, s2, null);
					System.out.println(mcs.calculateSimilarity(s1, s2, null));
				}
			};
			l.add(t);
		}
		for(Thread t:l){
			t.start();
			//t.join();
		}
		for(Thread t:l){
			t.join();
		}
		System.out.println("end "+(GregorianCalendar.getInstance().getTimeInMillis()-timeBegin));
		//10000 end 20922/25310/26030 with threads
		//10000 end 24348/23530/23574 without
		//1000 end 5513/4912/3753 with threads
		//1000 end 5198/5257/5152 without
		//100 end 2157/2065/1992 with threads
		//100 end 2529/2441/2567 without
		//now with setting up before
		////10000 end 14567/14743 with threads
		//10000 end 22425/22725 without
		//1000 end 2910/2978/2923 with threads
		//1000 end 4000/4063 without
		//100 end 750/809/735 with threads
		//100 end 1128/1133/1103 without
		
	}
    
     
    public double calculateSimilarity(String trim, String trim2, Connection conn) {
	    trim=trim.replace("\"", "");
	    trim2=trim2.replace("\"", "");
	    trim=trim.replaceAll("[A]-[Z]", " ");
	    String[] temp1=trim.split("(?=\\p{Upper})");
	    String[] temp2=trim2.split("(?=\\p{Upper})");
	    trim=trim2="";
	    for(int z=0;z<temp1.length;z++)
	    {
	    	trim+=temp1[z].toString()+" ";
	    }
	    for(int z=0;z<temp2.length;z++)
	    {
	    	trim2+=temp2[z].toString()+" ";
	    }

		String ftrim1;
		String ftrim2;      
		if (trim==null|| trim.length()<2 || trim2==null || trim2.length()<2)
			return 0.0;           
		Float sim = (float) 0;
		ftrim1 = trim.toLowerCase().replaceAll("`","'");
		ftrim2 = trim2.toLowerCase().replaceAll("`","'");
		
		Pair<String, String> pair=new Pair<>(ftrim1, ftrim2);
		Double res=resMap.get(pair);
		if(res!=null){
			return res;
		}
		
		TempMCSSimilarityEntity retriveSim = TempMcsSimilarityHandler.getSimByterm(trim.toLowerCase(), trim2.toLowerCase(), conn);
     
		if (retriveSim!=null) {
			sim = retriveSim.getMCSSimilarity();
			resMap.put(pair, new Double(sim));
			return sim;
		}
		 //ConfigManager.setSemilarDataRootFolder("C://SEMILARhome/");
		 //ConfigManager.setSemilarDataRootFolder("C://Users/Nili/workspace/.metadata/.plugins/org.eclipse.ltk.core.refactoring/.refactorings/");
          //       ConfigManager.setSemilarDataRootFolder("C://UFromDeskTop/WorkSpaces/workspace/.metadata/.plugins/org.eclipse.ltk.core.refactoring/.refactorings/");
	//	 SentencePreprocessor preprocessor = new SentencePreprocessor(SentencePreprocessor.TokenizerType.STANFORD, SentencePreprocessor.TaggerType.STANFORD, SentencePreprocessor.StemmerType.PORTER, SentencePreprocessor.ParserType.STANFORD);
	//	 Sentence a=new Sentence(ftrim);
		
    	SentencePreprocessor preprocessor =
 			new SentencePreprocessor(SentencePreprocessor.TokenizerType.STANFORD,
 			SentencePreprocessor.TaggerType.STANFORD,
 			SentencePreprocessor.StemmerType.PORTER,
 			SentencePreprocessor.ParserType.STANFORD);
     	CorleyMihalceaComparer cmComparer = new CorleyMihalceaComparer(0.3f, false, "NONE", "par");
     	
    	Sentence sentence1 =preprocessor.preprocessSentence(ftrim1);
 		Sentence sentence2 =preprocessor.preprocessSentence(ftrim2);
		try{    	    
        	sim = cmComparer.computeSimilarity(sentence1, sentence2);
        	//System.out.println(sentence1.getRawForm()+" "+sentence2.getRawForm()+" "+sim);
        	if (sim.isNaN())
        		sim=0f;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			sim=0f;
			resMap.put(pair, new Double(sim));
			return sim;
		} 
         
         TempMcsSimilarityHandler.insertRequirement(trim.toLowerCase(), trim2.toLowerCase(), sim, conn);
                 
         resMap.put(pair, new Double(sim));
	     return sim;
	}
}
