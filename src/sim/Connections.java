package sim;

public class Connections {

	String className;
	String multi;


	public Connections(String name,String mult)
	{
		setClassName(name);
		setMulti(mult);
	}


	@Override
	public String toString() {
		return "Connections [className=" + className + ", multi=" + multi + "]";
	}


	public String getClassName() {
		return className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public String getMulti() {
		return multi;
	}


	public void setMulti(String multi) {
		this.multi = multi;
	}
}
