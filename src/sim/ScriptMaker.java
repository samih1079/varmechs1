package sim;

import java.io.*;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
public class ScriptMaker {
	private final static String visP ="13.2";
	private Map<String, Double> validVer;
	private Map<String, SimpleEntry<String,Double>> scriptMade;
	private Semaphore jsonSem=new Semaphore(1, true);
	public static void main(String[] args){
		String ragex;
		if(args.length>1)
			ragex=args[0];
		else
			return;
		preProccessProject(ragex, Arrays.copyOfRange(args, 1, args.length));
	}
	
	public static void preProccessProject(String ragex, String... projectsDir){
		for(String projectDir : projectsDir){
			List<String> vers=new LinkedList<>();
			ragexSearcher(new File(projectDir), vers, ragex);
			new ScriptMaker(projectDir, vers);
		}
	}
	
	//gets a dir and a list
	//puts the ragex matching file paths in the list
	private static void ragexSearcher(File dir, List<String> found, String ragex)
	{
		if (dir.isDirectory()) 
		{
			if(dir.getName().matches(ragex))
			{
				found.add(dir.getAbsolutePath());
				return;
			}
			File[] examples = dir.listFiles();
		    for (File project : examples) //recursively search for bin folder
		    	ragexSearcher(project, found, ragex);
		} 			
	}
	
	public ScriptMaker(String projectDir, List<String> vers){
		this.scriptMade=new LinkedHashMap<String, SimpleEntry<String,Double>>();
		this.validVer=new LinkedHashMap<String, Double>();
		vers.forEach(a->preprccess(a,projectDir,validVer));
		validVer.forEach((a,b)->validVer.forEach((c,d)->{
			if(!a.equals(c))
			{
				script(a,c, b*d, projectDir);
			}
		}));	
	}
	
	private void script(String a, String b, double size, String dir){
		try {
			String prjctA=getPrgName(a);
			String prjctB=getPrgName(b);
			String a_b=""; //a and b lexicographically ordered
			if(a.compareTo(b)>0)
				a_b=prjctA+"_"+prjctB;
			else
				a_b=prjctB+"_"+prjctA;
			String batName=a_b+".bat";
			File scriptsDir = new File(dir + "\\scripts\\");
			if(!scriptsDir.exists())
				scriptsDir.mkdirs();
			String fullBatPath=dir+"\\scripts\\"+batName;
			if(scriptMade.put(a_b, new SimpleEntry<String,Double>(fullBatPath,size)) != null) //check if were made already
				return;
			//PrintStream printer = new PrintStream(dir.substring(0, dir.lastIndexOf('\\'))+"\\scripts\\"+batName);
			PrintStream printer = new PrintStream(fullBatPath);//bat for 2
			PrintStream printer2 = new PrintStream(new FileOutputStream(dir+"\\scripts\\bigScript.bat", true));//bat for project
			
			String batStr="";
			batStr+="cd C:\\Users\\iris\\Desktop\\VarMech\\"+"\n";
			batStr+="java -jar -Xmx16g varmech.jar ";
			batStr+='"'+a+'"'+" \""+getPrgName(a)+"\" "+'"'+b+'"'+" \""+getPrgName(b)+"\" "+" MCS"+"\n";
			batStr+="set errA=%errorlevel%"+"\n";
			batStr+="cd \"C:\\Program Files\\Auto Mailer\""+"\n";
			batStr+="printAndMail.exe a "+a_b+" "+"%errA%"+"\n";
			printer.print(batStr);
			printer2.print(batStr);	
			printer.close();
			printer2.close();
			/*PrintStream printer3 = new PrintStream(new FileOutputStream(dir+"\\scripts\\"+getPrgName(dir)+".txt", true));//txt to import to varmech
			printer3.println();
			printer3.close();*/
		//	Runtime.getRuntime().exec("cmd /c start /B /D "+'"'+printer+'"');
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	private void preprccess(String a,String dir,Map<String, Double> validVer2) {
		try {
			a=a.replace("/", "\\");
			makeBat(a,dir);
			String prgName=getPrgName(a);
		     //a=a.substring(0, a.length()-"bin".length());//removes bin
			File jsnFile = new File(a+"\\Json");
			File xmiFile = new File(a+"\\"+prgName+".xmi");
			//System.out.println("jsn file for "+prgName+": "+jsnFile);
			if(xmiFile.exists()&&jsnFile.exists()&&jsnFile.isDirectory()&&jsnFile.listFiles().length>0)
				validVer2.put(a,(((double)xmiFile.length())/1024/1024));
		} catch (IOException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private void makeBat(String a,String dir) throws IOException, InterruptedException{
		String dbName = a.replace("/", "_").replace("\\", "_").replace(":", "").replace("?", "")
				.replace("<", "").replace(">", "").replace("|", "").replace("*", "").replace("\"", "");
		String prgName=getPrgName(a);
		System.out.println(prgName);
	//	String NoBin=a.substring(0, a.length()-"bin".length());//removes bin
		String NoBin=a+"\\";
		File xmiFile = new File(NoBin+prgName+".xmi");
		Process xmiP=null;
		if(!xmiFile.exists()){
			String xmiBatName="XmiTemp"+".bat";
			PrintStream xmiPrinter = new PrintStream(dir+"\\"+xmiBatName);
			xmiPrinter.println("cd C:\\Program Files\\Visual Paradigm "+visP+"\\scripts\\");	
			File vppFile = new File(NoBin+prgName+".vpp");
			if(!vppFile.exists())
				xmiPrinter.println("call InstantReverse -project "+'"'+vppFile+'"'+" -path "+'"'+a+'"'+
						" -lang Java -pathtype folder -sourcetype class -overwrite");
			if(!xmiFile.exists())
				xmiPrinter.println("call ExportXMI -project "+'"'+vppFile+'"'+" -out "+'"'+xmiFile+'"'+" -type 2.1");
			xmiPrinter.println("EXIT");
			xmiPrinter.close();
			xmiP=Runtime.getRuntime().exec("cmd /c "+'"'+dir+"\\"+xmiBatName+'"'); //runs the xmi preprocessing
		}
		
		File jsnFile = new File(NoBin+"Json");
		if(!jsnFile.exists()||!jsnFile.isDirectory()||jsnFile.listFiles().length<1)
		{
			jsonSem.acquire();
			String jsnBatName="JsnTemp"+".bat";
			PrintStream jsnPrinter = new PrintStream(dir+"\\"+jsnBatName);
			jsnPrinter.print("java -jar C:/Varmer/javapdg.jar -mt 1 -da jdbc:derby://localhost:1527/db/"+dbName+"_db -pc \""+a+"\"");
			jsnPrinter.println(" >> "+'"'+NoBin+prgName+"log.txt"+'"'+" 2>&1");
			jsnPrinter.print("java -jar C:/Varmer/javapdg.jar -mt 3 -da jdbc:derby://localhost:1527/db/"+dbName+"_db -js ");
			jsnPrinter.println('"'+NoBin+"Json"+'"'+" >> "+'"'+NoBin+prgName+"log.txt"+'"'+" 2>&1");
			jsnPrinter.println("EXIT");
			jsnPrinter.close();
			Process jsnP=Runtime.getRuntime().exec("cmd /c "+'"'+dir+"\\"+jsnBatName+'"'); //runs the preprocessing
			jsnP.waitFor();
			jsonSem.release();
		}		
		if(xmiP!=null)
			xmiP.waitFor();
		Thread.sleep(10);
	}
	//gets a string, return the string between the last 2 '\' without '.'
	private String getPrgName(String a) {
		int lastBack=a.lastIndexOf('\\');
		if(lastBack<0) //if no '\'
			return null;
		a=a.substring(lastBack+1);
		return a;
	/*	lastBack=a.lastIndexOf('\\');
		if(lastBack<0) //if no '\'
			return null;
		String b=a.substring(0, lastBack);
		a=a.substring(lastBack+1, a.length());
		lastBack=b.lastIndexOf('\\');
		b=b.substring(lastBack+1, b.length());
		a=b;
		a=a.replace(".", "");
		a=a.replace(" ", "");
		return a; */
	}
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
	    return map.entrySet()
	              .stream()
	              .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
	              .collect(Collectors.toMap(
	                Map.Entry::getKey, 
	                Map.Entry::getValue, 
	                (e1, e2) -> e1, 
	                LinkedHashMap::new
	              ));
	}
	public Map<String, SimpleEntry<String, Double>> getMade(){
		return scriptMade;
	}
	public List<SimpleEntry<String,Double>> getMade2(){
		List<SimpleEntry<String,Double>> l = new LinkedList<SimpleEntry<String,Double>>();
		scriptMade.forEach((a,b)->l.add(b));
		return l;
	}
	
}

