package sim;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Deep {

	// String programName;
	// String className;
	private Set<Parameter> att_used;
	private Set<Parameter> att_mod;
	private String opName;
	private String returnType;
	private List<String> sigParam = new LinkedList<String>();

	@Override
	public String toString() {
		return "Operation [att.-used=" + att_used + ",\t operation-name=" + opName + ",\t att.-mod=" + att_mod  + "]\n";
	}


	// -----------------------------------------Constructors---------------------------------------------------

	public Deep() {
		att_used = new HashSet<Parameter>();
		att_mod = new HashSet<Parameter>();
		opName = "";
		returnType = "";
	}

	// ------------------------------------Getters & Setters--------------------------------------------------

	/* public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getprogramName() {
		return programName;
	}

	public void setprogramName(String projectName) {
		this.programName = projectName;
	} */


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deep other = (Deep) obj;
		if (opName == null) {
			if (other.opName != null)
				return false;
		} else if (!opName.equals(other.opName))
			return false;
		if (att_used == null) {
			if (other.att_used != null)
				return false;
		} else if (!att_used.equals(other.att_used))
			return false;
		if (att_mod == null) {
			if (other.att_mod != null)
				return false;
		} else if (!att_mod.equals(other.att_mod))
			return false;
		return true;
	}

	public Set<Parameter> getUsed() {
		return att_used;
	}

	public void setUsed(Set<Parameter> att_used) {
		this.att_used = att_used;
	}

	public Set<Parameter> getModified() {
		return att_mod;
	}

	public void setModified(Set<Parameter> att_mod) {
		this.att_mod = att_mod;
	}

	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName;
	}


	public String getReturnType() {
		return returnType;
	}


	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}


	public List<String> getSigParam() {
		return sigParam;
	}


	public void setSigParam(List<String> sigParam) {
		this.sigParam = sigParam;
	}
}
