package sim;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JMethod {
	private VisabilityEnum visab=VisabilityEnum.PUBLIC; //public as default
	private int id;
	private String name;
	private String returnType;
	private Map<Parameter, Integer> attMod;
	private Map<Parameter, Integer>  attUsed;
	private Set<JPointer<Parameter>> usedParams=new LinkedHashSet<>();
	private Set<Parameter> params=new LinkedHashSet<>();
	private Parameter hiddenParam=null;
	private int clsID;
	/*public JMethod(int id, String name, String returnType, Set<Parameter> attMod, Set<Parameter> attUsed) {
		this.id=id;
		//System.out.println("id: "+id);
		this.name=name;
		this.returnType=returnType;
		this.attMod=attMod;
		this.attUsed=attUsed;	
	}*/
	public JMethod(int clsID, int metID, String name, String returnType) {
		this.clsID=clsID;
		this.id=metID;
		//System.out.println("id: "+id);
		this.name=name;
		this.returnType=returnType;
		this.attMod=new LinkedHashMap<>();
		this.attUsed=new LinkedHashMap<>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReturnType() {
		return returnType;
	}
	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	public Map<Parameter, Integer> getUsed() {
		return Collections.unmodifiableMap(attUsed);
	}
	public Map<Parameter, Integer> getModified() {
		return Collections.unmodifiableMap(attMod);
	}
	public Set<Parameter> getParams() {
		return Collections.unmodifiableSet(params);
	}
	public VisabilityEnum getVisab() {
		return visab;
	}
	public void setVisab(VisabilityEnum visab) {
		this.visab = visab;
	}
	public void addParams(Parameter param) {
		this.params.add(param);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getClsID() {
		return clsID;
	}
	public void setClsID(int clsID) {
		this.clsID = clsID;
	}
	public Parameter getHiddenParam() {
		return hiddenParam;
	}
	public void setHiddenParam(Parameter hiddenParam) {
		this.hiddenParam = hiddenParam;
	}
	public void addModified(Parameter mod, Integer amount) {
		JPointer<Parameter> modPointer = new JPointer<Parameter>(mod);
		if(usedParams.contains(modPointer))
			return;
		usedParams.add(modPointer);
		Integer modedAmount = attMod.get(mod);
		if(modedAmount==null)
			attMod.put(mod, amount);
		else
			attMod.put(mod, 0+amount);
	}
	public void addUsed(Parameter used, Integer amount) {
		JPointer<Parameter> usedPointer = new JPointer<Parameter>(used);
		if(usedParams.contains(usedPointer))
			return;
		usedParams.add(usedPointer);
		Integer usedAmount = attUsed.get(used);
		if(usedAmount==null)
			attUsed.put(used, amount);
		else
			attUsed.put(used, 0+amount);		
	}
	public boolean equals(Object other){
		if(other instanceof JMethod){
			if(other==this)
				return true;
			JMethod otherM = (JMethod)other;
			if(visab.equals(otherM.visab) && id==otherM.id)
				if(name.equals(otherM.name) && returnType.equals(otherM.returnType))
					if(params.equals(otherM.params) && usedParams.equals(otherM.usedParams))
						if(attMod.equals(otherM.attMod) && attUsed.equals(otherM.attUsed))
							return true;
		}
		return false;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		result = prime * result + ((attMod == null) ? 0 : attMod.hashCode());
		result = prime * result + ((attUsed == null) ? 0 : attUsed.hashCode());
		result = prime * result + ((usedParams == null) ? 0 : usedParams.hashCode());
		result = prime * result + ((params == null) ? 0 : params.hashCode());
		return result;
	}
	public String toString(){
		return name+"( "+params+" ) "+returnType+" "+attUsed+" "+attMod;
	}
	public void primToObject() {
		returnType=ReaderHelper.primitiveToObject(returnType);
		params.forEach(p->p.type=ReaderHelper.primitiveToObject(p.type));
		attMod.forEach((p,i)->p.type=ReaderHelper.primitiveToObject(p.type));
		attUsed.forEach((p,i)->p.type=ReaderHelper.primitiveToObject(p.type));
	}
}
