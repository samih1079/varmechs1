package sim;

public class ExtParam extends Parameter{
	final private JMethod method;
	final private boolean isUsed;
	public ExtParam(Parameter param, JMethod method, boolean isUsed){
		super(param.name, param.type);
		this.method=method;
		this.isUsed=isUsed;
	}
	public JMethod getMethod() {
		return method;
	}
	public boolean isUsed() {
		return isUsed;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((isUsed) ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ExtParam)
			if (this.getName().equals(((ExtParam) obj).getName()))
				if (this.getType().equals(((ExtParam) obj).getType()))
					if (this.method.equals(((ExtParam) obj).method))
						return this.isUsed==((ExtParam) obj).isUsed;
		return false;
	}
	/*public String toString() {
		return "("+name + "|" + type+"|"+method+"|"+isUsed+")";
	}*/
}
