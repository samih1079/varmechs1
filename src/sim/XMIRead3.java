package sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

/**
 * @author Iris - replaces XMIReadFromFolder
 */

public class XMIRead3 {

	final static String ATTRIBUTES = "ownedAttribute";
	final static String METHODS = "ownedOperation";
	final static String CLASSES = "ownedMember";
	final static String PARAMETERS = "ownedParameter";

	final static String COUPLINGS = "UML:Associationend";
	final static String CLASS_INHERITANCES = "UML:GeneralizableElement.generalization";

	String folders[];
	String progs[];
	ArrayList<XMIStruct> [] classes;
	/* String temp;
	String prog1name;
	String prog2name;
	public static File fileUse; */

	public XMIRead3(String folder1, String folder2, String prog1name, String prog2name)
	{
		super();
		this.folders= new String[2];
		this.folders[0] = folder1;
		this.folders[1] = folder2;
		this.progs=new String[2];
		this.progs[0]=prog1name;
		this.progs[1]=prog2name;
		this.classes=null;
	}

	public ArrayList[] ReadFromFile()
	{
		classes = new ArrayList [2];
		try
		{
	        //File folder = new File(folderPath + "\\");	//program 1
	        for (int w=0; w<2 ; w++)					// for each application
	        {
	    		classes [w] = new ArrayList<XMIStruct>();

	    		String tempFolderStr=this.folders[0];
	    		if (w>0)
	    			tempFolderStr=this.folders[1];
	    		File tempFolder=new File(tempFolderStr);
		        File[] listOfFiles = tempFolder.listFiles();
		        if (listOfFiles != null)
		        {
			        int size = listOfFiles.length;
			        for (int i = 0; i < size ; i++)
			        {
				        File file = listOfFiles[i];
				        if ((file.isFile()) && file.getName().contains(".xmi"))
				        {
					        System.out.println("FILE READ: "+ file.getName() + "\n");
					        try {
					        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					        	Document doc = dBuilder.parse(file);

					        	//optional, but recommended - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
					        	doc.getDocumentElement().normalize();

					        	NodeList listClass;
					        	NodeList listAttributes;
					        	NodeList listMethods;
					        	NodeList listParameters;

					        	// retrieve classes
					        	listClass = doc.getElementsByTagName(CLASSES);
					            for (int j = 0; j < listClass.getLength(); j++) {
					            	Element currClass = (Element) listClass.item(j);
					            	String typeClass = currClass.getAttribute("xmi:type");
					            	if ((typeClass != null) && (typeClass.equals ("uml:Class"))) {
					            		XMIStruct xmiC = new XMIStruct (progs[w], currClass.getAttribute("name"), currClass.getAttribute("xmi:id"));
					            		// retrieve attributes
					            		listAttributes = currClass.getElementsByTagName(ATTRIBUTES);
							            for (int k = 0; k < listAttributes.getLength(); k++) {
							            	Element currAtt = (Element) listAttributes.item(k);
							            	String typeAtt = currAtt.getAttribute("xmi:type");
							            	if ((typeAtt !=null) && (typeAtt.equals ("uml:Property")) && !currAtt.getAttribute("name").contains("this$")) { // this$ - avoid subclasses ???
							            		Attribute att = new Attribute (currAtt.getAttribute("name"), currAtt.getAttribute("type"), currAtt.getAttribute("visibility"));
							            		xmiC.addAttribute(att);
							            	}
							            }
							            // retrieve methods
							            listMethods = currClass.getElementsByTagName(METHODS);
							            for (int k = 0; k < listMethods.getLength(); k++) {
							            	Element currMethod = (Element) listMethods.item(k);
							            	String typeMethod = currMethod.getAttribute("xmi:type");
							            	if ((typeMethod !=null) && (typeMethod.equals ("uml:Operation")) && !currMethod.getAttribute("name").contains("access$") && !isInteger(currMethod.getAttribute("name"))) { // access$ - avoid subclasses
							            		Shallow mthd = new Shallow(currMethod.getAttribute("name"), "void", VisabilityEnum.valueOf(currMethod.getAttribute("visibility").toUpperCase()));
							            		NodeList params = currMethod.getElementsByTagName(PARAMETERS);		// retrieve the parameters of the method
							            		for (int l = 0; l < params.getLength(); l++) {
							            			Element currParam = (Element) params.item(l);
							            			if ((currParam.getAttribute("xmi:type") !=null) && (currParam.getAttribute("xmi:type").equals ("uml:Parameter"))) {
							            				if (currParam.getAttribute("kind").equals ("return"))
							            					mthd.setReturnType(currParam.getAttribute("type"));
							            				else
							            					mthd.addParameter(new Parameter (currParam.getAttribute("name"), currParam.getAttribute("type")));
							            			}
							            		}
							            		xmiC.addMethod(mthd);
							            	}
							            }
							            classes[w].add(xmiC);
					            }
					          }

					          // replace type codes
					          for (XMIStruct xmiC:classes[w]) {
					          	ArrayList<Attribute> xmiAs = new ArrayList<Attribute>(xmiC.getPara());
					           	for (int ca=0; ca<xmiAs.size(); ca++) {
					           		Attribute xmiA=xmiAs.get(ca);
					                String newType = findActType(xmiA.getType(), classes[w]);
					           		if (!newType.equals(xmiA.getType())) {
					           			xmiC.removeAttribute(xmiA);
					           			Attribute newXmiA = new Attribute (xmiA.getName(), newType, xmiA.getVisibility());
					           			xmiC.addAttribute(newXmiA);
					           		}
					           	}
					           	ArrayList<Shallow> xmiMs = new ArrayList<Shallow>(xmiC.getMethods());
					           	for (int cm=0; cm<xmiMs.size(); cm++) {
					           		Shallow xmiM=xmiMs.get(cm);
					           		String returnType = findActType(xmiM.getReturnType(), classes[w]);
					           		if (!returnType.equals(xmiM.getReturnType())) {
					           			xmiC.removeMethod(xmiM);
					           			xmiM.setReturnType(returnType);
					           			xmiC.addMethod(xmiM);
					           		}
					           		ArrayList<Parameter> xmiPs = new ArrayList<Parameter>(xmiM.getPara());
						           	for (int cp=0; cp<xmiPs.size(); cp++) {
						           		Parameter xmiP=xmiPs.get(cp);
						           		String newType = findActType(xmiP.getType(), classes[w]);
						           		if (!newType.equals(xmiP.getType())) {
						           			xmiC.removeMethod(xmiM);
						           			xmiM.removeParameter(xmiP);
						           			xmiP.setType(newType);
						           			xmiM.addParameter(xmiP);
						           			xmiC.addMethod(xmiM);
						           		}
					           		}
					           	}
					          }

					    } catch (SAXParseException err) {
							System.out.println("** Parsing error" + ", line "
									+ err.getLineNumber() + ", uri " + err.getSystemId());
							System.out.println(" " + err.getMessage());
						} catch (SAXException e) {
							Exception x = e.getException();
							((x == null) ? e : x).printStackTrace();
						} catch (Throwable t) {
							t.printStackTrace();
						}

						    toExcel(classes[w], progs[w], folders[w]);
			        	}
					}
		        }
	        }
	      return classes;
		}
		 catch (Exception e)
		{
			 e.printStackTrace();
		}
		return null;
	}

	public static boolean isInteger(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch(NumberFormatException e) {
	        return false;
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}

   public static String findActType (String typeCode, ArrayList<XMIStruct> array) {
	   String result="";
	   if (typeCode.endsWith("_id")) {
		   result=typeCode.substring(0, typeCode.length()-3);
		   return result;
	   }  
	 //  if (typeCode.contains(".")){
		  for (XMIStruct it:array) {
           		if (it.getXmiID().equals(typeCode)) {
           			result = it.getName();
           			break;
           		}
           	}
	 //  }
	   return result;
   }

	/**
     *
     * @param classes
     * @throws IOException
     */
    public static void toExcel(ArrayList<XMIStruct> classes, String progName, String folderPath) throws IOException
    {
		try
		{
			 XSSFWorkbook wb = new XSSFWorkbook();
			 XSSFSheet sheet = wb.createSheet("XMI Table");
			 CreationHelper createHelper = wb.getCreationHelper();

			 // Cosmetics
			 XSSFFont defaultFont= wb.createFont();
		    defaultFont.setFontHeightInPoints((short)10);
		    defaultFont.setFontName("Arial");
		    defaultFont.setBold(true);

		    XSSFFont font= wb.createFont();
		    font.setFontHeightInPoints((short)10);
		    font.setFontName("Arial");
		    font.setBold(true);
		    font.setItalic(false);

		    CellStyle style = wb.createCellStyle();
		    style.setAlignment(CellStyle.ALIGN_CENTER);
		    style.setFont(font);

			 // Creating the header for methods	    
			 XSSFRow row0 = sheet.createRow((short)0);
			 row0.createCell(0).setCellValue(createHelper.createRichTextString("Class:"));
			 row0.createCell(1).setCellValue(createHelper.createRichTextString("Category:"));
			 row0.createCell(2).setCellValue(createHelper.createRichTextString("Element Name:"));
			 row0.createCell(3).setCellValue(createHelper.createRichTextString("Parent Name:"));
			 row0.createCell(4).setCellValue(createHelper.createRichTextString("Type:"));
			 row0.createCell(5).setCellValue(createHelper.createRichTextString("Project:"));

			 // Putting data inside the table
			 int rowCount = 1;

			 for (XMIStruct ds : classes)
			 {
				 XSSFCell cell = null;
				 XSSFRow row = sheet.createRow(rowCount++);
		    	 // Attributes output
				 for (Attribute att : ds.getPara())
				 {
					 row = sheet.createRow(rowCount++);
			    	 cell = row.createCell(0);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getName()));
			    	 cell.setCellStyle(style);
			    	 cell = row.createCell(1);
			    	 cell.setCellValue(createHelper.createRichTextString("Attribute"));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(2);
			    	 cell.setCellValue(createHelper.createRichTextString(att.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(3);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(4);
			    	 cell.setCellValue(createHelper.createRichTextString(att.getType()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(5);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
					 cell.setCellStyle(style);
				 }
		    	 // Methods output
				 for (Shallow met : ds.getMethods())
				 {
					 row = sheet.createRow(rowCount++);
					 cell = row.createCell(0);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getName()));
			    	 cell.setCellStyle(style);
			    	 cell = row.createCell(1);
			    	 cell.setCellValue(createHelper.createRichTextString("Operation"));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(2);
			    	 cell.setCellValue(createHelper.createRichTextString(met.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(3);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getName()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(4);
			    	 cell.setCellValue(createHelper.createRichTextString(met.getReturnType()));
					 cell.setCellStyle(style);
			    	 cell = row.createCell(5);
			    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
					 cell.setCellStyle(style);

			    	 // Parameters output
					 for (Parameter par : met.getPara())
					 {
						 row = sheet.createRow(rowCount++);
						 cell = row.createCell(0);
				    	 cell.setCellValue(createHelper.createRichTextString(ds.getName()));
				    	 cell.setCellStyle(style);
				    	 cell = row.createCell(1);
				    	 cell.setCellValue(createHelper.createRichTextString("Parameter"));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(2);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getName()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(3);
				    	 cell.setCellValue(createHelper.createRichTextString(met.getName()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(4);
				    	 cell.setCellValue(createHelper.createRichTextString(par.getType()));
						 cell.setCellStyle(style);
				    	 cell = row.createCell(5);
				    	 cell.setCellValue(createHelper.createRichTextString(ds.getProjectName()));
						 cell.setCellStyle(style);
					 }
				 }
			 }
			 // Cosmetics
			 sheet.autoSizeColumn(0);
			 sheet.autoSizeColumn(1);
			 sheet.autoSizeColumn(2);
			 sheet.autoSizeColumn(3);
			 sheet.autoSizeColumn(4);
			 sheet.autoSizeColumn(5);
			 // Write the output to a file.
			 File f = new File(folderPath);
			 // System.out.println ("folder for xmi file" + i + ": "+f.getParent());
			 FileOutputStream fileOut = new FileOutputStream(f.getParent()+"\\XmiDataFile_" + progName +  ".xlsx");
			 wb.write(fileOut);
			 fileOut.close();
			 wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }

}
