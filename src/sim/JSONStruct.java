package sim;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Iris - replacing the JSON part of DataStruct
 */

public class JSONStruct {

	String projectName;

	String className;
	String classID;

//	Set <Operation> operations;

	String methodName;
	String methodID;

	String instrId;
	String instrCode;
	String instrLineNum;
	String instrOperand;
	String instrType;

	public JSONStruct(String ProjN, String ClssN, String clssID, String mthdN, String mthdID, String instID, String instC,
			String instrLN, String instrOp, String instrT)
	{
		this.projectName=ProjN;
		this.className=ClssN;
		this.classID=clssID;
	//	this.operations=new HashSet<Operation>();
		this.methodName=mthdN;
		this.methodID=mthdID;
		this.instrId=instID;
		this.instrCode=instC;
		this.instrLineNum=instrLN;
		this.instrOperand=instrOp;
		this.instrType=instrT;
	}

	@Override
	public String toString()
	{
		return "JSONClass [project name=" + projectName + ", class=" + className + ",\n method " + methodName + "]\n*************************\n";
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String name) {
		this.projectName = name;
		return;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classID == null) ? 0 : classID.hashCode());
		result = prime * result + ((className == null) ? 0 : className.hashCode());
	//	result = prime * result + ((methodID == null) ? 0 : methodID.hashCode());
	//	result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JSONStruct other = (JSONStruct) obj;
		if (classID == null) {
			if (other.classID != null)
				return false;
		} else if (!classID.equals(other.classID))
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (methodID == null) {
			if (other.methodID != null)
				return false;
		} else if (!methodID.equals(other.methodID))
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		return true;
	}


	public String getInstrType() {
		return instrType;
	}

	public void setInstrType(String instrType) {
		this.instrType = instrType;
	}

	public String getInstrLineNum() {
		return instrLineNum;
	}

	public void setInstrLineNum(String instrLineNum) {
		this.instrLineNum = instrLineNum;
	}

	public String getInstrOperand() {
		return instrOperand;
	}

	public void setInstrOperand(String instrOperand) {
		this.instrOperand = instrOperand;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassID() {
		return classID;
	}

	public void setClassID(String classID) {
		this.classID = classID;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodID() {
		return methodID;
	}

	public void setMethodID(String methodID) {
		this.methodID = methodID;
	}

	public String gettInstrId() {
		return instrId;
	}

	public void setInstrId(String instrId) {
		this.instrId = instrId;
	}

	public String getInstrCode() {
		return instrCode;
	}

	public void setInstrCode(String instrCode) {
		this.instrCode = instrCode;
	}

	public String getJsonProjectName() {
		return projectName;
	}

/*	public Set<Operation> getOperations() {
		return operations;
	}

	public void setOperations(Set<Operation> operations) {
		this.operations = operations;
	} */

}