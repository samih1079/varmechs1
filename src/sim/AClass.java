package sim;

import java.util.HashSet;
import java.util.Set;

public class AClass {

	private String programName;
	private String className;

	private Set<Attribute> attributes;
	private Set<Shallow> shallows;
	private Set<Deep> deeps;

	// ------------------------------------CONSTRUCTORS--------------------------------------------------

	public AClass() {
		this.programName = "";
		this.className = "";
		this.attributes = new HashSet<>();
		this.shallows = new HashSet<Shallow>();
		this.deeps = new HashSet<Deep>();
	}

	// ------------------------------------GETTERS & SETTERS--------------------------------------------------


	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Set<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Set<Attribute> set) {
		this.attributes = set;
	}

	public Set<Shallow> getShallowOp() {
		return shallows;
	}

	public void setShallowOp(Set<Shallow> shallows) {
		this.shallows = shallows;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((programName == null) ? 0 : programName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AClass other = (AClass) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (programName == null) {
			if (other.programName != null)
				return false;
		} else if (!programName.equals(other.programName))
			return false;
		return true;
	}

	public Set<Deep> getDeepOp() {
		return this.deeps;
	}

	public void setDeepOp(Set<Deep> deeps) {
		this.deeps = deeps;
	}

	public void addOperation(Deep op) {
		this.deeps.add(op);
	}

	public String toString() {
		String returnedStr=className+"-"+shallows.size() + "; " + deeps.size() + "\nmethods[";
	/*	for (Attribute att:attributes)
			returnedStr+=att.getName()+";";
		returnedStr+="]-["; */
		for (Shallow mthd:shallows)
			returnedStr+=mthd.getName()+";";
		returnedStr+="]\noperations[";
		for (Deep op:deeps)
			returnedStr+="("+/* op.getS1()+", " +*/ op.getOpName()/*+", "+op.getS2() */ + ");";
		returnedStr+="]";

		return returnedStr;

	}
}
