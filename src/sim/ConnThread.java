package sim;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

public class ConnThread extends Thread {
	private final Connection conn;
	private final Set<String> fnS=new HashSet<>();
	public ConnThread(Connection conn){
		this.conn=conn;
	}
	public Connection getConn() {
		return conn;
	}
	/**
	 * @param p
	 * @return true if the string is already in the set
	 */
	public boolean addPair(String p) {
		return !fnS.add(p);
	}	
}