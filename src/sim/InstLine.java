﻿package sim;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;


import javafx.util.Pair;


public class InstLine {
        private final JMethod jmethod;
        private Stack<Pair<ObjectType,Parameter>> s=new Stack<Pair<ObjectType,Parameter>>(){
                private static final long serialVersionUID = 3244208637442478515L;


                public Pair<ObjectType,Parameter> pop(){
                        Pair<ObjectType,Parameter> retVal;
                        	try{
                        		retVal=super.pop();
                        	} catch(Exception e){
                        		retVal=new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("antiexception", ""));
                        	}
                        return retVal;
                }
        };
        private List<Pair<JMethod, Map<Parameter,Pair<ObjectType,Parameter>>>> invokeList=new LinkedList<>();
        public InstLine(JMethod jmethod){
                if(jmethod==null)
                        jmethod=new JMethod(0, 0, "", "void");
                this.jmethod=jmethod;
        }
        public void setInvokes(){
        	for (Pair<JMethod, Map<Parameter, Pair<ObjectType, Parameter>>> a:invokeList) {
                 JMethod otherMethod = a.getKey();
                 if(otherMethod == jmethod)
                	 continue;
                 Map<Parameter, Pair<ObjectType, Parameter>> paramMap = a.getValue();
                 //System.out.println(paramMap);
                 for(Entry<Parameter, Integer> e:otherMethod.getModified().entrySet()){
                         addToMod(paramMap.get(e.getKey()));
                 }
                 //System.out.println(otherMethod.getUsed().entrySet());
                 for(Entry<Parameter, Integer> e:otherMethod.getUsed().entrySet()){
                         addToUsed(paramMap.get(e.getKey()));
                 }                 
        	};        	
    /*            invokeList.forEach(a->{
                        JMethod otherMethod = a.getKey();
                        Map<Parameter, Pair<ObjectType, Parameter>> paramMap = a.getValue();
                        //System.out.println(paramMap);
                        for(Entry<Parameter, Integer> e:otherMethod.getModified().entrySet()){
                                addToMod(paramMap.get(e.getKey()));
                        }
                        for(Entry<Parameter, Integer> e:otherMethod.getUsed().entrySet()){
                                addToUsed(paramMap.get(e.getKey()));
                        }
                }); */
        }
        //load || getstatic
        public void load(ObjectType type, Parameter param) {
                s.push(new Pair<ObjectType,Parameter>(type, param));
        }
        public void store(ObjectType type, Parameter param) {
                Pair<ObjectType,Parameter> t=s.pop();
                addToMod(type, param);
                addToUsed(t);
        }
        public void putstatic(ObjectType type, Parameter param) {
                @SuppressWarnings("unused")
                Pair<ObjectType,Parameter> t=s.pop();
                //addToUsed(t);
                addToMod(type, param);
        }
        public void arrLoad() {
                Pair<ObjectType,Parameter> index=s.pop();
                Pair<ObjectType,Parameter> arr=s.pop();
                addToUsed(index);
                addToUsed(arr);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter(arr.getValue().name+"["+index.getValue().name+"]", arr.getValue().type.replace("[", "").replace("]", ""))));
        }
        public void arrStore() {
                Pair<ObjectType,Parameter> value=s.pop();
                Pair<ObjectType,Parameter> index=s.pop();
                Pair<ObjectType,Parameter> arr=s.pop();
                addToUsed(value);
                addToUsed(index);
                addToMod(arr);
        }
        public void math2args() {
                Pair<ObjectType,Parameter> val2=s.pop();
                Pair<ObjectType,Parameter> val1=s.pop();
                addToUsed(val1);
                addToUsed(val2);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("math#"+val1.getValue().name+"_"+val2.getValue().name, val1.getValue().type)));
        }
        public void cmp() {
                Pair<ObjectType,Parameter> val2=s.pop();
                Pair<ObjectType,Parameter> val1=s.pop();
                addToUsed(val1);
                addToUsed(val2);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("cmp#"+val1.getValue().name+"_"+val2.getValue().name, "int")));
        }
        public void math1arg() {
                Pair<ObjectType,Parameter> val=s.pop();
                addToUsed(val);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("math#"+val.getValue().name, val.getValue().type)));
        }
        public void conv(char to) {
                Pair<ObjectType,Parameter> val=s.pop();
                addToUsed(val);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("conv#"+val.getValue().name, charToType(to))));
        }
        public void pushConst(char type) {
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("const", charToType(type))));
        }
        public void brn1arg() {
                Pair<ObjectType,Parameter> val=s.pop();
                addToUsed(val);
        }
        public void brn2arg() {
                Pair<ObjectType,Parameter> val2=s.pop();
                Pair<ObjectType,Parameter> val1=s.pop();
                addToUsed(val1);
                addToUsed(val2);
        }
        public void iinc(ObjectType type, Parameter param) {
                addToMod(type, param);
        }        
        
        public void getfield(ObjectType type, Parameter param) {
                @SuppressWarnings("unused")
                Pair<ObjectType,Parameter> obj=s.pop();
                //addToUsed(obj);
                s.push(new Pair<ObjectType,Parameter>(type, param));
        }
        public void putfield(ObjectType type, Parameter param) {
                Pair<ObjectType,Parameter> value=s.pop();
                addToUsed(value);
                @SuppressWarnings("unused")
                Pair<ObjectType,Parameter> obj=s.pop();
                //addToUsed(obj);
                addToMod(type, param);
        }
        public void newarray(){
                Pair<ObjectType,Parameter> value=s.pop();
                addToUsed(value);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("new["+value.getValue().name+"]", "")));
        }
        public void mulnewarray(int dim){
                String values="";
                for(int i=0; i<dim; i++){
                        Pair<ObjectType,Parameter> value=s.pop();
                        addToUsed(value);
                        values+="["+value.getValue().name+"]";
                }
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("new"+values, "")));
        }
        public void arraylength(){
                Pair<ObjectType,Parameter> value=s.pop();
                addToUsed(value);
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter(value.getValue().name+".length", "int")));
        }
        public void doReturn(){
                if(jmethod.getReturnType().equals("void"))
                        return;
                Pair<ObjectType,Parameter> value=s.pop();
                addToUsed(value);
        }
        public void invokeIc(int argsLen, Parameter retP, JMethod otherMethod){
                if(otherMethod==null)
                        for(int i=0; i<argsLen; i++){
                                Pair<ObjectType,Parameter> value=s.pop();
                                addToUsed(value);
                        }
                else {
                        int i=0;
                        /*if(otherMethod.getHiddenParam()!=null){
                                addToUsed(s.pop());
                                i++;
                        }*/
                        Map<Parameter, Pair<ObjectType, Parameter>> paramsMap = new HashMap<>();        
                        for(Parameter p:otherMethod.getParams()){
                                paramsMap.put(p, s.pop());
                                i++;
                        }
                        if(i!=argsLen){
                                //System.err.println(s+" i too big "+i+" *** "+argsLen+" *** "+otherMethod+" *** "+this.jmethod);
                                //System.exit(1);
                        }
                        invokeList.add(new Pair<>(otherMethod, paramsMap));
                }
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT, retP));
        }
        public void jsr() {
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT,
                                new Parameter("address", "addr")));        
        }
        public void invokeRef(int argsLen, Parameter retP, JMethod otherMethod){
                Pair<ObjectType,Parameter> obj=s.pop();
                addToUsed(obj);
                invokeIc(argsLen, retP, otherMethod);
        }
        public void dup(int dupNum) {
                Stack<Pair<ObjectType,Parameter>> st=new Stack<Pair<ObjectType,Parameter>>();
                Pair<ObjectType, Parameter> top = s.peek();
                for(int i=0;i<=dupNum;i++){
                        Pair<ObjectType, Parameter> t = s.pop();
                        String typeOf = t.getValue().type;
                        if(typeOf.equals("double")||typeOf.equals("long"))
                                i++;
                        st.push(t);
                }
                s.push(top);
                while(!st.isEmpty())
                        s.push(st.pop());
        }
        public void dup() {
                s.push(s.peek());
        }
        public void dup2(int dupNum) {
                Stack<Pair<ObjectType,Parameter>> st=new Stack<Pair<ObjectType,Parameter>>();
                Pair<ObjectType, Parameter> top = s.pop();
                Pair<ObjectType, Parameter> second = s.pop();
                for(int i=0;i<dupNum;i++){
                        Pair<ObjectType, Parameter> t = s.pop();
                        String typeOf = t.getValue().type;
                        if(typeOf.equals("double")||typeOf.equals("long"))
                                i++;
                        st.push(t);
                }
                s.push(second);
                s.push(top);
                while(!st.isEmpty())
                        s.push(st.pop());
                s.push(second);
                s.push(top);
        }
        public void dup2() {
                Pair<ObjectType, Parameter> top = s.pop();
                Pair<ObjectType, Parameter> second = s.peek();
                s.push(top);
                s.push(second);
                s.push(top);
        }
        public void pop() {
                s.pop();
        }
        public void pop2() {
                String typeOf = s.pop().getValue().type;
                if(typeOf.equals("double")||typeOf.equals("long"))
                        return;
                s.pop();
        }
        public void swap() {
                Pair<ObjectType, Parameter> top = s.pop();
                Pair<ObjectType, Parameter> second = s.pop();
                s.push(top);
                s.push(second);
        }
        
        public void newObj() {
                s.push(new Pair<ObjectType,Parameter>(ObjectType.RESULT, new Parameter("newRef", "")));
        }
        public void tableswitch() {
                addToUsed(s.pop());        
        }
        private String charToType(char t){
                switch(t){
                case 'i':
                        return "int";
                case 'l':
                        return "long";
                case 'f':
                        return "float";
                case 'd':
                        return "double";
                case 'a':
                        return "ref";
                case 'b':
                        return "byte";
                case 'c':
                        return "char";
                case 's':
                        return "short";
                }
                return "";
        }
        private void addToMod(Pair<ObjectType, Parameter> t) {
                if(t!=null)
                        addToMod(t.getKey(), t.getValue());        
        }
        private void addToUsed(Pair<ObjectType, Parameter> t) {
                if(t!=null)
                        addToUsed(t.getKey(), t.getValue());        
        }
        private void addToUsed(ObjectType type, Parameter param) {
                if(checkIfParamOrField(type, param))
                        jmethod.addUsed(param, 1);        
        }
        private void addToMod(ObjectType type, Parameter param) {
                if(checkIfParamOrField(type, param))
                        jmethod.addModified(param, 1);
        }
        private boolean checkIfParamOrField(ObjectType type, Parameter param) {
                if(param.getName().equals("this"))
                        return false;
                return type.equals(ObjectType.FIELD)||type.equals(ObjectType.PARAMETER);
        }
}