package sim;


import javafx.util.Pair;


public class CommutativePointerPair<K,V> extends Pair<K,V> {
	private static final long serialVersionUID = 7971847932701896293L;
	public CommutativePointerPair(K arg0, V arg1) {
		super(arg0, arg1);
	}
	@Override
	public int hashCode() {
		return getKey().hashCode()+getValue().hashCode();
	}
	public boolean equals(Object other){
		
	
		if (other instanceof CommutativePointerPair) {
			CommutativePointerPair<Object,Object> otherPair=(CommutativePointerPair) other;
			if((otherPair.getKey()==(getKey()))&&(otherPair.getValue()==(getValue())))
				return true;
			return (otherPair.getKey()==(getValue()))&&(otherPair.getValue()==(getKey()));
		}	
		return false;
	}
}
