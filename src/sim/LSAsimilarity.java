package sim;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.sql.Connection;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.*;


/**
 *
 * @author Lenovo1
 */
public class LSAsimilarity implements SimilarityInterface {


  //  @Override
    public double calculateSimilarity(String s1, String s2, Connection conn) {
        char c='"';
        s1=s1.replaceAll(""+c, "");
        s2=s2.replaceAll(""+c, "");

    	String technique = "General_Reading_up_to_1st_year_college 	 (300 factors)";

        double lsaSim = 0;

        String url = "http://lsa.colorado.edu/cgi-bin/LSA-sentence-x.html";

        BufferedReader br = null;
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        method.addParameter("LSAspace",technique);
        method.addParameter("txt1",s1+ ":"+s2);
        String HTML = "";

        try {
            int statusCode = client.executeMethod(method);

            if(statusCode == HttpStatus.SC_NOT_IMPLEMENTED) {
                System.err.println("The Post method is not implemented by this URI");
	                // still consume the response body
                method.getResponseBodyAsString();

            } else {

                br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
                String readLine;
                StringBuilder str = new StringBuilder();
                String line = null;

                while(((readLine = br.readLine()) != null)) {
                    str.append(readLine);
                }
                HTML = str.toString();
                Document doc = Jsoup.parse(HTML,"UTF-8");
                Element tdElement = doc.select("TD").first();

                if (tdElement!=null) {

                    try{
                        lsaSim = Double.parseDouble(tdElement.text());
                    }catch (NumberFormatException e) {
                        lsaSim=0;
                }
                }
                else {
                    lsaSim = 0;
                }
        }

    } catch (Exception e) {
        System.err.println(e);
    } finally {
        method.releaseConnection();
        if(br != null) try { br.close(); } catch (Exception fe) {}
    }
      return lsaSim;

    }
}

