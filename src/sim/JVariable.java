package sim;

public class JVariable extends Parameter{
	private String vartype;
	public JVariable(String name, String type, String vartype){
		super(name, type);
		this.vartype=vartype;
	}
	public String getVartype() {
		return vartype;
	}
	public void setVartype(String vartype) {
		this.vartype = vartype;
	}
	public String toString(){
		return super.toString()+":"+vartype;
	}
}
