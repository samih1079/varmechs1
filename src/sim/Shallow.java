package sim;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Shallow {

	private final Parameter op;
	private final VisabilityEnum visibility;
	private final List<Parameter> para = new LinkedList<Parameter>();

	public Shallow(String name, String returnType, VisabilityEnum visabilityEnum) {
		this.op = new Parameter(name, returnType);
		this.visibility = visabilityEnum;
	}

	public List<Parameter> getPara() {
		return Collections.unmodifiableList(para);
	}

	public VisabilityEnum getVisibility() {
		return visibility;
	}

	public String getName() {
		return op.getName();
	}

	public void setName(String name) {
		// op.setName(name.replaceAll("[^a-zA-Z^\\d.]", ""));
		op.setName(name);
	}

	public String getReturnType() {
		return op.getType();
	}

	public void setReturnType(String returnType) {
		op.setType(returnType);
	}

	public void addParameter(Parameter param) {
		para.add(param);
	}

	public void removeParameter(Parameter param) {
		para.remove(param);
	}

	@Override
	public String toString() {
		if (para != null)
			return "Method [name=" + op.getName() + ",return " + op.getType() + ", para=" + para + ", visibility="
					+ visibility + "]\n";
		else
			return "Method [name=" + op.getName() + ",return " + op.getType() + ", para=void" + ", visibility="
					+ visibility + "]\n";
	}

}