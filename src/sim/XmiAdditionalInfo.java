package sim;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class XmiAdditionalInfo {
	private List<Entry<String, String>> classHirarchy = new LinkedList<Entry<String, String>> ();
	private final Map<String, List<Shallow>> methods=new HashMap<>();
	public void addMethod(String cls, Shallow m){
		List<Shallow> methodList = methods.get(cls);
		if(methodList==null){
			methodList=new LinkedList<>();
			methods.put(cls, methodList);
		}
		methodList.add(m);
	}
	public List<Entry<String, String>> getClassHirarchy() {
		return Collections.unmodifiableList(classHirarchy);
	}
	public Map<String, List<Shallow>> getMethods() {
		return Collections.unmodifiableMap(methods);
	}
	public void setClassHir(List<Entry<String, String>> classHirarchy) {
		this.classHirarchy=classHirarchy;
	}
}
