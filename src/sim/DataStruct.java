package sim;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Asaf - replaced by XMIStruct and JSONStruct
 */

public class DataStruct {

	// from XMI
	String projectName;
	String name;
	Set <Shallow> shallows;
	Set<MultiParam> para;

	// from JSON
	String jsonProjectName;

	String className;
	String classID;

	String methodName;
	String methodID;

	String instrId;
	String instrCode;
	String instrLineNum;
	String instrOperand;
	public String instrName;
	String instrType;

	String record;
	String methodRecord;
	String classRecord;

	public DataStruct()
	{

		this.shallows=new HashSet<Shallow>();
		this.para=new HashSet<MultiParam>();
	}

	@Override
	public String toString()
	{
		return "DataStruct [name=" + name + ",\n methods=" + shallows +"\n"+
				 ", classAttributes=" + para + "]\n*************************\n";
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String name) {
		this.projectName = name;
		return;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Set<Shallow> getMethods() {
		return shallows;
	}


	public void setMethods(Set<Shallow> shallows) {
		this.shallows = shallows;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
	//	result = prime * result + ((classID == null) ? 0 : classID.hashCode());
	//	result = prime * result + ((className == null) ? 0 : className.hashCode());
	//	result = prime * result + ((methodID == null) ? 0 : methodID.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataStruct other = (DataStruct) obj;
		if (classID == null) {
			if (other.classID != null)
				return false;
		} else if (!classID.equals(other.classID))
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (methodID == null) {
			if (other.methodID != null)
				return false;
		} else if (!methodID.equals(other.methodID))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		return true;
	}


	public Set<MultiParam> getPara() {
		return para;
	}


	public void setPara(Set<MultiParam> para) {
		this.para = para;
	}

	public String getInstrName() {
		return instrName;
	}

	public void setInstrName(String instrName) {
		this.instrName = instrName;
	}

	public String getInstrType() {
		return instrType;
	}

	public void setInstrType(String instrType) {
		this.instrType = instrType;
	}

	public String getInstrLineNum() {
		return instrLineNum;
	}

	public void setInstrLineNum(String instrLineNum) {
		this.instrLineNum = instrLineNum;
	}

	public String getInstrOperand() {
		return instrOperand;
	}

	public void setInstrOperand(String instrOperand) {
		this.instrOperand = instrOperand;
	}

	public String getClassRecord() {
		return classRecord;
	}

	public void setClassRecord(String classRecord)
	{
		this.classRecord = classRecord;

	String aa[] =  classRecord.split(",");
	for (int i=0; i<aa.length ; i++)
	{
    	if(aa[i].contains("\"CLASSNAME\""))
    	{
    		String str[] =  aa[i].split(":");
    		String name = str[1];
    		if (str[1].contains("."))
    		{
				String str2[] = str[1].split("\\.");
				name = str2[1];
    		}
    		name = name.replaceAll("[^a-zA-Z^\\d.]", "");
    		this.className = name;
    		if (this.methodName == null)
    			this.methodName = name;
    	}
	}

	String str[] = this.instrId.split(":");
	if (str != null && str.length > 1)
		this.instrId = str[1].replaceAll("[^a-zA-Z^\\d.]", "");

	String str2[] = this.instrCode.split(":");
	if (str2 != null && str2.length > 1)
		this.instrCode = str2[1].replaceAll("[^a-zA-Z^\\d.]", "");

	String str3[] = this.instrLineNum.split(":");
	if (str3 != null && str3.length > 1)
		this.instrLineNum = str3[1].replaceAll("[^a-zA-Z^\\d.]", "");

	String str4[] = this.instrOperand.split(":");
	if (str4 != null && str4.length > 1)
		this.instrName = str4[1];

	if (str4[1].contains("."))
	{
		String str5[] = str4[1].split("\\.");
		for (String name : str5)
		{
			this.instrName = name;
		}
	}


	if (str4.length > 2){
		this.instrType = str4[2].replaceAll("[^a-zA-Z^\\d.]", "");
		if (str4[2].contains("/"))
		{
			String aaa[] = str4[2].split("/");
			for (String type : aaa)
			{
				this.instrType = type.replaceAll("[^a-zA-Z^\\d.]", "");
			}
		}
	}
}

	public String getMethodRecord() {
		return methodRecord;
	}

	public void setMethodRecord(String methodRecord) {
		this.methodRecord = methodRecord;

		String aa[] =  methodRecord.split(",");
		for (int i=0; i<aa.length ; i++)
		{
			if (aa[i].contains("\"METHODNAME\""))
			{
				String str[] = aa[i].split(":");
				if (str != null && str.length > 1)
				{
					String name = str[1].replaceAll("[^a-zA-Z^\\d.]", "");
					if (name.equals("init"))
						this.methodName = null;
					else this.methodName = name;
				}
			}
			if (aa[i].contains("\"CLASSID\""))
			{
				this.classID = aa[i];
			}
		}
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record)
	{
		this.record = record;

		String aa[] =  record.split(",");
		for (int i=0; i<aa.length ; i++)
		{
			if (aa[i].contains("\"INSTRID\""))
			{
				this.instrId = aa[i];
			}
			if (aa[i].contains("\"METHODID\""))
			{
				this.methodID = aa[i];
			}
			if (aa[i].contains("\"INSTROPCODE\""))
			{
				this.instrCode = aa[i];
			}
			if (aa[i].contains("\"INSTRLINENUM\""))
			{
				this.instrLineNum = aa[i];
			}		if (aa[i].contains("\"INSTROPERAND\""))
			{
				this.instrOperand = aa[i];
			}
		}
	}


	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassID() {
		return classID;
	}

	public void setClassID(String classID) {
		this.classID = classID;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodID() {
		return methodID;
	}

	public void setMethodID(String methodID) {
		this.methodID = methodID;
	}

	public String getInstrId() {
		return instrId;
	}

	public void setInstrId(String instrId) {
		this.instrId = instrId;
	}

	public String getInstrCode() {
		return instrCode;
	}

	public void setInstrCode(String instrCode) {
		this.instrCode = instrCode;
	}
	public String getJsonProjectName() {
		return jsonProjectName;
	}


	public void setJsonProjectName(String jsonProjectName) {
		this.jsonProjectName = jsonProjectName;
	}


}