package sim;

import java.util.*;

/**
 * @author Iris - replacing the XMI part of DataStruct
 */

public class XMIStruct {

	String projectName;
	String name;
	String xmiID;
	Set <Shallow> shallows;
	Set<Attribute> para;

	public XMIStruct(String projectName, String name, String xmiID)
	{
		this.projectName=projectName;
		this.name=name;
		this.xmiID=xmiID;
		this.shallows=new LinkedHashSet<Shallow>();
		this.para=new LinkedHashSet<>();
	}

	@Override
	public String toString()
	{
		return "XMIClass [project name=" + projectName + ", name=" + name + ",\n methods=" + shallows +"\n"+
				 ", classAttributes=" + para + "]\n*************************\n";
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String name) {
		this.projectName = name;
		return;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXmiID() {
		return xmiID;
	}

	public void setXmiID(String xmiID) {
		this.xmiID = xmiID;
	}


	public Set<Shallow> getMethods() {
		return shallows;
	}

	public void setMethods(Set<Shallow> shallows) {
		this.shallows = shallows;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XMIStruct other = (XMIStruct) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		return true;
	}

	public Set<Attribute> getPara() {
		return para;
	}

	public void setPara(Set<Attribute> para) {
		this.para = para;
	}

	public void addMethod (Shallow mthd) {
		this.shallows.add(mthd);
	}

	public void removeMethod (Shallow mthd) {
		this.shallows.remove(mthd);
	}

	public void addAttribute (Attribute att) {
		this.para.add(att);
	}
	public void removeAttribute (Attribute xmiA) {
		this.para.remove(xmiA);
	}
}