package sim;


import javafx.util.Pair;

public class PointerPair<K,V> extends Pair<K,V> {
	private static final long serialVersionUID = 7971847932701896293L;
	public PointerPair(K arg0, V arg1) {
		super(arg0, arg1);
	}
	@Override
	public int hashCode() {
		return getKey().hashCode()+31*getValue().hashCode();
	}
	public boolean equals(Object other){
		if (other instanceof Pair) {
			@SuppressWarnings("unchecked")
			Pair<Object,Object> otherPair=(Pair<Object, Object>) other;
			if((otherPair.getKey()==(getKey()))&&(otherPair.getValue()==(getValue())))
				return true;
		}	
		return false;
	}
}
